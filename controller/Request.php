<?php
/**
 * Lightweight Request Object
 *
 * @version 0.0.1
 * @author Sebastian Langer <sl@scn.cx>
 */
class Request {
    public $url;
    public $type;
    public $params;
    public $cookies;
    public $hostname;
    public $path;
    public $protocol;
    public $query;
    public $route;
    public $header;
    public $files;

    const RESOURCE_NOT_LOADED = 'notloadedyet';

    public function __construct($type, $url) {
        $this->type = $type;
        $this->url = $url;
        $this->header = self::RESOURCE_NOT_LOADED;
        $this->files = self::RESOURCE_NOT_LOADED;
        $this->protocol = (array_key_exists('HTTPS', $_SERVER) && $_SERVER['HTTPS']) ? 'https' : 'http';
        $this->cookies = $_COOKIE;
        $this->hostname = $_SERVER['SERVER_NAME'];

        // set query
        if($this->type === 'get'){
            $this->query = $_GET;
        } elseif($this->type === 'post'){
            $this->query = $_POST;
        } else {
            $this->query = $_REQUEST;
        }
    }

    public function header($header) {
        // lazy-load headers
        if($this->header === self::RESOURCE_NOT_LOADED){
            $this->header = getallheaders();
        }

        if(array_key_exists($header, $this->header)){
            return $this->header[$header];
        } else {
            return null;
        }
    }

    public function isXhr(){
        if(!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest'){
            return true;
        }
        return false;
    }

    public function getFile($id){
        if($this->files === self::RESOURCE_NOT_LOADED){
            $this->files = $_FILES;
        }
        return $this->files[$id];
    }
}
