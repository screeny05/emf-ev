<?php
/**
 * Lightweight Response Object,
 * used for buffering output, lazy rendering, etc.
 *
 * @version 0.0.1
 * @author Sebastian Langer <sl@scn.cx>
 */
class Response {
    private $twig;

    private $buffer = '';
    private $headers = [];
    private $locked = false;
    private $status = 200;
    public  $data = [];

    const TEMPLATE_ERROR = 'server-error.twig';

    function __construct($twig) {
        $this->twig = $twig;
    }

    public function set($key, $value) {
        $this->data[$key] = $value;
        return $this;
    }

    public function get($key, $default = null) {
        if(array_key_exists($key, $this->data)){
            return $this->data[$key];
        } else {
            return $default;
        }
    }

    public function has($key){

    }

    public function header($key, $value){
        if($this->isLocked()){
            return;
        }
        $this->headers[strtolower($key)] = $value;
    }

    public function hasHeader($key){
        return array_key_exists(strtolower($key), $this->headers);
    }

    public function redirect($url, $permanent = true){
        $this->status($permanent ? 301 : 307);
        $this->headers = ['Location' => $url];
        $this->buffer = '';
        $this->lock();
    }

    public function contentType($type){
        if($this->isLocked()){
            return;
        }
        $this->header('Content-Type', $type);
    }

    public function contentDisposition($filename){
        if($this->isLocked()){
            return;
        }
        if(!$this->hasHeader('Content-Type')){
            $this->header('Content-Type', 'application/octet-stream');
        }
        $this->header('Content-Disposition', 'attachment;filename=' . $filename);
        $this->header('Content-Transfer-Encoding', 'binary');
    }

    public function status($status){
        $this->status = $status;
        return $this;
    }

    public function getStatus(){
        return $this->status;
    }

    public function lock(){
        $this->locked = true;
    }
    public function isLocked(){
        return $this->locked;
    }

    public function raw($string){
        if(!$this->isLocked()){
            $this->buffer .= $string;
        }
        return $this;
    }

    public function render($tpl, $data = []){
        if(!$this->isLocked()){
            $data = array_merge($data, $this->data);
            $this->buffer .= $this->twig->render($tpl, $data);
        }

        return $this;
    }

    public function renderError($title, $message){
        $data = array_merge($this->data, [
            'error_code' => $this->status,
            'error_head' => $title,
            'error_message' => $message
        ]);
        $this->buffer = $this->twig->render(self::TEMPLATE_ERROR, $data);

        return $this;
    }

    public function renderData($data = []){
        $this->buffer = json_encode($data);
        return $this;
    }

    public function execute(){
        echo $this->buffer;

        foreach ($this->headers as $key => $value) {
            if(gettype($key) == 'string'){
                header($key . ': ' . $value);
            } else {
                header($value);
            }
        }

        http_response_code($this->status);
    }
}
