<?php
/**
* Ultra-lightweight URL-Routing for PHP
*
* @version 0.1.1
* @author Sebastian Langer <sl@scn.cx>
*/

class Route {
    public $pattern;
    public $compiledPattern;
    public $middleware;

    private $addSlash = false;
    private $upgradePath = true; // upgrades to the correct trailing slash

    public function __construct($pattern, $isRegex, $middleware){
        $this->pattern = $pattern;
        $this->middleware = $middleware;

        if($isRegex){
            $this->compiledPattern = $pattern;
            return;
        }

        // remove trailing slash to allow matching on
        // paths without trailing slash.
        // (allows for easier upgrading)
        if($this->endsWithChar($pattern, '/')){
            $pattern = substr($pattern, 0, strlen($pattern) - 1);
            $this->addSlash = true;
        }

        // compile the (prefix)match patterns
        $pattern = preg_replace_callback('/([:\*])([\w]+)/', function($match){
            switch ($match[1]) {
                case ':':
                    return '(?<' . $match[2] . '>[a-zA-Z0-9_\+\-%]+?)';
                case '*':
                    return '(?<' . $match[2] . '>.*)';
            }
        }, $pattern);

        $this->compiledPattern = "/^" . str_replace('/', '\/', $pattern) . "$/";
    }
    public function match($url){
        $matchInfo = [
            'matches'       => false,
            'url'           => '',
            'patternValues' => []
        ];
        $pos = strpos($url, '?');
        $url = $pos ? substr($url, 0, $pos) : $url;
        $matchInfo['url'] = $url;

        if($this->endsWithChar($url, '/')){
            $url = substr($url, 0, strlen($url) - 1);
        }

        $matchInfo['matches'] = preg_match($this->compiledPattern, $url, $matchInfo['patternValues']);
        return $matchInfo;
    }
    public function runMiddleware($req, $res){
        $newUrl = $this->tryUpgradePath($req);
        if($newUrl !== false){
            return header('Location: ' . $newUrl);
        }


        foreach ($this->middleware as $fn){
            if($fn($req, $res) === true || $res->isLocked()){
                break;
            }
        }
        $res->execute();
    }

    private function tryUpgradePath($req){
        if(!$this->upgradePath){
            return false;
        }

        $url = $req->path;
        $params = $req->query;
        $hasTrailingSlash = substr($url, strlen($url) - 1, 1) == '/';

        if($hasTrailingSlash && !$this->addSlash){
            $url = substr($url, 0, strlen($url) - 1);
        } elseif(!$hasTrailingSlash && $this->addSlash){
            $url = $url . '/';
        } else {
            return false;
        }

        // add missing params (works only in get)
        if(!empty($params) && $req->type == 'get'){
            $url .= '?' . http_build_query($params, '', '&');
        }

        return $url;
    }

    private function endsWithChar($haystack, $char){
        return substr($haystack, strlen($haystack) - 1, 1) == $char;
    }
}
