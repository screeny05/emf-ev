<?php
class Router {

    public $routeCollection = [
        'get'    => [],
        'post'   => [],
        'put'    => [],
        'delete' => []
    ];

    public $globalMiddleware = [];

    private $twig;


    function __construct($twig) {
        $this->twig = $twig;
    }



    // add global middleware
    public function useGlobal($fn) {
        array_push($this->globalMiddleware, $fn);
    }

    // add some sugar in here!
    public function get($urlPattern, $isRegex) {
        $argOffset = 1;
        if($isRegex === true){
            $argOffset++;
        }
        $this->addRoute('get', $urlPattern, $isRegex === true, array_slice(func_get_args(), $argOffset));
    }
    public function post($urlPattern, $isRegex) {
        $argOffset = 1;
        if($isRegex === true){
            $argOffset++;
        }
        $this->addRoute('post', $urlPattern, $isRegex === true, array_slice(func_get_args(), $argOffset));
    }
    public function all($urlPattern, $isRegex) {
        $argOffset = 1;
        if($isRegex === true){
            $argOffset++;
        }
        $this->addRoute('all', $urlPattern, $isRegex === true, array_slice(func_get_args(), $argOffset));
    }

    public function addRoute($requestType, $urlPattern, $isRegex, $middleware) {
        $requestType = strtolower($requestType);

        if($requestType == 'all'){
            foreach ($this->routeCollection as $arrayRequestType => $arrayRequestPatterns) {
                $this->addRoute($arrayRequestType, $urlPattern, $isRegex, $middleware);
            }
            return;
        }

        if(gettype($urlPattern) == 'array'){
            foreach ($urlPattern as $pattern) {
                $this->addRoute($requestType, $pattern, $isRegex, $middleware);
            }
            return;
        }

        if(!array_key_exists($requestType, $this->routeCollection)){
            $this->routeCollection[$requestType] = [];
        }

        $route = new Route($urlPattern, $isRegex, $middleware);
        array_push($this->routeCollection[$requestType], $route);
    }

    public function runGlobal($req, $res){
        foreach ($this->globalMiddleware as $fn) {
            $doNext = $fn($req, $res);
            if($doNext === false){
                return false;
            }
        }
    }

    public function route($requestType, $url) {
        $requestType = strtolower($requestType);

        // opinionated? maybe!
        if(!array_key_exists($requestType, $this->routeCollection)){
            $requestType = 'get';
        }

        $req = new Request($requestType, $url);
        $res = new Response($this->twig);


        try {
            foreach ($this->routeCollection[$requestType] as $route) {
                $routeInfo = $route->match($url);
                $routeInfo['requestType'] = $requestType;
                if($routeInfo['matches']){
                    $req->path = $routeInfo['url'];
                    $req->params = $routeInfo['patternValues'];
                    $req->route = $route;

                    if($this->runGlobal($req, $res) === false){
                        return;
                    }
                    $route->runMiddleware($req, $res);
                    break;
                }
            }
        } catch(Exception $e){
            $stacktrace = $e->getTrace();
            array_unshift($stacktrace, [
                'file' => $e->getFile(),
                'line' => $e->getLine(),
                'function' => '<root>'
            ]);
            $res->status(503)
                ->set('stacktrace', $stacktrace)
                ->renderError(false, $e->getMessage())
                ->execute();
        }
    }
}
