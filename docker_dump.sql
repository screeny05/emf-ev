# ************************************************************
# Sequel Pro SQL dump
# Version 4500
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.49-0ubuntu0.14.04.1)
# Datenbank: emf-ev
# Erstellt am: 2016-05-16 17:17:41 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Export von Tabelle nnGroupsRoles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `nnGroupsRoles`;

CREATE TABLE `nnGroupsRoles` (
  `groupID` varchar(255) NOT NULL,
  `roleID` varchar(255) NOT NULL,
  PRIMARY KEY (`groupID`,`roleID`),
  KEY `nnGroupsRoles_fi_d603d7` (`roleID`),
  CONSTRAINT `nnGroupsRoles_fk_d603d7` FOREIGN KEY (`roleID`) REFERENCES `tblAccessRoles` (`roleID`),
  CONSTRAINT `nnGroupsRoles_fk_e6d3be` FOREIGN KEY (`groupID`) REFERENCES `tblAccessGroups` (`groupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `nnGroupsRoles` WRITE;
/*!40000 ALTER TABLE `nnGroupsRoles` DISABLE KEYS */;

INSERT INTO `nnGroupsRoles` (`groupID`, `roleID`)
VALUES
	('author','admin'),
	('user','admin'),
	('user','admin-calendar'),
	('user','admin-calendar-create'),
	('user','admin-calendar-create-private'),
	('user','admin-calendar-create-public'),
	('user','admin-calendar-update'),
	('user','admin-calendar-view-private'),
	('user','admin-calendar-view-public'),
	('author','admin-files'),
	('author','admin-files-create'),
	('author','admin-files-create-private'),
	('author','admin-files-create-public'),
	('author','admin-files-delete'),
	('author','admin-files-update'),
	('author','admin-files-view-private'),
	('user','admin-files-view-private'),
	('author','admin-files-view-public'),
	('default','admin-files-view-public'),
	('user','admin-files-view-public'),
	('author','admin-news'),
	('user','admin-news'),
	('author','admin-news-create'),
	('author','admin-news-delete'),
	('author','admin-news-update');

/*!40000 ALTER TABLE `nnGroupsRoles` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblAccessGroups
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblAccessGroups`;

CREATE TABLE `tblAccessGroups` (
  `groupID` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  `admin` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`groupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblAccessGroups` WRITE;
/*!40000 ALTER TABLE `tblAccessGroups` DISABLE KEYS */;

INSERT INTO `tblAccessGroups` (`groupID`, `title`, `admin`)
VALUES
	('admin','Administrator', 1),
	('author','Redakteur', 0),
	('default','Öffentlich', 0),
	('user','Benutzer', 0);

/*!40000 ALTER TABLE `tblAccessGroups` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblAccessRoles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblAccessRoles`;

CREATE TABLE `tblAccessRoles` (
  `roleID` varchar(255) NOT NULL,
  `title` varchar(255) NOT NULL,
  PRIMARY KEY (`roleID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblAccessRoles` WRITE;
/*!40000 ALTER TABLE `tblAccessRoles` DISABLE KEYS */;

INSERT INTO `tblAccessRoles` (`roleID`, `title`)
VALUES
	('admin','Zugriff auf den Adminbereich'),
	('admin-calendar','Menüpunkt Kalender'),
	('admin-calendar-create','Kalender - Eintrag hinzufügen'),
	('admin-calendar-create-private','Kalender - Privaten Eintrag bearbeiten'),
	('admin-calendar-create-public','Kalender - Öffentlichen Eintrag bearbeiten'),
	('admin-calendar-update','Kalender - Eintrag bearbeiten'),
	('admin-calendar-view-private','Kalender - Private Einträge sichtbar'),
	('admin-calendar-view-public','Kalender - Öffentliche Einträge sichtbar'),
	('admin-config','Menüpunkt Konfiguration'),
	('admin-config-cache','Konfiguration - Template Cache leeren'),
	('admin-config-cloudflare-cache','Konfiguration - Cloudflare Cache leeren'),
	('admin-config-gulp','Konfiguration - Gulp Tasks ausführen'),
	('admin-config-update','Konfiguration - Einstellungen bearbeiten'),
	('admin-files','Menüpunkt Dateien'),
	('admin-files-create','Dateien - Datei hinzufügen'),
	('admin-files-create-private','Dateien - Private Datei bearbeiten'),
	('admin-files-create-public','Dateien - Öffentliche Datei bearbeiten'),
	('admin-files-delete','Dateien - Datei löschen'),
	('admin-files-folder','Dateien - Ordner bearbeiten'),
	('admin-files-update','Dateien - Datei bearbeiten'),
	('admin-files-view-private','Dateien - Private Dateien sichtbar'),
	('admin-files-view-public','Dateien - Öffentliche Dateien sichtbar'),
	('admin-mail','Menüpunkt E-Mail'),
	('admin-mail-create','E-Mail - hinzufügen'),
	('admin-mail-delete','E-Mail - löschen'),
	('admin-news','Menüpunkt News'),
	('admin-news-create','News - Hinzufügen'),
	('admin-news-delete','News - Löschen'),
	('admin-news-update','News - Bearbeiten'),
	('admin-snippets','Menüpunkt Snippets'),
	('admin-snippets-create','Snippets - Hinzufügen'),
	('admin-snippets-update','Snippets - Bearbeiten'),
	('admin-user','Menüpunkt User'),
	('admin-user-csv','User - Exportieren'),
	('admin-user-group','User - Gruppe bearbeiten'),
	('admin-user-group-create','User - Gruppe hinzufügen'),
	('admin-user-group-delete','User - Gruppe löschen'),
	('admin-user-pin','User - PIN hinzufügen'),
	('admin-user-pin-delete','User - PINs löschen'),
	('admin-user-update','User - User bearbeiten'),
	('admin-sponsors', 'Menüpunkt Sponsoren'),
	('admin-sponsors-create', 'Sponsoren - Hinzufügen'),
	('admin-sponsors-delete', 'Sponsoren - Löschen'),
	('admin-sponsors-update', 'Sponsoren - Updaten');

/*!40000 ALTER TABLE `tblAccessRoles` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblConfig
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblConfig`;

CREATE TABLE `tblConfig` (
  `configID` varchar(255) NOT NULL,
  `value` varchar(8192) NOT NULL,
  `domainID` int(11) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`configID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblConfig` WRITE;
/*!40000 ALTER TABLE `tblConfig` DISABLE KEYS */;

INSERT INTO `tblConfig` (`configID`, `value`, `domainID`, `created_at`, `updated_at`)
VALUES
	('DefaultRequestScheme','http',NULL,'2015-09-13 15:50:24','2015-09-15 10:21:14'),
	('Domain','localhost',NULL,'2015-09-13 01:19:07','2015-09-21 19:51:00'),
	('Environment','dev',NULL,NULL,'2015-09-21 19:51:00'),
	('FilePath','files',NULL,'2015-09-13 01:19:07','2015-09-13 01:19:07'),
	('FilePrefix','/',NULL,'2015-09-13 01:19:07','2016-01-16 18:58:22'),
	('FooterSocial','0',NULL,'2015-09-15 20:21:05','2015-09-15 20:21:05'),
	('MailFrom','info@emf-ev.de',NULL,'2015-09-28 14:06:41','2015-09-28 14:06:41'),
	('MailFromName','EMF e.V.',NULL,'2015-09-28 14:06:41','2015-09-30 16:52:35'),
	('MusicUrl','https://www.mixcloud.com/ElectronicMusicFriends_eV/',NULL,'2015-11-19 17:35:03','2015-11-19 17:35:03'),
	('PicsUrl','https://www.flickr.com/photos/electronic-music-friends/',NULL,'2015-09-13 01:19:08','2015-09-13 21:24:07'),
	('ShopUrl','https://emf-ev.spreadshirt.de/',NULL,'2015-09-13 01:19:08','2015-09-13 01:19:08');

/*!40000 ALTER TABLE `tblConfig` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblFileFolders
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFileFolders`;

CREATE TABLE `tblFileFolders` (
  `folderID` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `public` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`folderID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblFileFolders` WRITE;
/*!40000 ALTER TABLE `tblFileFolders` DISABLE KEYS */;

INSERT INTO `tblFileFolders` (`folderID`, `name`, `public`)
VALUES
	(1,'Interne Dateien',NULL),
	(2,'Öffentliche Dateien',1),
	(3,'News-Dateien',0);

/*!40000 ALTER TABLE `tblFileFolders` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblFiles
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblFiles`;

CREATE TABLE `tblFiles` (
  `fileID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `path` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `size` int(11) NOT NULL,
  `icon` varchar(255) DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `extension` varchar(255) NOT NULL,
  `type` tinyint(4) DEFAULT NULL,
  `description` text,
  `description_cached` text,
  `access_level` int(11) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `folderID` int(11) DEFAULT NULL,
  PRIMARY KEY (`fileID`),
  KEY `tblFiles_fi_f9492f` (`userID`),
  KEY `tblFiles_fk_b19ec1` (`folderID`),
  CONSTRAINT `tblFiles_fk_b19ec1` FOREIGN KEY (`folderID`) REFERENCES `tblFileFolders` (`folderID`),
  CONSTRAINT `tblFiles_fk_f9492f` FOREIGN KEY (`userID`) REFERENCES `tblUser` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblFiles` WRITE;
/*!40000 ALTER TABLE `tblFiles` DISABLE KEYS */;

INSERT INTO `tblFiles` (`fileID`, `userID`, `path`, `name`, `size`, `icon`, `title`, `extension`, `type`, `description`, `description_cached`, `access_level`, `created_at`, `updated_at`, `folderID`)
VALUES
	(8,2,'/files/emf-beitrittsformular.pdf','emf-beitrittsformular.pdf',111190,'file-pdf-o','Beitrittsformular','pdf',4,'Dies ist das offizielle beitrittsformular zur Aufname als Mitglied in unseren Verein. \r\nEinfach herunterladen, ausfüllen, abschicken und schon bist du mitglied bei Electronic-Music-Friends e.V','<p>Dies ist das offizielle beitrittsformular zur Aufname als Mitglied in unseren Verein.\nEinfach herunterladen, ausfüllen, abschicken und schon bist du mitglied bei Electronic-Music-Friends e.V</p>\n',0,'2015-09-20 21:38:26','2015-09-20 21:39:02',2),
	(12,1,'/files/berger-und-tafel-housebloggerklein.jpg','berger-und-tafel-housebloggerklein.jpg',31431,'file-image-o','houseblogger berger und tafel bild klein','jpg',4,'bild für artikel\r\n','<p>bild für artikel</p>\n',1,'2015-10-20 19:15:50','2016-03-03 22:54:58',3),
	(14,1,'/files/toxi-news-bild-klein.JPG','toxi-news-bild-klein.JPG',55910,'file-image-o','toxi bild klein','JPG',4,'Newsartikel Toxicator Bild klein','<p>Newsartikel Toxicator Bild klein</p>\n',1,'2015-12-10 16:49:21','2016-03-03 22:55:10',3),
	(15,1,'/files/artox-bild.JPG','artox-bild.JPG',26877,'file-image-o','Bild Artox News','JPG',4,'bild für Artox Artikel','<p>bild für Artox Artikel</p>\n',1,'2015-12-17 16:58:56','2016-03-03 22:55:22',3),
	(67,2,'/files/armin-ony-news-1.jpg','armin-ony-news-1.jpg',138309,'file-image-o','armin ony news bild','jpg',4,'bla','<p>bla</p>\n',0,'2016-05-11 20:04:17','2016-05-11 20:04:17',3);

/*!40000 ALTER TABLE `tblFiles` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblNews
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblNews`;

CREATE TABLE `tblNews` (
  `newsID` int(11) NOT NULL AUTO_INCREMENT,
  `userID` int(11) NOT NULL,
  `published` datetime NOT NULL,
  `title` text NOT NULL,
  `author` varchar(255),
  `content` text NOT NULL,
  `content_cached` text,
  `seo_title` varchar(255) NOT NULL,
  `listed` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`newsID`),
  UNIQUE KEY `tblNews_u_b5677b` (`seo_title`),
  KEY `tblNews_fi_f9492f` (`userID`),
  CONSTRAINT `tblNews_fk_f9492f` FOREIGN KEY (`userID`) REFERENCES `tblUser` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblNews` WRITE;
/*!40000 ALTER TABLE `tblNews` DISABLE KEYS */;

INSERT INTO `tblNews` (`newsID`, `userID`, `published`, `title`, `content`, `content_cached`, `seo_title`, `listed`)
VALUES
	(3,5,'2015-01-05 11:32:00','Neues Projekt: EMF.TV','Wir haben nun endlich unser neues Projekt: \"EMF.TV\" ins Leben gerufen.\r\nEMF.TV ist unser eigener Kanal auf YouTube, auf dem wir regelmäßig News, Mixtapes unserer DJs, Interviews und vieles mehr veröffentlichen werden.\r\n\r\nSo waren wir für euch auf der \"Sunshine Live - Die 90er Live On Stage\" in der Maimarkthalle in Mannheim unterwegs und haben dies in der ersten Folge \"EMF TV\" festgehalten:\r\n\r\n<iframe style=\"display:block;margin:0 auto;\" width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/RQvvnnqVdKo\" frameborder=\"0\" allowfullscreen></iframe>\r\n\r\nAuch weiterhin werden wir für euch auf Veranstaltungen unterwegs sein, um dort die Stimmung festzuhalten und auch das ein oder andere Interview mit euch persönlich oder dem ein oder anderen Act zu führen.\r\n\r\nNatürlich haben wir auf den Veranstaltungen auch immer unsere EMF Partyfotografen dabei, die euch perfekt in Szene setzen.\r\n\r\nSeit gespannt, wir haben noch so einiges vor!\r\n\r\nBis dahin schaut auch auf jedenfall bei uns auf Facebook vorbei: <a href=\"https://www.facebook.com/emfev\" target=\"_blank\" class=\"heavy-link\"><i class=\"fa fa-facebook\"></i> EMF auf Facebook</a>','<p>Wir haben nun endlich unser neues Projekt: &quot;EMF.TV&quot; ins Leben gerufen.\nEMF.TV ist unser eigener Kanal auf YouTube, auf dem wir regelmäßig News, Mixtapes unserer DJs, Interviews und vieles mehr veröffentlichen werden.</p>\n<p>So waren wir für euch auf der &quot;Sunshine Live - Die 90er Live On Stage&quot; in der Maimarkthalle in Mannheim unterwegs und haben dies in der ersten Folge &quot;EMF TV&quot; festgehalten:</p>\n<iframe style=\"display:block;margin:0 auto;\" width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/RQvvnnqVdKo\" frameborder=\"0\" allowfullscreen></iframe>\n<p>Auch weiterhin werden wir für euch auf Veranstaltungen unterwegs sein, um dort die Stimmung festzuhalten und auch das ein oder andere Interview mit euch persönlich oder dem ein oder anderen Act zu führen.</p>\n<p>Natürlich haben wir auf den Veranstaltungen auch immer unsere EMF Partyfotografen dabei, die euch perfekt in Szene setzen.</p>\n<p>Seit gespannt, wir haben noch so einiges vor!</p>\n<p>Bis dahin schaut auch auf jedenfall bei uns auf Facebook vorbei: <a href=\"https://www.facebook.com/emfev\" target=\"_blank\" class=\"heavy-link\"><i class=\"fa fa-facebook\"></i> EMF auf Facebook</a></p>\n','neues-projekt-emftv',1),
	(5,2,'2015-04-22 00:44:21','Zurück aus der \"Winterpause\"','Wir sind zurück aus der \"Winterpause\"  \r\nUnd dabei haben wir einen komplett überarbeiteten Onlineshop  \r\nfür unsere EMF Shirts, Pullies und Goodies.  \r\nAusserdem neu bei uns im Shop:  \r\n \r\n* Der [EMF SwagBag](http://emf-ev.spreadshirt.de/emf-swagbag-A102079254/customize/color/14)\r\n* Die [Friend\'s Cap](http://emf-ev.spreadshirt.de/friend-s-cap-A102080355/customize/color/461)\r\n* und der [TechSack](http://emf-ev.spreadshirt.de/tech-sack-A102083478/customize/color/2)\r\n\r\n\r\nPassend zum Start der Festivalsaison 2015!  \r\nNeugierig? Dann schau doch in [unserem Shop](http://emf-ev.spreadshirt.de/) vorbei!\r\n','<p>Wir sind zurück aus der &quot;Winterpause&quot;<br />\nUnd dabei haben wir einen komplett überarbeiteten Onlineshop<br />\nfür unsere EMF Shirts, Pullies und Goodies.<br />\nAusserdem neu bei uns im Shop:</p>\n<ul>\n<li>Der <a href=\"http://emf-ev.spreadshirt.de/emf-swagbag-A102079254/customize/color/14\">EMF SwagBag</a>\n</li>\n<li>Die <a href=\"http://emf-ev.spreadshirt.de/friend-s-cap-A102080355/customize/color/461\">Friend\'s Cap</a>\n</li>\n<li>und der <a href=\"http://emf-ev.spreadshirt.de/tech-sack-A102083478/customize/color/2\">TechSack</a>\n</li>\n</ul>\n<p>Passend zum Start der Festivalsaison 2015!<br />\nNeugierig? Dann schau doch in <a href=\"http://emf-ev.spreadshirt.de/\">unserem Shop</a> vorbei!</p>\n','zurueck-aus-der-winterpause',1),
	(8,5,'2015-08-04 12:43:21','EMF Producing Workshop (Sommerferienprogramm Bönnigheim)','In einem Producingworkshop beim Jugendferienprogramm am vergangenen Wochenende\r\nkonnten wir 5 begeisterten, jungen Teilnehmern einen Einblick in die Musikproduktion ermöglichen.\r\nWir freuen uns auch sehr über den netten Artikel in der Heilbronner Stimme!  \r\n![Artikel](https://cdn.scn.cx/dist/img/hz-artikel-workshop-2015.jpg)','<p>In einem Producingworkshop beim Jugendferienprogramm am vergangenen Wochenende\nkonnten wir 5 begeisterten, jungen Teilnehmern einen Einblick in die Musikproduktion ermöglichen.\nWir freuen uns auch sehr über den netten Artikel in der Heilbronner Stimme!<br />\n<img src=\"https://cdn.scn.cx/dist/img/hz-artikel-workshop-2015.jpg\" alt=\"Artikel\" /></p>\n','emf-producing-workshop',1),
	(9,5,'2015-09-21 17:30:02','EMF Podcast #002','Der Electronic-Music-Friends Podcast geht in die Zweite Runde!\r\nDiesesmal zeigt euch **DJ Jam** das Beste aus der härteren Ecke der elektronischen Tanzmusik und gibt euch eine halbe Stunde straight \"150 BPM\" auf die Ohren!\r\n\r\n<iframe width=\"660\" height=\"180\" src=\"https://www.mixcloud.com/widget/iframe/?embed_type=widget_standard&amp;embed_uuid=8cb7ddc2-22fe-44a2-a4e8-42679baad426&amp;feed=https%3A%2F%2Fwww.mixcloud.com%2FElectronicMusicFriends_eV%2Femf-podcast-002-dj-jam-hardstyle%2F&amp;hide_cover=1&amp;hide_tracklist=1&amp;replace=0\" frameborder=\"0\"></iframe><div style=\"clear: both; height: 3px; width: 652px;\"></div><p style=\"display: block; font-size: 11px; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; margin: 0px; padding: 3px 4px; color: rgb(153, 153, 153); width: 652px;\"><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/emf-podcast-002-dj-jam-hardstyle/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=resource_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">EMF Podcast #002 DJ Jam (Hardstyle)</a><span> by </span><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=profile_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">Electronicmusicfriends_Ev</a><span> on </span><a href=\"https://www.mixcloud.com/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=homepage_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\"> Mixcloud</a></p><div style=\"clear: both; height: 3px; width: 652px;\"></div>','<p>Der Electronic-Music-Friends Podcast geht in die Zweite Runde!\nDiesesmal zeigt euch <strong>DJ Jam</strong> das Beste aus der härteren Ecke der elektronischen Tanzmusik und gibt euch eine halbe Stunde straight &quot;150 BPM&quot; auf die Ohren!</p>\n<iframe width=\"660\" height=\"180\" src=\"https://www.mixcloud.com/widget/iframe/?embed_type=widget_standard&amp;embed_uuid=8cb7ddc2-22fe-44a2-a4e8-42679baad426&amp;feed=https%3A%2F%2Fwww.mixcloud.com%2FElectronicMusicFriends_eV%2Femf-podcast-002-dj-jam-hardstyle%2F&amp;hide_cover=1&amp;hide_tracklist=1&amp;replace=0\" frameborder=\"0\"></iframe><div style=\"clear: both; height: 3px; width: 652px;\"></div><p style=\"display: block; font-size: 11px; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; margin: 0px; padding: 3px 4px; color: rgb(153, 153, 153); width: 652px;\"><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/emf-podcast-002-dj-jam-hardstyle/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=resource_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">EMF Podcast #002 DJ Jam (Hardstyle)</a><span> by </span><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=profile_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">Electronicmusicfriends_Ev</a><span> on </span><a href=\"https://www.mixcloud.com/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=homepage_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\"> Mixcloud</a></p><div style=\"clear: both; height: 3px; width: 652px;\"></div>\n','emf-podcast-002',1),
	(10,1,'2015-09-08 16:06:00','EMF Podcast #001','Classic Fans aufgepasst!\r\nTrance Classics von Vinyl, im EMF Podcast #001, gemixt von EMF-DJ **Jona Laxen**:\r\n\r\n<iframe src=\"https://www.mixcloud.com/widget/iframe/?embed_type=widget_standard&amp;embed_uuid=86e6097b-a8e4-48cd-a6d3-a9d1a19cc994&amp;feed=https%3A%2F%2Fwww.mixcloud.com%2FElectronicMusicFriends_eV%2Femf-podcast-001-jona-laxen-trance-classics%2F&amp;hide_cover=1&amp;hide_tracklist=1&amp;replace=0\" width=\"660\" frameborder=\"0\" height=\"180\"></iframe><div style=\"clear: both; height: 3px; width: 652px;\"></div><p style=\"display: block; font-size: 11px; font-family: &quot;Open Sans&quot;,Helvetica,Arial,sans-serif; margin: 0px; padding: 3px 4px; color: rgb(153, 153, 153); width: 652px;\"><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/emf-podcast-001-jona-laxen-trance-classics/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=resource_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">EMF Podcast #001 Jona Laxen (Trance Classics)</a><span> by </span><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=profile_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">Electronicmusicfriends_Ev</a><span> on </span><a href=\"https://www.mixcloud.com/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=homepage_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\"> Mixcloud</a></p><div style=\"clear: both; height: 3px; width: 652px;\"></div>','<p>Classic Fans aufgepasst!\nTrance Classics von Vinyl, im EMF Podcast #001, gemixt von EMF-DJ <strong>Jona Laxen</strong>:</p>\n<iframe src=\"https://www.mixcloud.com/widget/iframe/?embed_type=widget_standard&amp;embed_uuid=86e6097b-a8e4-48cd-a6d3-a9d1a19cc994&amp;feed=https%3A%2F%2Fwww.mixcloud.com%2FElectronicMusicFriends_eV%2Femf-podcast-001-jona-laxen-trance-classics%2F&amp;hide_cover=1&amp;hide_tracklist=1&amp;replace=0\" width=\"660\" frameborder=\"0\" height=\"180\"></iframe><div style=\"clear: both; height: 3px; width: 652px;\"></div><p style=\"display: block; font-size: 11px; font-family: &quot;Open Sans&quot;,Helvetica,Arial,sans-serif; margin: 0px; padding: 3px 4px; color: rgb(153, 153, 153); width: 652px;\"><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/emf-podcast-001-jona-laxen-trance-classics/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=resource_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">EMF Podcast #001 Jona Laxen (Trance Classics)</a><span> by </span><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=profile_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">Electronicmusicfriends_Ev</a><span> on </span><a href=\"https://www.mixcloud.com/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=homepage_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\"> Mixcloud</a></p><div style=\"clear: both; height: 3px; width: 652px;\"></div>\n','emf-podcast-001',1),
	(11,5,'2015-09-06 16:13:00','EMF.TV #003 - Lounge, Freibad Bönnigheim','EMF.TV Folge 3 ist nun online mit einem Aftermovie zur EMF Lounge am vergangenen Wochenende im Freibad Bönnigheim.\r\n\r\n<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/0rkQ38ND5PU\" frameborder=\"0\" allowfullscreen></iframe>','<p>EMF.TV Folge 3 ist nun online mit einem Aftermovie zur EMF Lounge am vergangenen Wochenende im Freibad Bönnigheim.</p>\n<iframe width=\"560\" height=\"315\" src=\"https://www.youtube.com/embed/0rkQ38ND5PU\" frameborder=\"0\" allowfullscreen></iframe>\n','emftv-003',1),
	(12,5,'2015-10-07 18:22:45','EMF Podcast #003','Zwei Wochen sind vergangen und das heißt, es ist wieder Zeit für den nächsten EMF Podcast!\r\nIn dieser Woche gemixt von \"DerTonmann\".\r\nTief wummernde Bässe und knallende Percussions in Verbindung mit deepsten Athmos sind seine Spezialität.\r\nDer perfekte Sound für euren Feierabend.\r\n\r\n<iframe width=\"660\" height=\"180\" src=\"https://www.mixcloud.com/widget/iframe/?embed_type=widget_standard&amp;embed_uuid=ad63433e-b465-439b-8cbb-8c5207f6863b&amp;feed=https%3A%2F%2Fwww.mixcloud.com%2FElectronicMusicFriends_eV%2Femf-podcast-003-dertonmann-techno%2F&amp;hide_cover=1&amp;hide_tracklist=1&amp;replace=0\" frameborder=\"0\"></iframe><div style=\"clear: both; height: 3px; width: 652px;\"></div><p style=\"display: block; font-size: 11px; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; margin: 0px; padding: 3px 4px; color: rgb(153, 153, 153); width: 652px;\"><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/emf-podcast-003-dertonmann-techno/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=resource_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">EMF Podcast #003 DerTonmann (Techno)</a><span> by </span><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=profile_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">Electronicmusicfriends_Ev</a><span> on </span><a href=\"https://www.mixcloud.com/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=homepage_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\"> Mixcloud</a></p><div style=\"clear: both; height: 3px; width: 652px;\"></div>','<p>Zwei Wochen sind vergangen und das heißt, es ist wieder Zeit für den nächsten EMF Podcast!\nIn dieser Woche gemixt von &quot;DerTonmann&quot;.\nTief wummernde Bässe und knallende Percussions in Verbindung mit deepsten Athmos sind seine Spezialität.\nDer perfekte Sound für euren Feierabend.</p>\n<iframe width=\"660\" height=\"180\" src=\"https://www.mixcloud.com/widget/iframe/?embed_type=widget_standard&amp;embed_uuid=ad63433e-b465-439b-8cbb-8c5207f6863b&amp;feed=https%3A%2F%2Fwww.mixcloud.com%2FElectronicMusicFriends_eV%2Femf-podcast-003-dertonmann-techno%2F&amp;hide_cover=1&amp;hide_tracklist=1&amp;replace=0\" frameborder=\"0\"></iframe><div style=\"clear: both; height: 3px; width: 652px;\"></div><p style=\"display: block; font-size: 11px; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; margin: 0px; padding: 3px 4px; color: rgb(153, 153, 153); width: 652px;\"><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/emf-podcast-003-dertonmann-techno/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=resource_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">EMF Podcast #003 DerTonmann (Techno)</a><span> by </span><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=profile_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\">Electronicmusicfriends_Ev</a><span> on </span><a href=\"https://www.mixcloud.com/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=homepage_link\" target=\"_blank\" style=\"color:#808080; font-weight:bold;\"> Mixcloud</a></p><div style=\"clear: both; height: 3px; width: 652px;\"></div>\n','emf-podcast-003',1),
	(13,5,'2015-10-20 14:00:00','EMF Podcast #004','In dieser Woche präsentieren wir euch die vierten Ausgabe unseres EMF Podcasts.  \r\nDiesmal mit DJ AnDee und feinstem Hands Up Sound.  \r\nViel Spaß beim reinhören!  \r\n\r\n<iframe width=\"660\" height=\"180\" src=\"https://www.mixcloud.com/widget/iframe/?embed_type=widget_standard&amp;embed_uuid=56d26f6d-1a17-45a5-b584-7c08783c4df5&amp;feed=https%3A%2F%2Fwww.mixcloud.com%2FElectronicMusicFriends_eV%2Femf-podcast-004-dj-andee-hands-up%2F&amp;hide_cover=1&amp;hide_tracklist=1&amp;replace=0&amp;stylecolor=118f9e\" frameborder=\"0\"></iframe><div style=\"clear: both; height: 3px; width: 652px;\"></div><p style=\"display: block; font-size: 11px; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; margin: 0px; padding: 3px 4px; color: rgb(153, 153, 153); width: 652px;\"><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/emf-podcast-004-dj-andee-hands-up/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=resource_link\" target=\"_blank\" style=\"color: rgb(17, 143, 158); font-weight: bold;\">EMF Podcast #004 DJ AnDee (Hands Up)</a><span> by </span><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=profile_link\" target=\"_blank\" style=\"color: rgb(17, 143, 158); font-weight: bold;\">Electronicmusicfriends_Ev</a><span> on </span><a href=\"https://www.mixcloud.com/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=homepage_link\" target=\"_blank\" style=\"color: rgb(17, 143, 158); font-weight: bold;\"> Mixcloud</a></p><div style=\"clear: both; height: 3px; width: 652px;\"></div>','<p>In dieser Woche präsentieren wir euch die vierten Ausgabe unseres EMF Podcasts.<br />\nDiesmal mit DJ AnDee und feinstem Hands Up Sound.<br />\nViel Spaß beim reinhören!</p>\n<iframe width=\"660\" height=\"180\" src=\"https://www.mixcloud.com/widget/iframe/?embed_type=widget_standard&amp;embed_uuid=56d26f6d-1a17-45a5-b584-7c08783c4df5&amp;feed=https%3A%2F%2Fwww.mixcloud.com%2FElectronicMusicFriends_eV%2Femf-podcast-004-dj-andee-hands-up%2F&amp;hide_cover=1&amp;hide_tracklist=1&amp;replace=0&amp;stylecolor=118f9e\" frameborder=\"0\"></iframe><div style=\"clear: both; height: 3px; width: 652px;\"></div><p style=\"display: block; font-size: 11px; font-family: \'Open Sans\', Helvetica, Arial, sans-serif; margin: 0px; padding: 3px 4px; color: rgb(153, 153, 153); width: 652px;\"><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/emf-podcast-004-dj-andee-hands-up/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=resource_link\" target=\"_blank\" style=\"color: rgb(17, 143, 158); font-weight: bold;\">EMF Podcast #004 DJ AnDee (Hands Up)</a><span> by </span><a href=\"https://www.mixcloud.com/ElectronicMusicFriends_eV/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=profile_link\" target=\"_blank\" style=\"color: rgb(17, 143, 158); font-weight: bold;\">Electronicmusicfriends_Ev</a><span> on </span><a href=\"https://www.mixcloud.com/?utm_source=widget&amp;utm_medium=web&amp;utm_campaign=base_links&amp;utm_term=homepage_link\" target=\"_blank\" style=\"color: rgb(17, 143, 158); font-weight: bold;\"> Mixcloud</a></p><div style=\"clear: both; height: 3px; width: 652px;\"></div>\n','emf-podcast-004',1),
	(16,5,'2015-12-10 16:33:49','Komerzialisierung der Szene -   Die Toxicator 2015','![NewsHeader](https://emf-ev.de/files/toxi-news-bild-klein.JPG)\r\n  \r\nAuch dieses Jahr fanden sich wieder tausende feierwütige Menschen in der Maimarkthalle   Mannheim ein, um gemeinsam beim letzten Festival des Jahres ordentlich nach alter   Ravermanier zu feiern.  \r\nAuch ich war dieses Jahr wieder dort, allerdings diesmal nicht ausschließlich nur um  zu feiern,   \r\nsondern auch um mir ein Bild davon zu machen, ob es stimmt weswegen einige alten Hasen der Raverszene in letzter Zeit solche einen Groll hegen.  \r\n\r\nBei der Anfahrt auffällig war dieses Jahr vor allem die Zahl der Polizeikräfte, welche   rund um das Maimarktgelände patrouillierten. Sicher hatte dies vor allem mit der   politischen Situation und den Ereignissen in der Welt zutun, aber wohl auch mit der   Anzahl an Menschen aufgrund der ausverkauften Veranstaltung.  \r\n\r\nDie Taschen- sowie Kartenkontrolle fanden dieses Jahr nicht wie in den verganenen   Jahren in der Maimarkthalle, sondern in Zelten auf dem Maimarktgelände statt.  \r\nDies machte allerdings keinen all zu großen Unterschied, es ging wie sonst auch   gewohnt \"gemütlich\" zu.  \r\n\r\nAls wir es dann in die Halle geschafft hatten wurde es ungewohnt eng.  \r\nNach meinen ersten drei Toxicator\'s, 2011, 2012 und 2013, konnte ich 2014 leider nicht dort sein.   \r\naber was sich in diesen einen Jahr so alles ändern kann, hat mich dann doch erschrocken.  \r\nWo in den vergangenen Jahren knapp 8000 bis 9000 (fast ausschließlich nur \"Raver\")   gefeiert hatten, waren es in diesem Jahr knapp 12000 Menschen.  \r\nDarunter wie ich leider feststellen musste, unzählige \"Hipster\" der Generation   Turnbeutel, die sich leider überhaupt nicht an die \"Szene anpassten\" \r\nund keine Ahnung hatten, wie man sich auf einer solch einer Veranstaltung zu verhalten   hat.  \r\n  \r\nSo wurde man früher auf den Floors dezent beiseite gebeten bzw. ein wenig geschoben   wenn jemand den Floor verlassen wollte. Heute allerdings muss man ständig damit   rechnen hin und her gedrückt zu werden, von Leuten welche alle 10 Minuten den Floor   wechseln, weil sie es wohl nicht gewohnt sind, dass ein Track auch mal länger als nur   eine Minute Intro, und einer Minute Drop laufen kann.  \r\n  \r\nDie Aufmachung der Bühnenkonstruktionen war wie in den vergangenen Jahren sehr ansehnlich und entgegen meiner Erwartungen wurde immerhin auf unnütze Pyrotechnik und völlig übertriebene CO2 Kanonen verzichtet.\r\nAllerdings schien bei den ersten Sets entweder ein sehr gelangweilter, oder garkein Lichtoperator anwesend gewesen zu sein.\r\nSo blinkte das Licht anfangs nicht nur in den Drop\'s mit immergleichen Lichtszenen, sondern auch in so manchem Intro, was an solchen Stellen vollkommen unpassend war.\r\n  \r\nAuch musikalisch waren so manche gespielten Titel eine ganz neue Erfahrung für mich.\r\nMan selbst steht da und bekommt das Gefühl der Einzige zu sein, der nicht nur da steht um auf den Drop zu warten.    \r\nDie vom MC heruntergeleierten Phrasen, welche man vielleicht von den ganzen    kommerziellen EDM Festivals kennt und die nur dazu dienen das \"gemeine Feiervolk\"   irgendwie bei Laune zu halten, machten das Ganze auch nicht besser.  \r\n  \r\nImmerwieder die Aufforderung die Hände in die Luft zu strecken,  \r\ndamit auch diejenigen, welche der Szenemusik nicht sonderlich affin sind, wissen dass   jetzt gerade ein emotionaler Part des Tracks läuft.  \r\n    \r\nUnd als würden die Buld-Up\'s, welche ohnehin früher in diesem typischen EDM Style nie in Genres wie Hardstyle zu hören waren, den Drop nicht ausreichend ankündigen, gibt   der MC natürlich mit der Phrase \"3,2,1,Go\" (wahlweise auch \"3,2,1,Jump\"),   \r\nauch dem letzten Menschen auf der Tanzfläche zu verstehen, dass jetzt gleich der Drop beginnt und er doch bitte hysterisch auf und ab hüpfen soll.  \r\n    \r\nDas ganze führt dazu, dass man sich nicht wie früher von der Musik treiben lässt und feiert wie man es am liebsten tut, sondern dass man sich von dem MC auf der Bühne in Echtzeit erklähren lassen muss, wie man anscheinend richtig feiert.   \r\nWenn man dann doch so feiert wie man sich persönlich am besten fühlt, erhascht man auch noch mahnende Blicke von den ganzen Hipstergrüppchen,  \r\ndie einem damit wohl zu verstehen geben wollen, dass man gerade etwas falsch macht weil ja Alle gerade das tun, was der MC von ihnen verlangt.  \r\n    \r\nWas mir dieses Jahr zusätslich extrem aufgefallen ist, sind die horenden Preise, die mittlerweile auf I-Motion Events herrschen.  \r\nWo man früher noch 2 Euro, und bis vor zwei Jahren 3 Euro für sein   Wiedereinlassbändchen gezahlt hat, so sind es heute 4 Euro. Auch die Getränkebons sind  heute im Umtausch 1:1 (1 Bon = 1 Euro).  \r\n  \r\nUnterm Strich bekommt man immer mehr das Gefühl, dass I-Motion ihre Events fast   ausschließlich nur noch als Geldmaschine sehen. Das Line-Up und der allgemeine   Anspruch eines Raves werden immer schlechter.   \r\n  \r\nAls wir dann schon ein paar Stunden am feiern waren, begaben wir uns in den Bereich zwischen den Floors.  \r\nDort wo früher hunderte Bänke, ein paar Essensstände und 3-4 Mercandise Stände waren,   finden sich heute unzählige Ramschläden, die so ziemlich alles verkaufen in das LED\'s eingebaut sind um nutzlos in der Gegend herumzublinken.\r\nIn der überfüllten Halle hat man so kaum noch die Möglichkeit sich einfach so für ein paar Minuten hinzusetzen.   \r\nStattdessen muss man lange warten und hoffen, dass demnächst irgendwo eine der wenigen Bänke frei wird.   \r\n  \r\nFür mich war die Toxicator 2015 die bittere Erkenntniss, dass Genres wie Hardstyle oder Hardcore wohl keine Szenegenres mehr sind und Events wie die Toxicaor oder auch die Mayday immer mehr von Menschen überrannt werden, welche die Kultur dessen was diese Events früher einmal waren, bald mehr oder weniger verdrängen werden.  \r\n  \r\nÜbrig bleiben einem da nur noch die abgeranzten Schuppen, welche man als Raver so lieben gerlernt hat und die Hoffung, dass zumindest diesen Clubs nicht irrgendwann einmal dasselbe passiert.  ','<p><img src=\"https://emf-ev.de/files/toxi-news-bild-klein.JPG\" alt=\"NewsHeader\" /></p>\n<p>Auch dieses Jahr fanden sich wieder tausende feierwütige Menschen in der Maimarkthalle   Mannheim ein, um gemeinsam beim letzten Festival des Jahres ordentlich nach alter   Ravermanier zu feiern.<br />\nAuch ich war dieses Jahr wieder dort, allerdings diesmal nicht ausschließlich nur um  zu feiern,<br />\nsondern auch um mir ein Bild davon zu machen, ob es stimmt weswegen einige alten Hasen der Raverszene in letzter Zeit solche einen Groll hegen.</p>\n<p>Bei der Anfahrt auffällig war dieses Jahr vor allem die Zahl der Polizeikräfte, welche   rund um das Maimarktgelände patrouillierten. Sicher hatte dies vor allem mit der   politischen Situation und den Ereignissen in der Welt zutun, aber wohl auch mit der   Anzahl an Menschen aufgrund der ausverkauften Veranstaltung.</p>\n<p>Die Taschen- sowie Kartenkontrolle fanden dieses Jahr nicht wie in den verganenen   Jahren in der Maimarkthalle, sondern in Zelten auf dem Maimarktgelände statt.<br />\nDies machte allerdings keinen all zu großen Unterschied, es ging wie sonst auch   gewohnt &quot;gemütlich&quot; zu.</p>\n<p>Als wir es dann in die Halle geschafft hatten wurde es ungewohnt eng.<br />\nNach meinen ersten drei Toxicator\'s, 2011, 2012 und 2013, konnte ich 2014 leider nicht dort sein.<br />\naber was sich in diesen einen Jahr so alles ändern kann, hat mich dann doch erschrocken.<br />\nWo in den vergangenen Jahren knapp 8000 bis 9000 (fast ausschließlich nur &quot;Raver&quot;)   gefeiert hatten, waren es in diesem Jahr knapp 12000 Menschen.<br />\nDarunter wie ich leider feststellen musste, unzählige &quot;Hipster&quot; der Generation   Turnbeutel, die sich leider überhaupt nicht an die &quot;Szene anpassten&quot;\nund keine Ahnung hatten, wie man sich auf einer solch einer Veranstaltung zu verhalten   hat.</p>\n<p>So wurde man früher auf den Floors dezent beiseite gebeten bzw. ein wenig geschoben   wenn jemand den Floor verlassen wollte. Heute allerdings muss man ständig damit   rechnen hin und her gedrückt zu werden, von Leuten welche alle 10 Minuten den Floor   wechseln, weil sie es wohl nicht gewohnt sind, dass ein Track auch mal länger als nur   eine Minute Intro, und einer Minute Drop laufen kann.</p>\n<p>Die Aufmachung der Bühnenkonstruktionen war wie in den vergangenen Jahren sehr ansehnlich und entgegen meiner Erwartungen wurde immerhin auf unnütze Pyrotechnik und völlig übertriebene CO2 Kanonen verzichtet.\nAllerdings schien bei den ersten Sets entweder ein sehr gelangweilter, oder garkein Lichtoperator anwesend gewesen zu sein.\nSo blinkte das Licht anfangs nicht nur in den Drop\'s mit immergleichen Lichtszenen, sondern auch in so manchem Intro, was an solchen Stellen vollkommen unpassend war.</p>\n<p>Auch musikalisch waren so manche gespielten Titel eine ganz neue Erfahrung für mich.\nMan selbst steht da und bekommt das Gefühl der Einzige zu sein, der nicht nur da steht um auf den Drop zu warten.<br />\nDie vom MC heruntergeleierten Phrasen, welche man vielleicht von den ganzen    kommerziellen EDM Festivals kennt und die nur dazu dienen das &quot;gemeine Feiervolk&quot;   irgendwie bei Laune zu halten, machten das Ganze auch nicht besser.</p>\n<p>Immerwieder die Aufforderung die Hände in die Luft zu strecken,<br />\ndamit auch diejenigen, welche der Szenemusik nicht sonderlich affin sind, wissen dass   jetzt gerade ein emotionaler Part des Tracks läuft.</p>\n<p>Und als würden die Buld-Up\'s, welche ohnehin früher in diesem typischen EDM Style nie in Genres wie Hardstyle zu hören waren, den Drop nicht ausreichend ankündigen, gibt   der MC natürlich mit der Phrase &quot;3,2,1,Go&quot; (wahlweise auch &quot;3,2,1,Jump&quot;),<br />\nauch dem letzten Menschen auf der Tanzfläche zu verstehen, dass jetzt gleich der Drop beginnt und er doch bitte hysterisch auf und ab hüpfen soll.</p>\n<p>Das ganze führt dazu, dass man sich nicht wie früher von der Musik treiben lässt und feiert wie man es am liebsten tut, sondern dass man sich von dem MC auf der Bühne in Echtzeit erklähren lassen muss, wie man anscheinend richtig feiert.<br />\nWenn man dann doch so feiert wie man sich persönlich am besten fühlt, erhascht man auch noch mahnende Blicke von den ganzen Hipstergrüppchen,<br />\ndie einem damit wohl zu verstehen geben wollen, dass man gerade etwas falsch macht weil ja Alle gerade das tun, was der MC von ihnen verlangt.</p>\n<p>Was mir dieses Jahr zusätslich extrem aufgefallen ist, sind die horenden Preise, die mittlerweile auf I-Motion Events herrschen.<br />\nWo man früher noch 2 Euro, und bis vor zwei Jahren 3 Euro für sein   Wiedereinlassbändchen gezahlt hat, so sind es heute 4 Euro. Auch die Getränkebons sind  heute im Umtausch 1:1 (1 Bon = 1 Euro).</p>\n<p>Unterm Strich bekommt man immer mehr das Gefühl, dass I-Motion ihre Events fast   ausschließlich nur noch als Geldmaschine sehen. Das Line-Up und der allgemeine   Anspruch eines Raves werden immer schlechter.</p>\n<p>Als wir dann schon ein paar Stunden am feiern waren, begaben wir uns in den Bereich zwischen den Floors.<br />\nDort wo früher hunderte Bänke, ein paar Essensstände und 3-4 Mercandise Stände waren,   finden sich heute unzählige Ramschläden, die so ziemlich alles verkaufen in das LED\'s eingebaut sind um nutzlos in der Gegend herumzublinken.\nIn der überfüllten Halle hat man so kaum noch die Möglichkeit sich einfach so für ein paar Minuten hinzusetzen.<br />\nStattdessen muss man lange warten und hoffen, dass demnächst irgendwo eine der wenigen Bänke frei wird.</p>\n<p>Für mich war die Toxicator 2015 die bittere Erkenntniss, dass Genres wie Hardstyle oder Hardcore wohl keine Szenegenres mehr sind und Events wie die Toxicaor oder auch die Mayday immer mehr von Menschen überrannt werden, welche die Kultur dessen was diese Events früher einmal waren, bald mehr oder weniger verdrängen werden.</p>\n<p>Übrig bleiben einem da nur noch die abgeranzten Schuppen, welche man als Raver so lieben gerlernt hat und die Hoffung, dass zumindest diesen Clubs nicht irrgendwann einmal dasselbe passiert.</p>\n','komerzialisierung-der-szene-die-toxicator-2015',1),
	(17,5,'2015-12-17 19:18:17','Electronic Music Talk -   Interview mit Dubstep Künstler \"Artox\"','### Wer sich hierzulande schon einmal mit der Dubstep Szene auseinandergesetzt hat, der wird wohl auch schon von dem DJ und Producer \"Artox\" gehört haben. Wir haben ihn gefragt wie seine Anfänge als Dubstep- Producer und DJ waren.\r\n<br />\r\n<br />\r\n![](https://emf-ev.de/files/artox-bild.JPG)  \r\n<br />\r\n<br />\r\n**Wie hast hast du zu Dubstep gefunden?**    \r\n  \r\n*\"Zum ersten mal wurde mir Dubstep Anfang 2011 von einem guten Freund gezeigt. Ich glaube der erste Song den ich gehört habe war „Skream & Example – Shot Yourself In The Foot Again“. Da ich vor Dubstep fast ausschlieslich HipHop gehört habe, war ich anfangs skeptisch wegen den schon sehr experimentellen Sounds. Allerdings konnte ich der Musik nach und nach immer mehr abgewinnen und habe verstanden, um was es dabei geht, nämlich nur um den Sound. Das ist warscheinlich auch der Grund, warum ich dann selbst angefangen habe, Dubstep zu produzieren. Der Reiz dabei ist, sich von allen „normalen“ Sound-Ästhetiken zu lösen und etwas noch nie Dagewesenes zu schaffen. Ich denke, diese Freiheit hat man bei fast keinem anderen Genre.\"*   \r\n<br />\r\n<br />\r\n<br />\r\n**Seit wann bist du unter dem Synonym \"Artox\" als Dubstep-DJ und Producer unterwegs,   \r\nund wie kamst du auf den Namen?**    \r\n  \r\n*\"Mein erstes Release unter \"Artox\" war im September 2013. Davor hatte ich mich noch  nicht um einen Künstlernamen gekümmert, da ich zu dem Zeitpunkt nur für mich produziert und ausprobiert habe. Der Name hat tatsächlich keine tiefere Bedeutung. Ich weiß auch nichtmehr wirklich, wie ich da drauf gekommen bin.\"*  \r\n<br />\r\n<br />\r\n<br />\r\n**Hast, oder hattest du bestimmte Künstler, die dich inspieriert haben? Wenn ja, welche?**      \r\n  \r\n*\"Zu meiner Anfangszeit war ich großer Fan von Circus Records aus London. Vor allem Flux Pavillion und Doctor P waren große Idole. Heutzutage bin ich sehr beeindruckt von einzigartigen Sounddesignern wie z.B. \"Barely Alive\" von Disciple Records. Außerdem bin ich jeden Tag mindestens eine Stunde auf Soundcloud unterwegs und hole mir da von so vielen verschiedenen Artists Inspiration und Ideen, dass ich diese garnicht genau betiteln kann.\"*  \r\n<br />\r\n<br />\r\n<br />\r\n**Du bist ja auch Drummer, hilft das beim Produzuieren?**  \r\n\r\n*\"Ich denke, dass es zumindest kein Nachteil ist. Vor allem wenn es darum geht schnell eine Idee festzuhalten, ist es gut, auf ein gewisses Repertoire an Rhythmen zugreifen zu können. Auch das spielen in verschiedenen Bands hat mir eventuell geholfen, ein besseres Verständnis für Arrangement und Zusammenspiel von verschiedenen Instrumenten zu verschaffen. Aber in Zeiten von Midi-Sequenzern und Quantisierung kann denke ich jeder mit genug Zeit interessante Rhythmen schaffen, auch ohne Schlagzeuger zu sein.\"*  \r\n<br />\r\n<br />\r\n<br />\r\n**Hast du auch schon eigene Singles released?**    \r\n  \r\n*\"Offiziell releaste Songs hatte ich 2013 auf Walze Records aus Stuttgart. Dieses Jahr habe ich für eine Rock Band einen Remix gemacht der über einen DIY Vertrieb veröffentlich wurde. Ansonsten macht es für mich am meisten Sinn die Songs einfach auf Soundcloud hochzuladen.\"*  \r\n<br />\r\n<iframe width=\"80%\" height=\"250\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/123113526&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>\r\n<br />\r\n<br />  \r\n\r\n**Nun spielst du ja schon seit einiger Zeit immerwieder im Club Lehmann   \r\nund im Zollamt Stuttgart, wie kam es dazu?**\r\n\r\n*\"Ich hatte das Glück, dass der Betreiber von Walze Records auch der Veranstalter der Reihe „Stuttgart Kaputtdubben“ ist. Irgendwann hat er mich einfach angeschrieben und gefragt „Eigentlich Bock aufzulegen?“. Da musste ich nicht lange überlegen.\"*  \r\n<br />\r\n<br />\r\n<br />\r\n**Wie war das erste mal Auflegen im Club für dich?**  \r\n  \r\n*\"Ich war davor natürlich tierisch aufgeregt, den ganzen Abend, bis ich dran war. Als ich dann aber endlich gespielt habe, war das alles weg und ich hab es einfach nur genossen. Auch als ich gemerkt habe, dass das was ich da mach gut ankommt, hat sich das wirklich gut angefühlt. Zugegeben: Es war schon halb 4 morgens und das Publikum war warscheinlich nichtmehr so ganz nüchtern, aber sie haben\'s gefeiert.\"*\r\n<br />\r\n<br />\r\n<br />\r\n**Was bedeutet das Genre \"Dubstep\" für dich?**  \r\n  \r\n*\"Für mich ist Dubstep das einzige Genre, bei dem ich fast jede Woche einen „Wow-Moment“ erlebe, wenn ich neue Tracks anhöre. Die Vielfalt der Subgenres wächst immer weiter und bleibt somit interessant. Es gibt einerseits die etwas populärere Sparte „Brostep“, welche stark von Skrillex geprägt wurde, andererseits geht der Trend zur Zeit wieder mehr Richtung „Riddim“, was mehr dem „Ursprungs-Dubstep“ entspricht. Dabei hat man nie das Gefühl, dass die Entwicklung stehen bleibt. Das macht es für mich sehr spannend ein Teil dieser Szene zu sein.\"*  \r\n<br />\r\n<br />\r\n<br />\r\n**Was würdest du Jungen DJ\'s und Newcomer Produzenten   \r\nmit auf den Weg geben?**  \r\n  \r\n\r\n*\"Das Wichtigste überhaupt ist meiner Meinung nach, dass man sich selbst treu bleibt. Auch wenn jemand das was man macht mal nicht gut findet, darf man sich davon nicht verbiegen lassen. Auch sollte man nicht versuchen wie jemand anderes zu sein. Damit hat man keine Chance sein Potenzial voll auszuschöpfen. Einfach ehrgeizig an dem weiterarbeiten was man macht und aufgeschlossen sein und bleiben. Der Rest ergibt sich mit etwas Glück und Zeit.\"*  \r\n<br />\r\n<br />\r\n<br />\r\n**Zum Schluss noch unsere EMF-Frage:   \r\nWas ist dein absolutes Lieblingsgetränk?**  \r\n  \r\n*\"Eindeutig Gin Tonic\"*','<h3>Wer sich hierzulande schon einmal mit der Dubstep Szene auseinandergesetzt hat, der wird wohl auch schon von dem DJ und Producer &quot;Artox&quot; gehört haben. Wir haben ihn gefragt wie seine Anfänge als Dubstep- Producer und DJ waren.</h3>\n<p><br />\n<br />\n<img src=\"https://emf-ev.de/files/artox-bild.JPG\" alt=\"\" /><br />\n<br />\n<br />\n<strong>Wie hast hast du zu Dubstep gefunden?</strong></p>\n<p><em>&quot;Zum ersten mal wurde mir Dubstep Anfang 2011 von einem guten Freund gezeigt. Ich glaube der erste Song den ich gehört habe war „Skream &amp; Example – Shot Yourself In The Foot Again“. Da ich vor Dubstep fast ausschlieslich HipHop gehört habe, war ich anfangs skeptisch wegen den schon sehr experimentellen Sounds. Allerdings konnte ich der Musik nach und nach immer mehr abgewinnen und habe verstanden, um was es dabei geht, nämlich nur um den Sound. Das ist warscheinlich auch der Grund, warum ich dann selbst angefangen habe, Dubstep zu produzieren. Der Reiz dabei ist, sich von allen „normalen“ Sound-Ästhetiken zu lösen und etwas noch nie Dagewesenes zu schaffen. Ich denke, diese Freiheit hat man bei fast keinem anderen Genre.&quot;</em><br />\n<br />\n<br />\n<br />\n<strong>Seit wann bist du unter dem Synonym &quot;Artox&quot; als Dubstep-DJ und Producer unterwegs,<br />\nund wie kamst du auf den Namen?</strong></p>\n<p><em>&quot;Mein erstes Release unter &quot;Artox&quot; war im September 2013. Davor hatte ich mich noch  nicht um einen Künstlernamen gekümmert, da ich zu dem Zeitpunkt nur für mich produziert und ausprobiert habe. Der Name hat tatsächlich keine tiefere Bedeutung. Ich weiß auch nichtmehr wirklich, wie ich da drauf gekommen bin.&quot;</em><br />\n<br />\n<br />\n<br />\n<strong>Hast, oder hattest du bestimmte Künstler, die dich inspieriert haben? Wenn ja, welche?</strong></p>\n<p><em>&quot;Zu meiner Anfangszeit war ich großer Fan von Circus Records aus London. Vor allem Flux Pavillion und Doctor P waren große Idole. Heutzutage bin ich sehr beeindruckt von einzigartigen Sounddesignern wie z.B. &quot;Barely Alive&quot; von Disciple Records. Außerdem bin ich jeden Tag mindestens eine Stunde auf Soundcloud unterwegs und hole mir da von so vielen verschiedenen Artists Inspiration und Ideen, dass ich diese garnicht genau betiteln kann.&quot;</em><br />\n<br />\n<br />\n<br />\n<strong>Du bist ja auch Drummer, hilft das beim Produzuieren?</strong></p>\n<p><em>&quot;Ich denke, dass es zumindest kein Nachteil ist. Vor allem wenn es darum geht schnell eine Idee festzuhalten, ist es gut, auf ein gewisses Repertoire an Rhythmen zugreifen zu können. Auch das spielen in verschiedenen Bands hat mir eventuell geholfen, ein besseres Verständnis für Arrangement und Zusammenspiel von verschiedenen Instrumenten zu verschaffen. Aber in Zeiten von Midi-Sequenzern und Quantisierung kann denke ich jeder mit genug Zeit interessante Rhythmen schaffen, auch ohne Schlagzeuger zu sein.&quot;</em><br />\n<br />\n<br />\n<br />\n<strong>Hast du auch schon eigene Singles released?</strong></p>\n<p><em>&quot;Offiziell releaste Songs hatte ich 2013 auf Walze Records aus Stuttgart. Dieses Jahr habe ich für eine Rock Band einen Remix gemacht der über einen DIY Vertrieb veröffentlich wurde. Ansonsten macht es für mich am meisten Sinn die Songs einfach auf Soundcloud hochzuladen.&quot;</em><br />\n<br /></p>\n<iframe width=\"80%\" height=\"250\" scrolling=\"no\" frameborder=\"no\" src=\"https://w.soundcloud.com/player/?url=https%3A//api.soundcloud.com/tracks/123113526&amp;auto_play=false&amp;hide_related=false&amp;show_comments=true&amp;show_user=true&amp;show_reposts=false&amp;visual=true\"></iframe>\n<br />\n<br />  \n<p><strong>Nun spielst du ja schon seit einiger Zeit immerwieder im Club Lehmann<br />\nund im Zollamt Stuttgart, wie kam es dazu?</strong></p>\n<p><em>&quot;Ich hatte das Glück, dass der Betreiber von Walze Records auch der Veranstalter der Reihe „Stuttgart Kaputtdubben“ ist. Irgendwann hat er mich einfach angeschrieben und gefragt „Eigentlich Bock aufzulegen?“. Da musste ich nicht lange überlegen.&quot;</em><br />\n<br />\n<br />\n<br />\n<strong>Wie war das erste mal Auflegen im Club für dich?</strong></p>\n<p><em>&quot;Ich war davor natürlich tierisch aufgeregt, den ganzen Abend, bis ich dran war. Als ich dann aber endlich gespielt habe, war das alles weg und ich hab es einfach nur genossen. Auch als ich gemerkt habe, dass das was ich da mach gut ankommt, hat sich das wirklich gut angefühlt. Zugegeben: Es war schon halb 4 morgens und das Publikum war warscheinlich nichtmehr so ganz nüchtern, aber sie haben\'s gefeiert.&quot;</em>\n<br />\n<br />\n<br />\n<strong>Was bedeutet das Genre &quot;Dubstep&quot; für dich?</strong></p>\n<p><em>&quot;Für mich ist Dubstep das einzige Genre, bei dem ich fast jede Woche einen „Wow-Moment“ erlebe, wenn ich neue Tracks anhöre. Die Vielfalt der Subgenres wächst immer weiter und bleibt somit interessant. Es gibt einerseits die etwas populärere Sparte „Brostep“, welche stark von Skrillex geprägt wurde, andererseits geht der Trend zur Zeit wieder mehr Richtung „Riddim“, was mehr dem „Ursprungs-Dubstep“ entspricht. Dabei hat man nie das Gefühl, dass die Entwicklung stehen bleibt. Das macht es für mich sehr spannend ein Teil dieser Szene zu sein.&quot;</em><br />\n<br />\n<br />\n<br />\n<strong>Was würdest du Jungen DJ\'s und Newcomer Produzenten<br />\nmit auf den Weg geben?</strong></p>\n<p><em>&quot;Das Wichtigste überhaupt ist meiner Meinung nach, dass man sich selbst treu bleibt. Auch wenn jemand das was man macht mal nicht gut findet, darf man sich davon nicht verbiegen lassen. Auch sollte man nicht versuchen wie jemand anderes zu sein. Damit hat man keine Chance sein Potenzial voll auszuschöpfen. Einfach ehrgeizig an dem weiterarbeiten was man macht und aufgeschlossen sein und bleiben. Der Rest ergibt sich mit etwas Glück und Zeit.&quot;</em><br />\n<br />\n<br />\n<br />\n<strong>Zum Schluss noch unsere EMF-Frage:<br />\nWas ist dein absolutes Lieblingsgetränk?</strong></p>\n<p><em>&quot;Eindeutig Gin Tonic&quot;</em></p>\n','electronic-music-talk-interview-mit-dubstep-kunstler-artox',1),
	(19,5,'2016-03-02 16:27:29','Eine Schuldisco wird zum Rave','![](https://emf-ev.de/files/news-schuldisco-header.jpg)\r\n\r\nZum zweiten mal starteten die Electronic Music Friends vergangenes Wochenende eine Schuldisco in der *Sophie La Roche Realschule Bönnigheim*, bei der auch mal die Kleinen feiern konnten wie die ganz Großen.\r\n\r\nIn Zusammenarbeit mit der SMV der Realschule wurde so die Aula des Bau I zum Club umgewandelt. So feierten knapp 100 Unter- und Oberstufenschüler ausgelassen bis 22 Uhr zu den Beats von *DJ S.E.S*, *DJ Jam* und dem DJ Duo *Sequence2*. Zum allerersten mal waren auch die zwei Newcomer DJ\'s, *DJ Incarbone* und *Andrew.O* dabei, welche ihr Können erstmalig unter Beweis stellten und die Menge zum Toben brachten.\r\n<br />\r\nDie Schuldisco 2016, die in diesem Jahr unter dem Motto \"Clubbin\' School\" stand, war für Alle ein voller Erfolg und wird voraussichtlich im nächsten Jahr wieder stattfinden.\r\n<br />  \r\n![](https://emf-ev.de/files/news-schuldisco-bild.jpg)\r\n<br />\r\nDie Schuldisco an der Realschule Bönnigheim ist für den Verein EMF auch ein wichtiges Projekt in Sachen Jugendförderung, da man den Jugendlichen die Musik näherbringen kann und ihnen die Möglichkeit bietet, ausgelassen feiern zu können, in gesittetem Rahmen und unter Aufsicht der Lehrer.\r\n \r\nDas Projekt \"Schuldisco\" kam bei den Schülern und Lehrern sehr gut an und wir freuen uns schon auf das nächste Mal!\r\n<br />  \r\n[Hier](https://www.flickr.com/photos/electronic-music-friends/albums/72157662896692183) findest du noch weitere Bilder der Schuldisco 2016.','<p><img src=\"https://emf-ev.de/files/news-schuldisco-header.jpg\" alt=\"\" /></p>\n<p>Zum zweiten mal starteten die Electronic Music Friends vergangenes Wochenende eine Schuldisco in der <em>Sophie La Roche Realschule Bönnigheim</em>, bei der auch mal die Kleinen feiern konnten wie die ganz Großen.</p>\n<p>In Zusammenarbeit mit der SMV der Realschule wurde so die Aula des Bau I zum Club umgewandelt. So feierten knapp 100 Unter- und Oberstufenschüler ausgelassen bis 22 Uhr zu den Beats von <em>DJ S.E.S</em>, <em>DJ Jam</em> und dem DJ Duo <em>Sequence2</em>. Zum allerersten mal waren auch die zwei Newcomer DJ\'s, <em>DJ Incarbone</em> und <em>Andrew.O</em> dabei, welche ihr Können erstmalig unter Beweis stellten und die Menge zum Toben brachten.\n<br />\nDie Schuldisco 2016, die in diesem Jahr unter dem Motto &quot;Clubbin\' School&quot; stand, war für Alle ein voller Erfolg und wird voraussichtlich im nächsten Jahr wieder stattfinden.\n<br /><br />\n<img src=\"https://emf-ev.de/files/news-schuldisco-bild.jpg\" alt=\"\" />\n<br />\nDie Schuldisco an der Realschule Bönnigheim ist für den Verein EMF auch ein wichtiges Projekt in Sachen Jugendförderung, da man den Jugendlichen die Musik näherbringen kann und ihnen die Möglichkeit bietet, ausgelassen feiern zu können, in gesittetem Rahmen und unter Aufsicht der Lehrer.</p>\n<p>Das Projekt &quot;Schuldisco&quot; kam bei den Schülern und Lehrern sehr gut an und wir freuen uns schon auf das nächste Mal!\n<br /><br />\n<a href=\"https://www.flickr.com/photos/electronic-music-friends/albums/72157662896692183\">Hier</a> findest du noch weitere Bilder der Schuldisco 2016.</p>\n','eine-schuldisco-wird-zum-rave',1),
	(20,2,'2016-03-01 18:34:14','Die WinterWorld 2016 in Frankfurt','![](https://emf-ev.de/files/winterworld-header-klein.JPG)\r\n\r\nDer Umzug von Wiesbaden nach Frankfurt war geglückt: \r\nIm Februar 2015 feierte WinterWorld eine gelungene Premiere in Halle 11 der Messe Frankfurt. Mit 16.000 Besuchern war die Veranstaltung restlos ausverkauft. \r\nBei der diesjährigen WinterWorld „burning ice“ am 27. Februar spielten erneut über 20 Top-DJ\'s und Live-Acts auf drei Floors.\r\n\r\nVerschiedene Stile der elektronischer Musik wurden von Künstlern wie „Felix Jaehn“, „Gestört aber GeiL“, „AKA AKA“ oder „Klaudia Gawlas“ präsentiert. \r\nBereits zum 14. Mal fand die WinterWorld statt und ist traditionell\r\ndie erste große Party im Jahr.\r\n\r\nUnser Partyshooter Fabian Lang war für euch vor Ort und hat die besten Impressionen mit seiner Kamera eingefangen!\r\n\r\nDie besten Fotos von der WinterWorld 2016 findet ihr [HIER](https://www.flickr.com/photos/electronic-music-friends/albums/72157665244028546)','<p><img src=\"https://emf-ev.de/files/winterworld-header-klein.JPG\" alt=\"\" /></p>\n<p>Der Umzug von Wiesbaden nach Frankfurt war geglückt:\nIm Februar 2015 feierte WinterWorld eine gelungene Premiere in Halle 11 der Messe Frankfurt. Mit 16.000 Besuchern war die Veranstaltung restlos ausverkauft.\nBei der diesjährigen WinterWorld „burning ice“ am 27. Februar spielten erneut über 20 Top-DJ\'s und Live-Acts auf drei Floors.</p>\n<p>Verschiedene Stile der elektronischer Musik wurden von Künstlern wie „Felix Jaehn“, „Gestört aber GeiL“, „AKA AKA“ oder „Klaudia Gawlas“ präsentiert.\nBereits zum 14. Mal fand die WinterWorld statt und ist traditionell\ndie erste große Party im Jahr.</p>\n<p>Unser Partyshooter Fabian Lang war für euch vor Ort und hat die besten Impressionen mit seiner Kamera eingefangen!</p>\n<p>Die besten Fotos von der WinterWorld 2016 findet ihr <a href=\"https://www.flickr.com/photos/electronic-music-friends/albums/72157665244028546\">HIER</a></p>\n','die-winterworld-2016-in-frankfurt',1),
	(23,5,'2016-05-10 15:14:35','DLRG Party im Freibad Bönnigheim','Vergangenes Wochenende veranstaltete die Jugendabteilung des [DLRG Bönnigheim](http://boennigheim-kirchheim.dlrg-jugend.de/) das erste Rettungsvergleichsturnier im Bönnigheimer Mineralfreibad.\r\nEMF durfte dabei im Anschluss das musikalische Rahmenprogramm des Abends übernehmen.\r\n   \r\n![](https://emf-ev.de/files/dlrg-news-bild-1.jpg)\r\n\r\nAm 7. Mai traten 55 Teams mit insgesamt 250 Teilnehmern der DLRG Ortsgruppen aus der Region bei den Wettkämpfen im Freibad Bönnigheim gegeneinander an. Bereits ab 10:30 Uhr rangen die Teilnehmer in verschiedenen Disziplinen, wie \r\nHindernisstaffel, Puppenrettung, Gurtrettung und Rettungsstaffel um den Sieg.\r\nHerr Bürgermeister Bamberger und der ehemalige Schwimmmeister und Gründer der DLRG Ortsgruppe Bönnigheim/Kirchheim \"Alwin Weinreuter\", die den Wettkampf eröffneten, führten am Abend ebenfalls gemeinsam durch die Siegerehrung.\r\n   \r\nAnschließend wurden noch einmal alle Kraftreserven mobilisiert, um gemeinsam zu den Beats von [DerTonmann](https://www.facebook.com/Tonmannofficial/) und [DJ S.E.S](https://www.facebook.com/djsesofficial/?fref=ts) bis in die Nacht zu feiern. Das EMF-Team hatte schon am Tag zuvor jede Menge Ton- & Lichttechnik aufgebaut, um mit den Teilnehmern am Abend einen gelungenen Wettkampf zu feiern.\r\n  \r\n![](https://emf-ev.de/files/dlrg-news-bild-2.jpg)\r\n  \r\nAlles in Allem waren es schöne Wettkämpfe und ein gelungener Abend.\r\nWir freuen uns schon sehr auf den nächsten Rettungsvergleichswettkampf und auf die kommende Zusammenarbeit mit der Jugendabteilung des DLRG Bönnigheim.\r\n<br />  \r\n[Hier](https://www.flickr.com/photos/electronic-music-friends/albums/72157667844589242) findest du noch weitere Bilder des DLRG Rettungsvergleichsturniers.','<p>Vergangenes Wochenende veranstaltete die Jugendabteilung des <a href=\"http://boennigheim-kirchheim.dlrg-jugend.de/\">DLRG Bönnigheim</a> das erste Rettungsvergleichsturnier im Bönnigheimer Mineralfreibad.\nEMF durfte dabei im Anschluss das musikalische Rahmenprogramm des Abends übernehmen.</p>\n<p><img src=\"https://emf-ev.de/files/dlrg-news-bild-1.jpg\" alt=\"\" /></p>\n<p>Am 7. Mai traten 55 Teams mit insgesamt 250 Teilnehmern der DLRG Ortsgruppen aus der Region bei den Wettkämpfen im Freibad Bönnigheim gegeneinander an. Bereits ab 10:30 Uhr rangen die Teilnehmer in verschiedenen Disziplinen, wie\nHindernisstaffel, Puppenrettung, Gurtrettung und Rettungsstaffel um den Sieg.\nHerr Bürgermeister Bamberger und der ehemalige Schwimmmeister und Gründer der DLRG Ortsgruppe Bönnigheim/Kirchheim &quot;Alwin Weinreuter&quot;, die den Wettkampf eröffneten, führten am Abend ebenfalls gemeinsam durch die Siegerehrung.</p>\n<p>Anschließend wurden noch einmal alle Kraftreserven mobilisiert, um gemeinsam zu den Beats von <a href=\"https://www.facebook.com/Tonmannofficial/\">DerTonmann</a> und <a href=\"https://www.facebook.com/djsesofficial/?fref=ts\">DJ S.E.S</a> bis in die Nacht zu feiern. Das EMF-Team hatte schon am Tag zuvor jede Menge Ton- &amp; Lichttechnik aufgebaut, um mit den Teilnehmern am Abend einen gelungenen Wettkampf zu feiern.</p>\n<p><img src=\"https://emf-ev.de/files/dlrg-news-bild-2.jpg\" alt=\"\" /></p>\n<p>Alles in Allem waren es schöne Wettkämpfe und ein gelungener Abend.\nWir freuen uns schon sehr auf den nächsten Rettungsvergleichswettkampf und auf die kommende Zusammenarbeit mit der Jugendabteilung des DLRG Bönnigheim.\n<br /><br />\n<a href=\"https://www.flickr.com/photos/electronic-music-friends/albums/72157667844589242\">Hier</a> findest du noch weitere Bilder des DLRG Rettungsvergleichsturniers.</p>\n','dlrg-party-im-freibad-bonnigheim',1),
	(24,9,'2016-05-11 20:23:00','Armin Only – Embrace ','![](https://emf-ev.de/files/armin-news-header.JPG)    \r\n  \r\nBereits 2013 waren wir, ein Teil von EMF, auf der Albumtour von Armin van Buuren (Intense). Nach der Veranstaltung waren wir ausnahmslos begeistert von der Show die AvB zum besten gab. \r\n  \r\nKnapp zweieinhalb Jahre später, vergangenen Samstag war es dann wieder soweit. Die neue [Armin-Only-Tour „Embrace“](http://www.arminonly.com/) stand in den Startlöchern. Diesmal war Ich, der Autor, allein vor Ort. Dank einer etwas früheren Anreise konnte meine Gruppe perfekte Plätze ergattern. Perfekter Sound und perfekte Sicht waren also gegeben und dem Spektakel stand nichts mehr im Wege. Das Bier war mit ca. 3 € sogar unerwartet günstig für Events von [Armada](http://www.armadamusic.com/). \r\n  \r\nDoch nun zum wichtigen Thema, der Musik, der Lightshow und den Liveacts.\r\nVertreten waren die Band [Kensington](http://www.kensingtonband.com/), [Mr. Probs](https://www.facebook.com/MrProbz/) (bekannt durch „Waves“ oder „Another you“ ), [Cimo Fränkel](https://www.facebook.com/cimofrankelofficial/?fref=ts) ( „Year Of Summer“) und noch einige weitere Sängerinnen deren Namen mir selbst leider nicht bekannt sind. Jegliche Instrumente der Liveacts und deren Stimmen wurden live in die Show eingebaut. Playback also zum Glück: Fehlanzeige. Nun um es kurz zu machen, alles was eingesungen und eingespielt wurde war wie gewohnt bombastisch. Die Lightshow und die Bühnenshow waren im Vergleich zur „Intense“ etwas dezenter gehalten, aber dennoch einzigartig. Beispielsweise hatte Armin van Buuren zwischenzeitlich sein Mischpult mitten in der Halle, dieses wurde aufwändig während der Show auf- und abgebaut. \r\n  \r\nEine Frage ist jedoch noch offen: Armin van Buuren oder Armin van Bigroom?\r\n„A State Of Trance“ oder kurz „ASOT“, mit diesem Motto wurde van Buuren einst zum gefeierten Superstar. Leider lässt „Embrace“, wie auch schon das gleichnamige Album zu wünschen übrig, jedenfalls was den „echten“ Trance betrifft. Die ersten zwei Stunden des Sets im Ziggodome beinhalteten fast ausschließlich heftige Tomorrowland-Bässe und Aufforderungen zu springen, wenn der Bass einsetzt. Was bei „Intense“ noch der Kern der Show war umfasste diesmal ca. 3 Lieder, von denen ich zwei bereits lange kenne. Eines davon war das Anthem zur diesjährigen ASOT750 (Ben Gold) und das andere war ein Mashup aus „The Tribe“ von Vini Vici mit  weiblichen Vocals, die nicht gerade der Brüller waren. Zum Glück gab es als Zugabe noch ein einstündiges Vinyl-Classic Set, das dafür sorgte, dass doch noch alle glücklich wurden.\r\n  \r\n![](https://emf-ev.de/files/armin-ony-news-1.jpg)\r\nDas Bild zeigt unsere Perspektive.\r\n  \r\nWenn man also Armin van Buuren einmal hören will, weil man „A State Of Trance“ erleben möchte, dann sollte sich mit seinen alten Releases beschäftigen. Zum Leid der Fans wird Armin wohl langsam aber sicher den Weg des Tiesto einschlagen. Es bleibt nur die Frage, ob er selbst damit glücklich ist. Wenn ja dann soll es so sein.','<p><img src=\"https://emf-ev.de/files/armin-news-header.JPG\" alt=\"\" /></p>\n<p>Bereits 2013 waren wir, ein Teil von EMF, auf der Albumtour von Armin van Buuren (Intense). Nach der Veranstaltung waren wir ausnahmslos begeistert von der Show die AvB zum besten gab.</p>\n<p>Knapp zweieinhalb Jahre später, vergangenen Samstag war es dann wieder soweit. Die neue <a href=\"http://www.arminonly.com/\">Armin-Only-Tour „Embrace“</a> stand in den Startlöchern. Diesmal war Ich, der Autor, allein vor Ort. Dank einer etwas früheren Anreise konnte meine Gruppe perfekte Plätze ergattern. Perfekter Sound und perfekte Sicht waren also gegeben und dem Spektakel stand nichts mehr im Wege. Das Bier war mit ca. 3 € sogar unerwartet günstig für Events von <a href=\"http://www.armadamusic.com/\">Armada</a>.</p>\n<p>Doch nun zum wichtigen Thema, der Musik, der Lightshow und den Liveacts.\nVertreten waren die Band <a href=\"http://www.kensingtonband.com/\">Kensington</a>, <a href=\"https://www.facebook.com/MrProbz/\">Mr. Probs</a> (bekannt durch „Waves“ oder „Another you“ ), <a href=\"https://www.facebook.com/cimofrankelofficial/?fref=ts\">Cimo Fränkel</a> ( „Year Of Summer“) und noch einige weitere Sängerinnen deren Namen mir selbst leider nicht bekannt sind. Jegliche Instrumente der Liveacts und deren Stimmen wurden live in die Show eingebaut. Playback also zum Glück: Fehlanzeige. Nun um es kurz zu machen, alles was eingesungen und eingespielt wurde war wie gewohnt bombastisch. Die Lightshow und die Bühnenshow waren im Vergleich zur „Intense“ etwas dezenter gehalten, aber dennoch einzigartig. Beispielsweise hatte Armin van Buuren zwischenzeitlich sein Mischpult mitten in der Halle, dieses wurde aufwändig während der Show auf- und abgebaut.</p>\n<p>Eine Frage ist jedoch noch offen: Armin van Buuren oder Armin van Bigroom?\n„A State Of Trance“ oder kurz „ASOT“, mit diesem Motto wurde van Buuren einst zum gefeierten Superstar. Leider lässt „Embrace“, wie auch schon das gleichnamige Album zu wünschen übrig, jedenfalls was den „echten“ Trance betrifft. Die ersten zwei Stunden des Sets im Ziggodome beinhalteten fast ausschließlich heftige Tomorrowland-Bässe und Aufforderungen zu springen, wenn der Bass einsetzt. Was bei „Intense“ noch der Kern der Show war umfasste diesmal ca. 3 Lieder, von denen ich zwei bereits lange kenne. Eines davon war das Anthem zur diesjährigen ASOT750 (Ben Gold) und das andere war ein Mashup aus „The Tribe“ von Vini Vici mit  weiblichen Vocals, die nicht gerade der Brüller waren. Zum Glück gab es als Zugabe noch ein einstündiges Vinyl-Classic Set, das dafür sorgte, dass doch noch alle glücklich wurden.</p>\n<p><img src=\"https://emf-ev.de/files/armin-ony-news-1.jpg\" alt=\"\" />\nDas Bild zeigt unsere Perspektive.</p>\n<p>Wenn man also Armin van Buuren einmal hören will, weil man „A State Of Trance“ erleben möchte, dann sollte sich mit seinen alten Releases beschäftigen. Zum Leid der Fans wird Armin wohl langsam aber sicher den Weg des Tiesto einschlagen. Es bleibt nur die Frage, ob er selbst damit glücklich ist. Wenn ja dann soll es so sein.</p>\n','armin-only-embrace',1);

/*!40000 ALTER TABLE `tblNews` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblSnippets
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblSnippets`;

CREATE TABLE `tblSnippets` (
  `snippetID` varchar(255) NOT NULL,
  `content` text NOT NULL,
  `contentCached` text,
  `description` text NOT NULL,
  `renderMarkdown` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`snippetID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblSnippets` WRITE;
/*!40000 ALTER TABLE `tblSnippets` DISABLE KEYS */;

INSERT INTO `tblSnippets` (`snippetID`, `content`, `contentCached`, `description`, `renderMarkdown`, `created_at`, `updated_at`)
VALUES
	('DownloadsOverviewDescription','In diesem Bereich findet Ihr immer die aktuellsten Formulare, Infoschreiben, etc.','<p>In diesem Bereich findet Ihr immer die aktuellsten Formulare, Infoschreiben, etc.</p>\n','Beschreibung für die Downloadübersicht',1,'2015-09-13 21:35:05','2015-09-13 21:35:05'),
	('DownloadsOverviewHeadline','Übersicht','Übersicht','Überschrift für die Downloadübersicht',0,'2015-09-13 21:36:12','2015-09-13 21:36:12'),
	('error-401','Du hast Keinen Zutritt auf diese Seite. Entweder du musst <a href=\"#\" class=\"toggle-login-form\">dich anmelden</a> oder dir fehlt die Berechtigung dazu, diese Seite anzusehen.\nFalls Du denkst, dass du die nötigen Berechtigungen für die angeforderten Seite besitzt, melde dich [beim Admin](mailto:admin@emf-ev.de).','<p>Du hast Keinen Zutritt auf diese Seite. Entweder du musst <a href=\"#\" class=\"toggle-login-form\">dich anmelden</a> oder dir fehlt die Berechtigung dazu, diese Seite anzusehen.\nFalls Du denkst, dass du die nötigen Berechtigungen für die angeforderten Seite besitzt, melde dich <a href=\"mailto:admin@emf-ev.de\">beim Admin</a>.</p>\n','Die Fehlermeldung für HTTP 401 - Nicht Autorisiert',1,NULL,NULL),
	('error-403','Der Zutritt auf die von dir Gewünschte URL ist verboten.','<p>Der Zutritt auf die von dir Gewünschte URL ist verboten.</p>\n','Wenn der User versucht ein verbotenes Verzeichnis / eine verbotene Datei zu öffnen',0,NULL,NULL),
	('error-404','Uuuups... Die Seite konnte nicht gefunden werden. Sorry :(','<p>Uuuups... Die Seite konnte nicht gefunden werden. Sorry :(</p>\n','Dei Fehlermeldung, welche beim auftreten eines \"Error 404 - File not found\" auftritt',1,NULL,NULL),
	('error-article-404','Der angeforderte Post konnte nicht gefunden werden.','<p>Der angeforderte Post konnte nicht gefunden werden.</p>\n','Fehlermeldung bei nicht gefundenem Blogpost',1,'2015-09-13 20:37:32','2015-09-13 20:37:32'),
	('error-csrf','Es trat ein Fehler auf beim Übertragen des Formulars. Der CSRF-Token ist ungültig oder nicht vorhanden.\r\n\r\nDieser Fehler kann folgende Ursachen haben:\r\n* Unautorisierter Request\r\n* Übertragungsfehler','<p>Es trat ein Fehler auf beim Übertragen des Formulars. Der CSRF-Token ist ungültig oder nicht vorhanden.</p>\n<p>Dieser Fehler kann folgende Ursachen haben:</p>\n<ul>\n<li>Unautorisierter Request</li>\n<li>Übertragungsfehler</li>\n</ul>\n','Fehlermeldung bei ungültigem CSRF',1,'2015-09-13 20:33:55','2015-09-15 10:24:17'),
	('error-file-404','Die angeforderte Datei konnte nicht gefunden werden.','<p>Die angeforderte Datei konnte nicht gefunden werden.</p>\n','Fehlermeldung bei nicht gefundenem Download',1,'2015-09-13 20:36:29','2015-09-13 20:36:29'),
	('error-head-login','Login Fehlgeschlagen, versuchs\'s einfach nochmal.','Login Fehlgeschlagen, versuchs\'s einfach nochmal.','Login Fehlgeschlagen - Überschrift',0,NULL,NULL),
	('error-loggedin','**Achtung**: Du bist bereits angemeldet. Falls du diesen Bereich dennoch betreten willst, musst du dich zuerst abmelden.','<p><strong>Achtung</strong>: Du bist bereits angemeldet. Falls du diesen Bereich dennoch betreten willst, musst du dich zuerst abmelden.</p>\n','Fehlermeldung, falls der Benutzer versucht sich erneut Anzumelden oder zu Registrieren',1,'2015-09-15 10:52:22','2015-09-15 10:52:22'),
	('error-login','Deine Logindaten sind Falsch. Bitte überprüfe deinen Benutzernamen und dein Passwort. Falls dieser Fehler zum ersten mal auftritt wende dich bitte an den [Admin](mailto:admin@emf-ev.de).\n\n**Hinweis:** Falls du dich gerade vom Dashboard aus abgemeldet hast ignoriere diese Nachricht einfach.','<p>Deine Logindaten sind Falsch. Bitte überprüfe deinen Benutzernamen und dein Passwort. Falls dieser Fehler zum ersten mal auftritt wende dich bitte an den <a href=\"mailto:admin@emf-ev.de\">Admin</a>.</p>\n<p><strong>Hinweis:</strong> Falls du dich gerade vom Dashboard aus abgemeldet hast ignoriere diese Nachricht einfach.</p>\n','Error bei Fehlerhaften Logindaten',1,NULL,NULL),
	('FooterGoogleAnalytics','(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\nm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\nga(\'create\', \'UA-39107403-2\', \'auto\');\nga(\'require\', \'displayfeatures\');\nga(\'send\', \'pageview\');\n','(function(i,s,o,g,r,a,m){i[\'GoogleAnalyticsObject\']=r;i[r]=i[r]||function(){\n(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),\nm=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)\n})(window,document,\'script\',\'//www.google-analytics.com/analytics.js\',\'ga\');\nga(\'create\', \'UA-39107403-2\', \'auto\');\nga(\'require\', \'displayfeatures\');\nga(\'send\', \'pageview\');\n','Der Google-Analytics Tracking Snippet',0,NULL,NULL),
	('FooterInfoText','[Kontakt](/where) | [Impressum](/impressum) | [Registrieren](/user/register)  \r\n© Electronic Music Friends e.V. 2013 - 2016','<p><a href=\"/where\">Kontakt</a> | <a href=\"/impressum\">Impressum</a> | <a href=\"/user/register\">Registrieren</a><br />\n© Electronic Music Friends e.V. 2013 - 2016</p>\n','Der Infotext im Footer der Website',1,NULL,'2016-01-27 22:40:39'),
	('HomeColumnFirstContent','Electronic Music Friends besteht aus motivierten, jungen Leuten, die eines gemeinsam haben:  \r\n**Die Liebe zur elektronischen Tanzmusik**\r\n\r\nOb Trance, Hardstyle, House, Minimal, Tech, Dubstep oder Hands Up.\r\nWir feiern alles, was \"drückt\".\r\n\r\nWenn Du also auch auf Party, abgefahrene Locations, Open Air Events, den Duft von Nebelfluid in der Nase\r\nund lauter Musik im Ohr stehst, dann bist du bei uns genau richtig!\r\n\r\nDu bist **DJ**, **LJ**, **Fotograf** oder **Filmer**?  \r\nDu möchtest vielleicht **selbst ein Event über den Verein organisieren**?  \r\nOder liebst du einfach elektronische Musik und bist auf der Suche nach **Gleichgesinnten**?\r\n\r\nEgal wie, bei uns ist jede Art von Unterstützung herzlich Willkommen!\r\n','<p>Electronic Music Friends besteht aus motivierten, jungen Leuten, die eines gemeinsam haben:<br />\n<strong>Die Liebe zur elektronischen Tanzmusik</strong></p>\n<p>Ob Trance, Hardstyle, House, Minimal, Tech, Dubstep oder Hands Up.\nWir feiern alles, was &quot;drückt&quot;.</p>\n<p>Wenn Du also auch auf Party, abgefahrene Locations, Open Air Events, den Duft von Nebelfluid in der Nase\nund lauter Musik im Ohr stehst, dann bist du bei uns genau richtig!</p>\n<p>Du bist <strong>DJ</strong>, <strong>LJ</strong>, <strong>Fotograf</strong> oder <strong>Filmer</strong>?<br />\nDu möchtest vielleicht <strong>selbst ein Event über den Verein organisieren</strong>?<br />\nOder liebst du einfach elektronische Musik und bist auf der Suche nach <strong>Gleichgesinnten</strong>?</p>\n<p>Egal wie, bei uns ist jede Art von Unterstützung herzlich Willkommen!</p>\n','Der Inhalt der ersten Spalte auf der \"Infos zu uns\" - Seite',1,NULL,'2015-09-22 16:21:42'),
	('HomeColumnFirstHead','Die Community','Die Community','Die Überschrift der ersten Spalte auf der \"Infos zu uns\" - Seite',0,NULL,NULL),
	('HomeColumnSecondContent','Keine Angst, wir sind alles andere, als ein klischeehafter Verein.\r\nWir sind jung, dynamisch und für jeden Rave zu haben.\r\n\r\nIm Verein hilfst du bei der Organisation und Planung von Events, kannst bei jedem Rave aktiv mitwirken und\r\nbis immer \"up to date\".\r\n  \r\nDeine Vorteile im Verein:  \r\n* Als **DJ** hast du die Möglichkeit auf Veranstaltungen zu spielen oder deinen Style im \"EMF Podcast\" zu präsentieren.\r\n* Du bekommst **Nachlässe** auf *Eintritte und Getränke*. Außerdem gibt\'s für Mitglieder einige **Specials**, wie Zugang zu *Backstagebereichen* und *Friend\'s Areas* (VIP Areas für Mitglieder).\r\n* Du hast Zugriff auf jede Menge professionelle **Ton- und Lichttechnik**, die dem Verein gehört.\r\n* **Wissensaustausch** und Hilfe bei allen Themen, die mit elektronischer Musik und Technik zu tun haben.','<p>Keine Angst, wir sind alles andere, als ein klischeehafter Verein.\nWir sind jung, dynamisch und für jeden Rave zu haben.</p>\n<p>Im Verein hilfst du bei der Organisation und Planung von Events, kannst bei jedem Rave aktiv mitwirken und\nbis immer &quot;up to date&quot;.</p>\n<p>Deine Vorteile im Verein:</p>\n<ul>\n<li>Als <strong>DJ</strong> hast du die Möglichkeit auf Veranstaltungen zu spielen oder deinen Style im &quot;EMF Podcast&quot; zu präsentieren.</li>\n<li>Du bekommst <strong>Nachlässe</strong> auf <em>Eintritte und Getränke</em>. Außerdem gibt\'s für Mitglieder einige <strong>Specials</strong>, wie Zugang zu <em>Backstagebereichen</em> und <em>Friend\'s Areas</em> (VIP Areas für Mitglieder).</li>\n<li>Du hast Zugriff auf jede Menge professionelle <strong>Ton- und Lichttechnik</strong>, die dem Verein gehört.</li>\n<li>\n<strong>Wissensaustausch</strong> und Hilfe bei allen Themen, die mit elektronischer Musik und Technik zu tun haben.</li>\n</ul>\n','Der Inhalt der zweiten Spalte auf der \"Infos zu uns\" - Seite',1,NULL,'2015-09-22 16:29:25'),
	('HomeColumnSecondHead','Wofür?!','Wofür?!','Die Überschrift der zweiten Spalte auf der \"Infos zu uns\" - Seite',0,NULL,'2015-09-24 15:20:59'),
	('HomeColumnThirdContent','Der Name ist Programm!\r\n\r\nImmer auf der Suche nach neuen Locations und Kooperationen mit anderen Veranstaltern, Clubs und Bars, veranstalten wir elektronische Events rund um Stuttgart und Heilbronn.\r\n    \r\nUm diese Events zu organisieren reicht purer Elan allerdings nicht ganz aus.  \r\nDeshalb suchen wir **Dich!**\r\n\r\nJa genau, *Dich*. Denn jede helfende Hand bringt uns unserem Ziel, eine frische Community für die elektronische Tanzmusik zu etablieren einen enormen Schritt näher.\r\n\r\nWenn du den Verein und dessen Jugendarbeit unterstützen möchstest, dann schau bei unserem Shop vorbei! Jeder Artikel ist von uns designed und spühlt eine kleine Provision in unsere Jugendkasse.\r\n\r\nHaben wir dein Interesse geweckt?  \r\n**Melde dich per Mail** an  \r\n[vorstand@emf-ev.de](mailto:vorstand@emf-ev.de).  \r\n','<p>Der Name ist Programm!</p>\n<p>Immer auf der Suche nach neuen Locations und Kooperationen mit anderen Veranstaltern, Clubs und Bars, veranstalten wir elektronische Events rund um Stuttgart und Heilbronn.</p>\n<p>Um diese Events zu organisieren reicht purer Elan allerdings nicht ganz aus.<br />\nDeshalb suchen wir <strong>Dich!</strong></p>\n<p>Ja genau, <em>Dich</em>. Denn jede helfende Hand bringt uns unserem Ziel, eine frische Community für die elektronische Tanzmusik zu etablieren einen enormen Schritt näher.</p>\n<p>Wenn du den Verein und dessen Jugendarbeit unterstützen möchstest, dann schau bei unserem Shop vorbei! Jeder Artikel ist von uns designed und spühlt eine kleine Provision in unsere Jugendkasse.</p>\n<p>Haben wir dein Interesse geweckt?<br />\n<strong>Melde dich per Mail</strong> an<br />\n<a href=\"mailto:vorstand@emf-ev.de\">vorstand@emf-ev.de</a>.</p>\n','Der Inhalt der dritten Spalte auf der \"Infos zu uns\" - Seite',1,NULL,'2015-12-03 16:02:11'),
	('HomeColumnThirdHead','Sei Dabei!','Sei Dabei!','Die Überschrift der dritten Spalte auf der \"Infos zu uns\" - Seite',0,NULL,'2015-09-24 15:21:11'),
	('ImprintFirstContent','Electronic Music Friends Bönnigheim e.V.  \nNähere Kontaktdaten sind [hier zu finden](/where).  \nGesetzlich Vertreten durch: Matthias Teifl, Andre Michelberger, Silvia Teifl','<p>Electronic Music Friends Bönnigheim e.V.<br />\nNähere Kontaktdaten sind <a href=\"/where\">hier zu finden</a>.<br />\nGesetzlich Vertreten durch: Matthias Teifl, Andre Michelberger, Silvia Teifl</p>\n','Impressum, obere Box',1,NULL,NULL),
	('ImprintSecondContent','Eintragung im Vereinsregister:    \nAmtsgericht Besigheim  \nRegisternummer: VR300780  \nRechtsform: Verein (Gemeinnützig)  \nGründung: 15.05.2013  ','<p>Eintragung im Vereinsregister:<br />\nAmtsgericht Besigheim<br />\nRegisternummer: VR300780<br />\nRechtsform: Verein (Gemeinnützig)<br />\nGründung: 15.05.2013</p>\n','Impressum, zweite Box',1,NULL,NULL),
	('meta-description','Electronic Music Friends ist ein seit 2013 eingetragener Verein. Unser Ziel besteht hauptsächlich darin eine Community im Kreis Stuttgart für Freunde der Elektronischen Tanzmusik zu etablieren.','Electronic Music Friends ist ein seit 2013 eingetragener Verein. Unser Ziel besteht hauptsächlich darin eine Community im Kreis Stuttgart für Freunde der Elektronischen Tanzmusik zu etablieren.','Meta Beschreibung',0,'2015-09-13 16:44:10','2015-09-13 16:44:10'),
	('meta-keywords','electronic music, musikverein, musik, elektronische musik, tanzmusik, tanzverein, verein bönnigheim, party stuttgart','electronic music, musikverein, musik, elektronische musik, tanzmusik, tanzverein, verein bönnigheim, party stuttgart','Meta Kommagetrennte Schlüsselwörter',0,'2015-09-13 16:44:54','2015-09-13 16:44:54'),
	('meta-title','Electronic Music Friends e.V.','Electronic Music Friends e.V.','Meta Titel',0,'2015-09-13 16:43:15','2015-09-13 16:43:15'),
	('NewsArticleDate','am','am','Newsartikel: \"am\"',0,NULL,NULL),
	('NewsArticleFrom','Von','Von','Newsartikel: \"Von\"',0,NULL,NULL),
	('RegisterWelcomeText','Die Registrierung steht nur Vereinsmitgliedern zur Verfügung! Wenn du deinen Account registrieren willst, dann fülle bitte folgendes Formular aus:','Die Registrierung steht nur Vereinsmitgliedern zur Verfügung! Wenn du deinen Account registrieren willst, dann fülle bitte folgendes Formular aus:','Begrüßungstext bei der Registrierung',0,NULL,'2015-09-20 21:21:17'),
	('TopbarLinkDownloads','Downloads','Downloads','Der Link auf \"Downloads\"',0,'2015-09-13 16:39:38','2015-09-13 16:39:38'),
	('TopbarLinkHome','Infos zum Verein','Infos zum Verein','Der Link auf \"Infos zu Uns\"',0,NULL,'2015-09-15 10:48:51'),
	('TopbarLinkMusic','Mixcloud','Mixcloud','Der Link auf \"Musik / Mixcloud\"',0,'2015-11-19 17:21:40','2015-11-19 17:21:40'),
	('TopbarLinkNews','News','News','Der Link auf \"News\"',0,NULL,NULL),
	('TopbarLinkPics','Galerie','Galerie','Der Link auf \"Pics\"',0,NULL,NULL),
	('TopbarLinkShop','Shop','Shop','Der Link auf \"Shop\"',0,NULL,NULL),
	('TopbarLinkWhere','Wo Du uns findest','Wo Du uns findest','Der Link auf \"Wo Ihr uns findet\"',0,NULL,NULL),
	('WhereContentResponsible','Matthias Teifl  \nHinter den Gärten 7  \n74357 Bönnigheim  \n<i class=\"icon-phone\"></i> 07143/4079970  \n<i class=\"icon-envelope\"></i> [der.tonmann@web.de](mailto:der.tonmann@web.de)','<p>Matthias Teifl<br />\nHinter den Gärten 7<br />\n74357 Bönnigheim<br />\n<i class=\"icon-phone\"></i> 07143/4079970<br />\n<i class=\"icon-envelope\"></i> <a href=\"mailto:der.tonmann@web.de\">der.tonmann@web.de</a></p>\n','Der Verantwortliche des Inhalts auf der \"Wo Ihr uns findet\" - Seite',1,NULL,NULL),
	('WhereHead','# <i class=\"icon-map-marker\"></i> Wo Du uns findest','<h1><i class=\"icon-map-marker\"></i> Wo Du uns findest</h1>\n','Die Überschrift der \"Wo Ihr uns findet\" - Seite',1,NULL,NULL),
	('WhereInfoText','**Wir haben dein Interesse geweckt?**\r\n\r\nDann melde dich bei uns! Egal was du machen willst, wir haben immer Platz für neue Mitglieder.  \r\nUm dich anzumelden setze dich mit uns in Verbindung. Melde dich dazu einfach bei [Matthias Teifl](mailto:vorstand@emf-ev.de).','<p><strong>Wir haben dein Interesse geweckt?</strong></p>\n<p>Dann melde dich bei uns! Egal was du machen willst, wir haben immer Platz für neue Mitglieder.<br />\nUm dich anzumelden setze dich mit uns in Verbindung. Melde dich dazu einfach bei <a href=\"mailto:vorstand@emf-ev.de\">Matthias Teifl</a>.</p>\n','Der Infotext auf der \"Wo Ihr uns findet\" - Seite',1,NULL,'2015-11-19 17:48:50'),
	('WhereSubheadline','## Generelle Infos zu Veranstaltungen und anderen Events findest Du in unseren [News](/news)!  \n## Weitere Neuigkeiten gibt\'s auch auf [Facebook](https://www.facebook.com/EMFeV)! ','<h2>Generelle Infos zu Veranstaltungen und anderen Events findest Du in unseren <a href=\"/news\">News</a>!</h2>\n<h2>Weitere Neuigkeiten gibt\'s auch auf <a href=\"https://www.facebook.com/EMFeV\">Facebook</a>!</h2>\n','Die Zweite Überschrift der \"Wo Ihr uns findet\" - Seite',1,NULL,NULL),
	('WhereTechResponsible','Sebastian Langer  \nTegeler Weg 9b  \n10589 Berlin   \n<i class=\"icon-envelope\"></i> [screeny05@gmail.com](mailto:screeny05@gmail.com)','<p>Sebastian Langer<br />\nTegeler Weg 9b<br />\n10589 Berlin<br />\n<i class=\"icon-envelope\"></i> <a href=\"mailto:screeny05@gmail.com\">screeny05@gmail.com</a></p>\n','Der Technische Verantwortliche auf der \"Wo Ihr uns findet\" - Seite',1,NULL,NULL);

/*!40000 ALTER TABLE `tblSnippets` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblUser
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblUser`;

CREATE TABLE `tblUser` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `groupID` varchar(255) DEFAULT NULL,
  `facebookID` varchar(255) DEFAULT NULL,
  `firstname` varchar(255) NOT NULL,
  `lastname` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `gender` tinyint(4) DEFAULT NULL,
  `birthday` date DEFAULT NULL,
  `active` tinyint(1) NOT NULL DEFAULT '0',
  `password_hash` varchar(255) NOT NULL,
  `session_id` varchar(255) DEFAULT NULL,
  `usedPin` varchar(255) DEFAULT NULL,
  `zip` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `street` varchar(255) DEFAULT NULL,
  `telephone` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  PRIMARY KEY (`userID`),
  UNIQUE KEY `tblUser_u_ce4c89` (`email`),
  KEY `tblUser_fi_e6d3be` (`groupID`),
  CONSTRAINT `tblUser_fk_e6d3be` FOREIGN KEY (`groupID`) REFERENCES `tblAccessGroups` (`groupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

LOCK TABLES `tblUser` WRITE;
/*!40000 ALTER TABLE `tblUser` DISABLE KEYS */;

INSERT INTO `tblUser` (`userID`, `groupID`, `facebookID`, `firstname`, `lastname`, `email`, `gender`, `birthday`, `active`, `password_hash`, `session_id`, `usedPin`, `zip`, `city`, `street`, `telephone`, `created_at`, `updated_at`)
VALUES
	(1,'admin',NULL,'Firstname','Lastname','mail@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2015-09-15 10:03:25','2016-05-13 12:15:55'),
	(2,'admin',NULL,'Firstname','Lastname','mail+1@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2015-09-15 13:45:11','2016-05-15 16:51:11'),
	(3,'user',NULL,'Firstname','Lastname','mail+2@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2015-09-20 19:21:01','2016-05-13 18:58:23'),
	(4,'user',NULL,'Firstname','Lastname','mail+3@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2015-09-20 19:21:52','2016-05-14 14:35:08'),
	(5,'admin',NULL,'Firstname','Lastname','mail+4@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2015-09-20 21:19:21','2016-05-13 16:42:57'),
	(6,'user',NULL,'Firstname','Lastname','mail+5@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2015-09-22 19:49:13','2016-02-15 16:05:21'),
	(7,'user',NULL,'Firstname','Lastname','mail+6@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2015-09-23 16:56:39','2016-04-25 17:24:56'),
	(8,'user',NULL,'Firstname','Lastname','mail+7@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2015-09-23 16:57:34','2015-09-27 19:06:23'),
	(9,'author',NULL,'Firstname','Lastname','mail+8@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2016-01-16 19:41:20','2016-05-11 20:17:02'),
	(10,'user',NULL,'Firstname','Lastname','mail+9@example.com',1,'1970-01-01',1,'$2a$04$3WH4/a3vPKZDqOOi/95wf.OhaCGBmZY5ffy3YqFsBTnw7JPspJpPe','[redacted]',null,'[redacted]','[redacted]','[redacted]','[redacted]','2016-03-28 21:23:34','2016-03-28 21:23:34');

/*!40000 ALTER TABLE `tblUser` ENABLE KEYS */;
UNLOCK TABLES;


# Export von Tabelle tblUserPin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblUserPin`;

CREATE TABLE `tblUserPin` (
  `pin` varchar(255) NOT NULL,
  `groupID` varchar(255) DEFAULT NULL,
  `createMailAccount` tinyint(1) DEFAULT '0',
  `created_at` datetime DEFAULT NULL,
  `email` varchar(255) DEFAULT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`pin`),
  KEY `tblUserPin_fi_e6d3be` (`groupID`),
  CONSTRAINT `tblUserPin_fk_e6d3be` FOREIGN KEY (`groupID`) REFERENCES `tblAccessGroups` (`groupID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;


# Export von Tabelle tblSponsors
# ------------------------------------------------------------

DROP TABLE IF EXISTS `tblSponsors`;

CREATE TABLE `tblSponsors`
(
    `sponsorID` INTEGER NOT NULL AUTO_INCREMENT,
    `file` VARCHAR(255),
    `position` INTEGER DEFAULT 1,
    `name` VARCHAR(255) NOT NULL,
    `link` VARCHAR(255),
    `description` TEXT,
    `description_cached` TEXT,
    `created_at` DATETIME,
    `updated_at` DATETIME,
    PRIMARY KEY (`sponsorID`)
) ENGINE=InnoDB;


/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
