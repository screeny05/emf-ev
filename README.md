# emf-ev.org

## License
[GNU AGPL](LICENSE.txt)

## Requirements
* gulp
* bower
* bundler
* compass
* sass-css-importer
* ceaser-easing
* composer
* nginx/apache - others may also work
* hhvm - php not tested
* dbms supported by [propel](http://propelorm.org)

## Installation
```bash
npm install
nano include/config.json
gulp
```

## db data
run propel to generate the tables
```bash
vendor/propel/propel/bin/propel sql:build --config-dir include/ --schema-dir include
```
now you're able to run the sql-queries in `generated-sql/emf.sql` and `sql-export.sql`
