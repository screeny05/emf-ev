<?xml version="1.0" encoding="UTF-8"?>
<database name="emf" defaultIdMethod="native">

    <table name="tblNews" phpName="News">
        <column name="newsID" phpName="NewsID" type="integer" required="true" primaryKey="true" autoIncrement="true"/>
        <column name="userID" phpName="UserID" type="integer" required="true"/>
        <column name="published" type="timestamp" required="true"/>
        <column name="title" type="longvarchar" required="true"/>
        <column name="author" type="varchar" required="false"/>
        <column name="content" type="longvarchar" required="true"/>
        <column name="content_cached" type="longvarchar"/>
        <column name="seo_title" type="varchar" required="true"/>
        <column name="listed" type="boolean" defaultValue="false" required="true"/>

        <foreign-key foreignTable="tblUser" phpName="User" refPhpName="News">
            <reference local="userID" foreign="userID"/>
        </foreign-key>

        <unique>
            <unique-column name="seo_title"/>
        </unique>
    </table>

    <table name="tblUser" phpName="User">
        <column name="userID" phpName="UserID" type="integer" required="true" primaryKey="true" autoIncrement="true"/>
        <column name="groupID" phpName="GroupID" type="varchar"/>
        <column name="facebookID" phpName="FacebookID" type="varchar"/>
        <column name="firstname" type="varchar" required="true"/>
        <column name="lastname" type="varchar" required="true"/>
        <column name="email" type="varchar" required="true"/>
        <column name="gender" type="enum" valueSet="female,male"/>
        <column name="birthday" type="date"/>

        <column name="active" type="boolean" defaultValue="false" required="true"/>
        <column name="password_hash" type="varchar" required="true"/>
        <column name="session_id" type="varchar"/>
        <column name="usedPin" type="varchar"/>

        <column name="zip" type="varchar" required="true"/>
        <column name="city" type="varchar" required="true"/>
        <column name="street" type="varchar"/>
        <column name="telephone" type="varchar"/>

        <foreign-key foreignTable="tblAccessGroups" phpName="Group" refPhpName="User">
            <reference local="groupID" foreign="groupID"/>
        </foreign-key>

        <unique>
            <unique-column name="email"/>
        </unique>

        <behavior name="timestampable"/>
    </table>

    <table name="tblSnippets" phpName="Snippet">
        <column name="snippetID" phpName="SnippetID" type="varchar" required="true" primaryKey="true"/>
        <column name="content" type="longvarchar" required="true"/>
        <column name="contentCached" phpName="contentCached" type="longvarchar" primaryString="true"/>
        <column name="description" type="longvarchar" required="true"/>
        <column name="renderMarkdown" phpName="RenderMarkdown" type="boolean" defaultValue="false" required="true"/>

        <behavior name="timestampable"/>
    </table>

    <table name="tblUserPin" phpName="Pin">
        <column name="pin" type="varchar" required="true" primaryKey="true"/>
        <column name="groupID" phpName="GroupID" type="varchar"/>
        <column name="email" type="varchar"/>
        <column name="name" type="varchar" required="true"/>
        <column name="createMailAccount" phpName="CreateMailAccount" type="boolean" defaultValue="false"/>

        <foreign-key foreignTable="tblAccessGroups" phpName="Group">
            <reference local="groupID" foreign="groupID"/>
        </foreign-key>

        <behavior name="timestampable">
            <parameter name="disable_updated_at" value="true"/>
        </behavior>
    </table>

    <table name="tblFiles" phpName="File">
        <column name="fileID" phpName="FileID" type="integer" primaryKey="true" autoIncrement="true"/>
        <column name="userID" phpName="UserID" type="integer" required="true"/>
        <column name="folderID" phpName="FolderID" type="integer" defaultValue="0"/>
        <column name="path" type="varchar" required="true"/>
        <column name="name" type="varchar" required="true"/>
        <column name="size" type="integer" required="true"/>
        <column name="icon" type="varchar"/>
        <column name="title" type="varchar" required="true"/>
        <column name="extension" type="varchar" required="true"/>
        <column name="type" type="enum" valueSet="audio,video,image,text,misc"/>
        <column name="description" type="longvarchar"/>
        <column name="description_cached" type="longvarchar"/>

        <foreign-key foreignTable="tblUser" phpName="User" refPhpName="Files">
            <reference local="userID" foreign="userID"/>
        </foreign-key>

        <foreign-key foreignTable="tblFileFolders" phpName="Folder" refPhpName="Files">
            <reference local="folderID" foreign="folderID"/>
        </foreign-key>

        <behavior name="timestampable"/>
    </table>

    <table name="tblFileFolders" phpName="FileFolder">
        <column name="folderID" phpName="FolderID" type="integer" primaryKey="true" autoIncrement="true"/>
        <column name="name" type="varchar" required="true"/>
        <column name="public" type="boolean" defaultValue="true"/>
    </table>

    <table name="tblAccessRoles" phpName="AccessRole">
        <column name="roleID" phpName="RoleID" type="varchar" primaryKey="true"/>
        <column name="title" type="varchar" required="true" primaryString="true"/>
    </table>

    <table name="tblAccessGroups" phpName="AccessGroup">
        <column name="groupID" phpName="GroupID" type="varchar" primaryKey="true"/>
        <column name="title" type="varchar" required="true" primaryString="true"/>
        <column name="admin" type="boolean" defaultValue="false"/>
    </table>

    <table name="nnGroupsRoles" phpName="GroupRole" isCrossRef="true">
        <column name="groupID" type="varchar" primaryKey="true"/>
        <column name="roleID" type="varchar" primaryKey="true"/>

        <foreign-key foreignTable="tblAccessGroups">
            <reference local="groupID" foreign="groupID"/>
        </foreign-key>
        <foreign-key foreignTable="tblAccessRoles">
            <reference local="roleID" foreign="roleID"/>
        </foreign-key>
    </table>

    <table name="tblConfig" phpName="Config">
        <column name="configID" phpName="ConfigID" type="varchar" required="true" primaryKey="true"/>
        <column name="value" type="longvarchar" required="true" primaryString="true"/>

        <!-- the domainId-field gives the optional ability to localize
            a config option to a domain, e.g.: a user or a role -->
        <column name="domainID" phpName="DomainID" type="integer"/>

        <behavior name="timestampable"/>
    </table>

    <table name="tblSponsors" phpName="Sponsors">
        <column name="sponsorID" phpName="SponsorID" type="integer" primaryKey="true" autoIncrement="true"/>
        <column name="file" type="varchar" required="true"/>
        <column name="position" type="integer" defaultValue="1"/>
        <column name="name" type="varchar" required="true" primaryString="true"/>
        <column name="link" type="varchar"/>
        <column name="description" type="longvarchar"/>
        <column name="description_cached" type="longvarchar"/>

        <behavior name="timestampable"/>
    </table>
</database>
