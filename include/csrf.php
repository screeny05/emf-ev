<?php
class CsrfProtector {
    const TOKEN_PREFIX = "c_";
    const SESSION_INDEX = "csrf-token";

    public static function validate($token){
        return array_key_exists(self::SESSION_INDEX, $_SESSION) && $_SESSION[self::SESSION_INDEX] === $token;
    }
    public static function get(){
        if(!array_key_exists(self::SESSION_INDEX, $_SESSION)){
            $_SESSION[self::SESSION_INDEX] = self::TOKEN_PREFIX . self::getRandomString();
        }
        return $_SESSION[self::SESSION_INDEX];
    }
    public static function getRandomString($length = 20){
        $dict = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890s";
        $dictLength = strlen($dict);
        $r = "";
        for($i = 0; $i < $length; $i++){
            $r .= $dict[mt_rand(0, $dictLength - 1)];
        }
        return $r;
    }
    public static function route(){
        return function($req, $res){
            $token = array_key_exists('ctoken', $req->query) ? $req->query['ctoken'] : false;
            if(!$token || !self::validate($token)){
                $res->status(401);
                $res->renderError(false, SnippetQuery::getOne('error-csrf'));
                $res->lock();
            }
        };
    }
}
