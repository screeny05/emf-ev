<?php

// config
require_once('vendor/autoload.php');
require_once("include/db.php");

// libraries
require_once("include/utils.php");
require_once("include/hash.php");
require_once("include/csrf.php");
//require_once("include/markdown.php");

// controllers
require_once("controller/Route.php");
require_once("controller/Router.php");
require_once("controller/Request.php");
require_once("controller/Response.php");

// routes
require_once("routes/api.php");
require_once("routes/debug.php");
require_once("routes/permission.php");
require_once("routes/static.php");
require_once("routes/catchall.php");
require_once("routes/error.php");

// full-routes
require_once("routes/admin.php");


Valitron\Validator::addRule('validPin', function($field, $value, array $params) {
    return count((new PinQuery())->findByPin($value)) > 0;
}, 'Ungültiger PIN.');

Valitron\Validator::addRule('unusedPin', function($field, $value, $params) {
    return count((new PinQuery())->findByPin($value)) == 0;
}, 'PIN bereits in der Datenbank vorhanden');

Valitron\Validator::addRule('unusedSnippetId', function($field, $value, $params) {
    return count((new SnippetQuery())->findPk($value)) == 0;
}, 'SnippetId bereits in der Datenbank vorhanden');

Valitron\Validator::addRule('unusedEmail', function($field, $value, array $params) {
    return count((new UserQuery())->findByEmail($value)) == 0;
}, 'Email-Adresse bereits in Benutzung.');
