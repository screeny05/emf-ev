<?php

use Base\SponsorsQuery as BaseSponsorsQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'tblSponsors' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SponsorsQuery extends BaseSponsorsQuery
{
    public static function consolidatePosition(){
        $sponsors = self::create()
            //->orderByName()
            ->orderByPosition(DESC)
            ->find();
        $i = 0;

        foreach($sponsors as $sponsor){
            $sponsor->setPosition($i);
            $sponsor->save();
            $i++;
        }
    }
}
