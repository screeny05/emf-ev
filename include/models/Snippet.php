<?php

use Base\Snippet as BaseSnippet;

/**
 * Skeleton subclass for representing a row from the 'tblSnippets' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Snippet extends BaseSnippet
{
    public static function getValidator($data, $update = false){
        $v = new Valitron\Validator($data);

        $requiredFields = ['Content'];
        if(!$update){
            $requiredFields[] = 'SnippetID';
            $requiredFields[] = 'Description';
        }

        $v->rule('required', $requiredFields)->message('{field} muss angegeben werden');
        $v->rule('slug', 'SnippetID')->message('Der Identifier darf keine Zeichen enthalten, bis auf a-z, 0-9, \'-\' und \'_\'');
        $v->rule('unusedSnippetId', 'SnippetID')->message('SnippetId bereits in der Datenbank vorhanden');

        $v->labels([
            'SnippetID' => 'Identifier',
            'Content'   => 'Inhalt',
            'Description' => 'Beschreibung'
        ]);

        return $v;
    }

    public function preSave($con = null){
        $src = $this->getContent();
        if(!$src){
            $this->setContentCached('');
        } elseif(!$this->getRenderMarkdown()) {
            $this->setContentCached(htmlspecialchars($src, ENT_QUOTES, 'UTF-8'));
        } else {
            $conv = new EMF\Markdown\CommonMarkConverter();
            $this->setContentCached($conv->convertToHtml($src));
        }
        return true;
    }
}
