<?php

use Base\Sponsors as BaseSponsors;

/**
 * Skeleton subclass for representing a row from the 'tblSponsors' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Sponsors extends BaseSponsors
{
    public static function getValidator($data){
        $v = new Valitron\Validator($data);

        $requiredFields = ['Name', 'File'];

        $v->rule('required', $requiredFields)->message('{field} muss angegeben werden');

        $v->labels([
            'File' => 'Bild'
        ]);

        return $v;
    }

    public function preSave($con = null){
        $src = $this->getDescription();
        if(!$src){
            $this->setDescriptionCached('');
        } else {
            $conv = new EMF\Markdown\CommonMarkConverter();
            $this->setDescriptionCached($conv->convertToHtml($src));
        }
        return true;
    }

    public static function routeMain($render = true){
        return function($req, $res) use($render){
            $sponsors = SponsorsQuery::create()
                ->find()
                ->toArray();

            $res->set('sponsors', $sponsors);

            if($render){
                $res->render('sponsors.twig');
            }
        };
    }
}
