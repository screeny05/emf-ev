<?hh

use Base\ConfigQuery as BaseConfigQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'tblConfig' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class ConfigQuery extends BaseConfigQuery
{
    private static $_cache = false;

    public static function getAll(){
        if(self::$_cache === false){
            self::$_cache = self::create()->find()->toKeyValue();
        }

        return self::$_cache;
    }

    public static function getOne($key){
        $snippets = self::getAll();
        return $snippets[$key];
    }

    public static function upsert($key, $value){
        $row = ConfigQuery::create()->findByConfigID($key);
        if(count($row) == 0){
            $row = new Config();
            $row->setConfigID($key);
        } else {
            $row = $row[0];
        }
        $row->setValue($value);
        $row->save();
    }
}
