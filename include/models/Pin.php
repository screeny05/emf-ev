<?php

use Base\Pin as BasePin;

/**
 * Skeleton subclass for representing a row from the 'tblUserPin' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class Pin extends BasePin
{
    public static function getValidator($data){
        $v = new Valitron\Validator($data);

        $v->rule('required', ['Pin', 'Name'])->message('{field} muss angegeben werden.');
        $v->rule('slug', 'Pin')->message('Der PIN darf keine Zeichen enthalten, bis auf a-z, 0-9, \'-\' und \'_\'');
        $v->rule('unusedPin', 'Pin')->message('Der angegebene PIN ist bereits hinterlegt.');

        if(array_key_exists('Sendmail', $data) && $data['Sendmail'] == "true"){
            $v->rule('required', 'Email')->message('{field} muss angegeben werden, falls eine Mail gesendet werden soll.');
            $v->rule('email', 'Email');
        }

        $v->labels([
            'Pin' => 'PIN',
            'Email' => 'Email'
        ]);

        return $v;
    }
}
