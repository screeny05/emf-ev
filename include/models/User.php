<?php

use Base\User as BaseUser;

/**
 * Skeleton subclass for representing a row from the 'tblUser' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class User extends BaseUser
{
    public function getFullName(){
        return $this->getFirstname() . ' ' . $this->getLastname();
    }

    public function setSession(){
        $hasher = new PasswordHash(8, false);
        $sessionId = base64_encode($hasher->HashPassword($this->getEmail() . "::get this string some salt::" . uniqid()));
        $this->setSessionId($sessionId);
        $this->save();

        setcookie("sid", $sessionId, 0, "/", "", false, true);
    }

    public function unsetSession(){
        $this->setSessionId("");
        $this->save();

        setcookie("sid", "", time() - 3600, "/");
    }

    public function can($role){
        $group = $this->getGroup();
        return $group ? $group->can($role) : false;
    }


    public static function checkCredentials($email, $password){
        if(!$email || !$password){
            return false;
        }

        $user = UserQuery::create()
            ->filterByEmail($email)
            ->findOne();

        if(!$user){
            return false;
        }

        $hasher = new PasswordHash(8, false);
        $valid = $hasher->CheckPassword($password, $user->getPasswordHash());

        if(!$valid){
            return false;
        }
        return $user;
    }

    public static function getValidator($data){
        $v = new Valitron\Validator($data);

        $v->rule('required', array('Email', 'Pin', 'Password', 'PasswordDuplicate', 'Firstname', 'Lastname', 'Gender', 'Telephone', 'Zip', 'City', 'Street', 'Birthday'))->message('{field} muss angegeben werden');
        $v->rule('email', 'Email')->message('Die angegebene Email ist ungültig');
        $v->rule('equals', 'Password', 'PasswordDuplicate')->message('Die beiden Passwörter stimmen nicht überein');
        $v->rule('lengthMin', 'Password', 6)->message('Das Passwort muss länger als 6 Zeichen sein');
        $v->rule('lengthMin', 'PasswordDuplicate', 6);
        $v->rule('date', 'Birthday')->message('Das Geburtsdatum ist nicht korrekt formatiert. Bitte folgendes Format beachten: YYYY-MM-DD');
        $v->rule('validPin', 'Pin')->message('Der angegebene Pin ist ungültig');
        $v->rule('unusedEmail', 'Email')->message("Die angegebene Email-Adresse wird bereits verwendet");
        $v->rule('in', 'Gender', array('female', 'male'));

        $v->labels(array(
            'Email' => 'Email',
            'Pin'   => 'Pin',
            'Password' => 'Passwort',
            'Firstname' => 'Vorname',
            'Lastname' => 'Nachname',
            'gender' => 'Geschlecht',
            'Telephone' => 'Telefonnummer',
            'Zip' => 'Postleitzahl',
            'City' => 'Ort',
            'Street' => 'Straße und Hausnummer',
            'Birthday' => 'Geburtstag'
        ));

        return $v;
    }

    public static function routeLogin(){
        return function($req, $res){
            $snippets = SnippetQuery::getAll();

            $mail = empty($req->query['mail']) ? false : $req->query['mail'];
            $pw = empty($req->query['pw']) ? false : $req->query['pw'];

            $user = self::checkCredentials($mail, $pw);

            if($user){
                $user->setSession();
                $res->redirect('/admin/');
            } else {
                $res->status(401);
                $res->renderError($snippets['error-head-login'], $snippets['error-login']);
            }
        };
    }

    public static function routeLogout(){
        return function($req, $res){
            $res->get('user')->unsetSession();

            $redirectPath = empty($req->query['ret']) ? '/' : $req->query['ret'];
            $res->redirect($redirectPath);
        };
    }

    public static function routeRegister(){
        return function($req, $res){
            $v = self::getValidator($req->query);
            $valid = $v->validate();

            if(!$valid){
                $validationResult = $v->errors();

                $res->set('error', $validationResult)
                    ->set('data', $req->query)
                    ->render('register.twig');
            } else {
                $hasher = new PasswordHash(8, false);
                $user = new User();
                $pin = PinQuery::create()->findPk($req->query['Pin']);
                $user->setActive(true)
                    ->setGroupID($pin->getGroupID())
                    ->setUsedPin($req->query['Pin'])
                    ->setPasswordHash($hasher->HashPassword($req->query['Password']));
                $pin->delete();
                $user->fromArray($req->query);
                $user->save();
                $user->setSession();
                $res->redirect('/admin/');
            }
        };
    }

    public static function routeRegisterView(){
        return function($req, $res){
            $res->set('validation', [])
                ->set('data', $req->query)
                ->render('register.twig');
        };
    }
}
