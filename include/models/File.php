<?php

use Base\File as BaseFile;

/**
 * Skeleton subclass for representing a row from the 'tblFiles' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class File extends BaseFile
{
    public static function getValidator($data){
        $v = new Valitron\Validator($data);

        $v->rule('required', ['Title', 'Description', 'Icon', 'FolderID'])->message('{field} muss angegeben werden.');
        $v->rule('in', 'FolderID', FileFolderQuery::create()->find()->getPrimaryKeys(false));

        return $v;
    }

    public function preSave($con = null){
        $src = $this->getDescription();
        if(!$src){
            $this->setDescriptionCached('');
        } else {
            $conv = new EMF\Markdown\CommonMarkConverter();
            $this->setDescriptionCached($conv->convertToHtml($src));
        }
        return true;
    }

    public static function routeMain($render = true){
        return function($req, $res) use($render){
            if($res->get('group')->can('admin-files-view-public')){
                $folders = FileFolderQuery::create()
                    ->filterByPublic(true)
                    ->orderByName()
                    ->leftJoinWith('FileFolder.Files')
                    ->find()
                    ->toArray();
                $res->set('folders', $folders);
            }

            if($res->get('group')->can('admin-files-view-private')){
                $folders = FileFolderQuery::create()
                    ->orderByName()
                    ->leftJoinWith('FileFolder.Files')
                    ->find()
                    ->toArray();
                $res->set('folders', $folders);
            }

            if($render){
                $res->render('downloads.twig');
            }
        };
    }

    public static function routeFile(){
        return function($req, $res){
            $file = FileQuery::create()
                ->findPK($req->params['id']);

            $mainRoute = self::routeMain(false);
            $mainRoute($req, $res);

            if(!$file){
                $res->status(404);
                return $res->renderError(false, SnippetQuery::getOne('error-file-404'));
            }

            $isPublic = $file->getFolder()->getPublic();

            if(($isPublic && !$res->get('group')->can('admin-files-view-public')) || (!$isPublic && !$res->get('group')->can('admin-files-view-private'))){
                $res->status(403);
                return $res->renderError(false, SnippetQuery::getOne('error-403'));
            }

            if($req->params['slug'] != $file->getName()){
                $res->redirect('/downloads/' . $file->getFileId() . '-' . $file->getName());
                return $res->execute();
            }

            $res->set('activeFile', $file)
                ->render('downloads.twig');
        };
    }
}
