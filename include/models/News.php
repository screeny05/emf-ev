<?php

use Base\News as BaseNews;

/**
 * Skeleton subclass for representing a row from the 'tblNews' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class News extends BaseNews
{
    public static $germanMonths = [
        "Januar",
        "Februar",
        "März",
        "April",
        "Mai",
        "Juni",
        "Juli",
        "August",
        "September",
        "Oktober",
        "November",
        "Dezember"
    ];

    public function getShortUrl(){
        return "/news/" . $this->getNewsID() . "-" . $this->getSeoTitle();
    }

    public static function getValidator($data){
        $v = new Valitron\Validator($data);

        $v->rule('required', array('Title', 'SeoTitle', 'Published', 'Content'))->message('{field} muss angegeben werden');
        $v->rule('slug', 'SeoTitle')->message('Die Short-URL darf keine Zeichen enthalten, bis auf a-z, 0-9, \'-\' und \'_\'');
        $v->rule('date', 'Published')->message('Das Veröffentlichungsdatum ist nicht korrekt formatiert.');

        $v->labels(array(
            'Title' => 'Titel',
            'SeoTitle'   => 'Short-URL',
            'Published' => 'Veröffentlichungsdatum',
            'Content' => 'Inhalt'
        ));

        return $v;
    }

    public static function routeMain(){
        return function($req, $res){
            $articles = NewsQuery::create()
                ->leftJoinWith('News.User')
                ->filterByListed(true)
                ->orderByPublished('desc')
                ->find();

            $res->set('articles', $articles)
                ->set('german_months', self::$germanMonths)
                ->set('in_overview', true)
                ->render('news.twig');
        };
    }

    public static function routeArticle(){
        return function($req, $res){
            $article = NewsQuery::create()->findPK($req->params['id']);

            if(!$article){
                $res->status(404);
                return $res->renderError(false, SnippetQuery::getOne('error-article-404'));
            }

            $article->getUser();

            if($req->params['slug'] != $article->getSeoTitle()){
                $res->redirect('/news/' . $article->getNewsId() . '-' . $article->getSeoTitle());
                return $res->execute();
            }

            $res->set('article', $article)
                ->set('german_months', self::$germanMonths)
                ->set('in_overview', false)
                ->render('news.twig');
        };
    }

    public function preSave($con = null){
        $src = $this->getContent();
        if(!$src){
            $this->setContentCached('');
        } else {
            $conv = new EMF\Markdown\CommonMarkConverter();
            $this->setContentCached($conv->convertToHtml($src));
        }
        return true;
    }
}
