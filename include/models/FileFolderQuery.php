<?php

use Base\FileFolderQuery as BaseFileFolderQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'tblFileFolders' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class FileFolderQuery extends BaseFileFolderQuery
{
    public static function getAllFolders(){
        $folders = array_map(function($folder){
            return $folder['Name'] . ' (' . ($folder['Public'] ? 'Öffentlich' : 'Privat') . ')';
        }, FileFolderQuery::create()->find()->toArray('FolderID'));
        return $folders;
    }
}
