<?php

use Base\SnippetQuery as BaseSnippetQuery;

/**
 * Skeleton subclass for performing query and update operations on the 'tblSnippets' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class SnippetQuery extends BaseSnippetQuery
{
    private static $_cache = false;

    public static function getAll(){
        if(self::$_cache === false){
            self::$_cache = self::create()->find()->toKeyValue();
        }

        return self::$_cache;
    }

    public static function getOne($key){
        $snippets = self::getAll();
        return $snippets[$key];
    }
}
