<?php

use Base\AccessGroup as BaseAccessGroup;

/**
 * Skeleton subclass for representing a row from the 'tblAccessGroups' table.
 *
 *
 *
 * You should add additional methods to this class to meet the
 * application requirements.  This class will only be generated as
 * long as it does not already exist in the output directory.
 *
 */
class AccessGroup extends BaseAccessGroup
{
    public function can($role){
        $roles = $this->getAccessRoles()->toKeyValue();
        return $this->getAdmin() || array_key_exists($role, $roles);
    }
}
