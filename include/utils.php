<?php

function startsWith($haystack, $needle) {
    return !strncmp($haystack, $needle, strlen($needle));
}
