<?php
$debugRoute = function($req, $res){
    $res->raw(
        '<code style="width:50%;min-width:400px;word-break:break-word;margin-bottom:30px;">' .
            '<h3>req:</h3>
            <pre>' . json_encode($req, JSON_PRETTY_PRINT) . '</pre>
            <h3>res:</h3>
            <pre>' . json_encode($res, JSON_PRETTY_PRINT) . '</pre>' .
        '</code>'
    );
};
