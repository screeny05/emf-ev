<?php

$snippetsModule = new AdminModule('snippets', 'Snippets', 'fa-align-left');

$snippetsModule->on('view', function($req, $res){
    return ["snippets" => SnippetQuery::create()->find()];
});
$snippetsModule->on('create', function($req, $res){
    return array('snippet' => false);
}, 'Neues Snippet');
$snippetsModule->on('update', function($req, $res){
    return [
        'snippet' => SnippetQuery::create()->findPK($req->params['id'])->toArray()
    ];
});
$snippetsModule->post('create', function($req, $res){
    $v = Snippet::getValidator($req->query);
    $valid = $v->validate();
    $renderMarkdown = array_key_exists('RenderMarkdown', $req->query) ? $req->query['RenderMarkdown'] : false;

    if(!$valid){
        $res->set('error', $v->errors())
            ->set('data', ['snippet' => $req->query]);
    } else {
        $snippet = new Snippet();
        $snippet->fromArray($req->query);
        $snippet
            ->setRenderMarkdown($renderMarkdown)
            ->save();
        $res->redirect('/admin/snippets/update/' . $snippet->getSnippetId() . '?success=1');
    }
});
$snippetsModule->post('update', function($req, $res){
    $id = $req->params['id'];
    $snippet = SnippetQuery::create()->findPK($id);
    $v = Snippet::getValidator($req->query, true);

    $valid = $v->validate();
    if(!$valid){
        $data = $req->query;
        $data['SnippetID'] = $snippet->getSnippetID();
        $data['Description'] = $snippet->getDescription();
        $errors = $v->errors();
        $res->set('error', $errors)
            ->set('data', ['snippet' => $data]);
    } else {
        $snippet
            ->setContent($req->query['Content'])
            ->setRenderMarkdown($req->query['RenderMarkdown'])
            ->save();
        $res->redirect('/admin/snippets/update/' . $snippet->getSnippetId() . '?success=1');
    }
});
