<?php

$newsModule = new AdminModule('news', 'News', 'fa-newspaper-o');

$newsModule->on('view', function($res, $req){
    return array("news" => NewsQuery::create()->orderByPublished("DESC")->find());
});
$newsModule->on('create', function($req, $res){
    return array('article' => false);
}, 'Neuer Artikel');
$newsModule->on('update', function($req, $res){
    $article = NewsQuery::create()->findPk($req->params['id']);
    return array(
        'article' => $article->toArray(),
        'user' => $article->getUser()->toArray()
    );
});
$newsModule->post('create', function($req, $res){
    $v = News::getValidator($req->query);
    $valid = $v->validate();

    if(!$valid){
        $res->set('error', $v->errors())
            ->set('data', ['article' => $req->query]);
    } else {
        $article = new News();
        $article->fromArray($req->query);
        $article
            ->setUser($res->get('user'))
            ->setListed($req->query['Listed'])
            ->save();
        $res->redirect('/admin/news/update/' . $article->getNewsId() . '?success=1');
    }
});
$newsModule->post('update', function($req,$res){
    $newsId = $req->params['id'];
    $article = NewsQuery::create()->findPk($newsId);

    $v = News::getValidator($req->query);
    $valid = $v->validate();

    if(!$valid){
        $data = $req->query;
        $res->set('error', $v->errors())
            ->set('data', ['article' => $data]);
    } else {
        $article->fromArray($req->query);
        $article
            ->setUser($res->get('user'))
            ->setListed($req->query['Listed'])
            ->save();
        $res->redirect('/admin/news/update/' . $article->getNewsId() . '?success=1');
    }
});
$newsModule->post('delete', function($req, $res){
    $article = NewsQuery::create()->findPk($req->query['id']);
    if(!$article){
        $res->status(404)
            ->renderError(false, SnippetQuery::getOne('error-404'))
            ->lock();
    } else {
        $article->delete();
        $res->redirect('/admin/news/?success=1');
    }
});
