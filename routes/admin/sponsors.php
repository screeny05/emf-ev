<?php

$sponsorsModule = new AdminModule('sponsors', 'Sponsoren', 'fa-rocket');

$sponsorsModule->on('view', function($req, $res){
    $sponsors = SponsorsQuery::create()
        //->orderByName()
        ->orderByPosition(DESC)
        ->find()
        ->toArray();

    return ['sponsors' => $sponsors];
});

$sponsorsModule->on('create', function($req, $res){

}, 'Sponsor hinzufügen');

$sponsorsModule->on('update', function($req, $res){
    return [
        'sponsor' => SponsorsQuery::create()->findPK($req->params['id'])->toArray()
    ];
});

$sponsorsModule->post('create', function($req, $res){
    $v = Sponsors::getValidator($req->query);
    $valid = $v->validate();

    if(!$valid){
        $res->set('error', $v->errors())
            ->set('data', ['sponsor' => $req->query]);
    } else {
        $sponsor = new Sponsors();
        $sponsor->fromArray($req->query);
        $sponsor->save();
        $res->redirect('/admin/sponsors/update/' . $sponsor->getSponsorId() . '?success=1');
    }
});

$sponsorsModule->post('update', function($req, $res){
    $sponsorId = $req->params['id'];
    $sponsor = SponsorsQuery::create()->findPk($sponsorId);

    $v = Sponsors::getValidator($req->query);
    $valid = $v->validate();

    if(!$valid){
        $data = $req->query;
        $res->set('error', $v->errors())
            ->set('data', ['sponsor' => $sponsor]);
    } else {
        $sponsor->fromArray($req->query);
        $sponsor->save();
        $res->redirect('/admin/sponsors/update/' . $sponsor->getSponsorId() . '?success=1');
    }
});

$sponsorsModule->post('delete', function($req, $res){
    $sponsor = SponsorsQuery::create()->findPk($req->query['id']);
    if(!$sponsor){
        $res->status(404)
            ->renderError(false, SnippetQuery::getOne('error-404'))
            ->lock();
        return;
    }
    $sponsor->delete();
    $res->redirect('/admin/sponsors?success=1');
});

$sponsorsModule->post('position', function($req, $res){
    $sponsorId = $req->query['id'];
    $positionTo = $req->query['to'];

    $foundIndex = -1;

    $sponsors = SponsorsQuery::create()
        ->orderByPosition(DESC)
        ->find();
    $i = 0;

    foreach($sponsors as $sponsor){
        $sponsor->setPosition($i);
        $i++;
    }

    $foundItem = $sponsors[$foundIndex];

    if($positionTo === 'up' && $foundIndex < count($sponsors)){
        $sponsors[$foundIndex] = $sponsors[$foundIndex - 1];
        $sponsors[$foundIndex - 1] = $foundItem;
    } elseif($positionTo === 'down' && $foundIndex > 0){
        $sponsors[$foundIndex] = $sponsors[$foundIndex + 1];
        $sponsors[$foundIndex + 1] = $foundItem;
    }


    $i = 0;
    foreach($sponsors as $sponsor){
        $sponsor->setPosition($i);
        $sponsor->save();
        $i++;
    }

    $res->redirect('/admin/sponsors?success=1');
});
