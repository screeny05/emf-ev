<?php

use EMF\Calendar\Calendar;

$calendarModule = new AdminModule('calendar', 'Kalender', 'fa-calendar');

$calendarModule->on('view', function($req, $res){
    return [
        'calendarOauthLink' => Calendar::getOauthUrl(),
        'calendarIcsLink' => Calendar::getIcsLink()
    ];
});

$calendarModule->on('create', function($req, $res){
    $event = [];

    if($req->query['date']){
        $date = new DateTime();
        $date->setTimestamp($req->query['date']);
        $event['Start'] = $date;
    }

    return [
        'eventTypes' => Calendar::getEventTypes(),
        'event' => $event,
        'users' => UserQuery::create()->find()->toArray()
    ];
}, 'Termin erstellen');

$calendarModule->post('create', function($req, $res){
    $v = Calendar::getValidator($req->query);
    $valid = $v->validate();

    if(!$valid){
        $res->set('error', $v->errors())
            ->set('data', [
                'event' => $req->query,
                'eventTypes' => Calendar::getEventTypes(),
                'users' => UserQuery::create()->find()->toArray()
            ]);
    } else {
        $event = Calendar::eventFromArray($req->query, $res->get('user'));
        $c = new Calendar();
        $c->createEvent($event);

        $res->redirect('/admin/calendar?success=1');
    }
});

$calendarModule->on('update', function($req, $res){
    $c = new Calendar();
    $event = $c->getEventAsArray($req->params['id']);

    return [
        'eventTypes' => Calendar::getEventTypes(),
        'event' => $event,
        'isDisabled' => !($event['UsersCanModify'] || $event['Organizer']['email'] === $res->get('user')->getEmail()),
        'users' => UserQuery::create()->find()->toArray()
    ];
});

$calendarModule->post('update', function($req, $res){
    $v = Calendar::getValidator($req->query);
    $valid = $v->validate();

    if(!$valid){
        $res->set('error', $v->errors())
            ->set('data', [
                'event' => $req->query,
                'eventTypes' => Calendar::getEventTypes(),
                'users' => UserQuery::create()->find()->toArray()
            ]);
    } else {
        $event = Calendar::eventFromArray($req->query, $res->get('user'));
        $c = new Calendar();
        $c->updateEvent($req->params['id'], $event, !!$req->query['NotifyChange'], $res->get('user'));

        $res->redirect('/admin/calendar?success=1');
    }
});

$calendarModule->on('delete', function($req, $res){
    $c = new Calendar();
    $event = $c->deleteEvent($req->params['id']);
    $res->redirect('/admin/calendar?success=1');
});

$calendarModule->api('getEvents', function($req, $res){
    $c = new Calendar();
    $events = $c->listEvents();
    return $events;
});

$calendarModule->api('oauthcb', function($req, $res){
    ConfigQuery::upsert('GoogleOauthResponse', Calendar::getEncodedAccessToken($req->query['code']));
    $res->redirect('/admin/calendar/?success=1');
});

$calendarNotificationCron = function($req, $res){
    $c = new Calendar();
    $mail = new EMF\Mail\Mail(ConfigQuery::getAll());
    $events = $c->getEventsNotNotified();

    $ret = ['success' => true, 'mailCount' => 0, 'eventCount' => 0];

    foreach ($events as $event) {
        $eventData = Calendar::eventToArray($event);

        foreach ($event->getAttendees() as $attendee) {
            $attendeeData = Calendar::getUserDataByAttendee($attendee);
            $attendeeName = "EMF Mitglied";

            if($attendeeData['user']){
                $attendeeName = $attendeeData['user']['Firstname'] . " " . $attendeeData['user']['Lastname'];
            }

            $attendeeAddress = [
                'address' => $attendeeData['email'],
                'name' => $attendeeName
            ];

            if($mail->send([$attendeeAddress], 'Erinnerung zum Termin "' . $eventData['Title'] . '"', 'calendar-notification.twig', [
                'User' => $attendeeData,
                'Event' => $eventData
            ])){
                $ret['mailCount']++;
            } else {
                $ret['success'] = false;
            }
        }

        $c->markEventAsNotified($event);
        $ret['eventCount']++;
    }

    $res->renderData($ret);
};
