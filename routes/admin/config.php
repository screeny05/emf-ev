<?php

$configModule = new AdminModule('config', 'Konfiguration', 'fa-sliders');

$configModule->on('view', function($req, $res){
    return ['config' => ConfigQuery::getAll()];
});
$configModule->on('gulp', function($req, $res){
    $csrfRoute = CsrfProtector::route();
    $csrfRoute($req, $res);
    if($res->isLocked()){
        return;
    }
    $buildItem = $req->params['id'];
    if(!in_array($buildItem, ['css', 'js', 'img'])){
        return $res
            ->status(404)
            ->renderError(false, SnippetQuery::getOne('error-404'))
            ->lock();
    }
    $res->set('output', shell_exec('gulp --no-color build:' . $buildItem));
});
$configModule->on('cache', function($req, $res){
    $csrfRoute = CsrfProtector::route();
    $csrfRoute($req, $res);
    if($res->isLocked()){
        return;
    }
    $res->set('output', shell_exec('rm -rf cache/*'));
    $res->redirect('/admin/config?success=1');
});
$configModule->on('cloudflare-cache', function($req, $res){
    $csrfRoute = CsrfProtector::route();
    $csrfRoute($req, $res);
    if($res->isLocked()){
        return;
    }

    $endpoint = 'https://api.cloudflare.com/client/v4/';
    $authKey = ConfigQuery::getOne('CloudflareKey');
    $authEmail = ConfigQuery::getOne('CloudflareMail');
    $zoneId = ConfigQuery::getOne('CloudflareZoneID');
    $reqBody = '{ "purge_everything": true }';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint . 'zones/' . $zoneId . '/purge_cache');
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'DELETE');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_POSTFIELDS, $reqBody);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'Content-Type: application/json',
        'Content-Length: ' . strlen($reqBody),
        'X-Auth-Key: ' . $authKey,
        'X-Auth-Email: ' . $authEmail
    ]);

    $result = curl_exec($ch);
    $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);
    $resultBody = json_decode($result);

    if($resultCode !== 200 || !$resultBody->success){
        $res
            ->status(501)
            ->set('stacktraceHtml', json_encode($resultBody, JSON_PRETTY_PRINT))
            ->renderError(false, 'Cloudflare API Error')
            ->lock();
    } else {
        $res->redirect('/admin/config?success=1');
    }
    curl_close($ch);
});
$configModule->post('update', function($req, $res){

    $fields = $req->query;
    foreach ($fields as $key => $value) {
        if((!$key || $key == 'ctoken')){
            break;
        }
        ConfigQuery::upsert($key, $value);
    }
    $res->redirect('/admin/config/?success=1');
});
$configModule->action('/admin/config/gulp/css?ctoken=' . CsrfProtector::get(), 'Build CSS');
$configModule->action('/admin/config/gulp/img?ctoken=' . CsrfProtector::get(), 'Build Images');
$configModule->action('/admin/config/gulp/js?ctoken=' . CsrfProtector::get(), 'Build JS');
$configModule->action('/admin/config/cache?ctoken=' . CsrfProtector::get(), 'Clear Template Cache');
$configModule->action('/admin/config/cloudflare-cache?ctoken=' . CsrfProtector::get(), 'Clear Cloudflare Cache');
$configModule->action(EMF\Calendar\Calendar::getOauthUrl(), 'Google Kalender Authentifizierung');
