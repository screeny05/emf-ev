<?php

use Cocur\Slugify\Slugify;

function sendFileUpdateMail($request, $file, $isUpdated){
    $users = UserQuery::create()->find();

    $addresses = [];
    foreach($users as $user){
        $name = $user->getFirstName() . ' ' . $user->getLastName();
        $addresses[] = ['name' => $name, 'address' => $user->getEmail()];
    }

    $mail = new EMF\Mail\Mail(ConfigQuery::getAll());
    if(!$mail->send($addresses, 'Dateiänderung bei EMF e.V.', 'file.twig', ['File' => $file->toArray()])){
        die('Mail konnte nicht versendet werden. Datei ist trotzdem gespeichert.');
    }
}

$filesModule = new AdminModule('files', 'Dateien', 'fa-file-o');

$filesModule->on('view', function($req, $res){
    $folders = FileFolderQuery::create()
        ->leftJoinWith('FileFolder.Files')
        ->find()
        ->toArray();

    return array('folders' => $folders);
});
$filesModule->on('create', function($req, $res){
    $folders = FileFolderQuery::getAllFolders();

    return ['file' => ['Path' => 'Nicht verfügbar'], 'folders' => $folders];
}, 'Datei hinzufügen');
$filesModule->on('folder', function($req, $res){
    $folder = $req->params['id'] ? FileFolderQuery::create()->findPk($req->params['id'])->toArray() : false;
    return ['folder' => $folder];
}, 'Ordner hinzufügen');
$filesModule->post('create', function($req, $res){
    $userFile = $req->getFile('File');
    if(!$userFile){
        $res->set('error', ['File' => ['Bitte wähle eine Datei']])
            ->set('data', ['file' => $req->query]);
        return;
    }

    $fileError = $userFile['error'];
    if($fileError != UPLOAD_ERR_OK){
        $snippet = false;
        switch($fileError){
            case UPLOAD_ERR_INI_SIZE:
            case UPLOAD_ERR_FORM_SIZE:
                $snippet = 'Datei zu groß';
                break;
            case UPLOAD_ERR_PARTIAL:
                $snippet = 'Datei nicht vollständig hochgeladen. Probiere es nochmal';
                break;
            case UPLOAD_ERR_NO_FILE:
                $snippet = 'Es wurde keine Datei hochgeladen';
                break;
            default:
                $snippet = 'Interner Fehler - Falls dieser nochmal auftritt, wende dich an den Administrator. Fehlermeldung <a href="https://secure.php.net/manual/de/features.file-upload.errors.php">' . $fileError . '</a>';
                break;
        }

        $res->set('error', ['File' => [$snippet]])
            ->set('data', ['file' => $req->query]);
        return;
    }

    $pathInfo = pathinfo($userFile['name']);

    $slugify = new Slugify();
    $userFile['name'] = $slugify->slugify($pathInfo['filename']) . '.' . $pathInfo['extension'];
    $filePath = '/' . trim(ConfigQuery::getOne('FilePath'), '/') . '/' . $userFile['name'];

    if(!move_uploaded_file($userFile['tmp_name'], '.' . $filePath)){
        $res->set('error', ['File' => ['Interner Fehler - Datei konnte nicht verschoben werden']])
            ->set('data', ['file' => $req->query]);
        return;
    }

    $v = File::getValidator($req->query);
    $valid = $v->validate();

    if(!$valid){
        $res->set('error', $v->errors())
            ->set('data', ['file' => $req->query]);
    } else {
        $file = new File();
        $file->fromArray($req->query);
        $file
            ->setUser($res->get('user'))
            ->setType('misc')
            ->setExtension($pathInfo['extension'])
            ->setSize($userFile['size'])
            ->setName($userFile['name'])
            ->setPath($filePath);
        $file->save();

        if($req->query['SendMail']){
            sendFileUpdateMail($req, $file, false);
        }

        $res->redirect('/admin/files/update/' . $file->getFileID() . '?success=1');
    }
});

$filesModule->post('update', function($req,$res){
    $fileId = $req->params['id'];
    $file = FileQuery::create()->findPk($fileId);
    $folders = FileFolderQuery::getAllFolders();


    $v = File::getValidator($req->query);
    $valid = $v->validate();

    if(!$valid){
        $data = $req->query;
        $res->set('error', $v->errors())
            ->set('data', ['file' => $data, 'folders' => $folders]);
    } else {
        $file->fromArray($req->query);
        $file
            ->setUser($res->get('user'))
            ->save();

        if($req->query['SendMail']){
            sendFileUpdateMail($req, $file, true);
        }

        $res->redirect('/admin/files/update/' . $file->getFileId() . '?success=1');
    }
});

$filesModule->on('update', function($req, $res){
    $file = FileQuery::create()->findPk($req->params['id'])->toArray();
    $folders = FileFolderQuery::getAllFolders();

    $file['Path'] = ConfigQuery::getOne('FilePrefix') . trim($file['Path'], '/');
    return ['file' => $file, 'folders' => $folders];
});

$filesModule->post('delete', function($req, $res){
    $file = FileQuery::create()->findPk($req->query['id']);
    if(!$file){
        $res->status(404)
            ->renderError(false, SnippetQuery::getOne('error-404'))
            ->lock();
        return;
    }
    unlink('./' . trim(ConfigQuery::getOne('FilePath'), '/') . '/' . $file->getName());
    $file->delete();
    $res->redirect('/admin/files?success=1');
});

$filesModule->post('folder', function($req, $res){
    $folder = FileFolderQuery::create()->findPk($req->params['id']);

    if(!$folder){
        $folder = new FileFolder();
    }

    $folder->fromArray($req->query);
    $folder->setPublic($req->query['Public'])
        ->save();

    $res->redirect('/admin/files/folder/' . $folder->getFolderId() . '?success=1');
});

$filesModule->api('files', function($req, $res){
    return FileFolderQuery::create()
        ->leftJoinWith('FileFolder.Files')
        ->find()
        ->toArray();
});
