<?php

$mailModule = new AdminModule('mail', 'E-Mail', 'fa-envelope-o');

function getMailDbConnection(){
    $configFile = file_get_contents('include/config.json');
    $config = json_decode($configFile, true);
    $dbConfig = $config['propel']['database']['connections']['emf'];

    return new medoo([
        'database_type' => $dbConfig['adapter'],
        'database_name' => 'mailserver',
        'server' => 'localhost',
        'username' => $dbConfig['user'],
        'password' => $dbConfig['password'],
    ]);
}

$mailModule->on('view', function($res, $req){
    $db = getMailDbConnection();
    $mailBoxes = $db->select('virtual_aliases', [
        'id',
        'source',
        'destination'
    ], [
        'source[~]' => ConfigQuery::getOne('Domain')
    ]);
    $formattedBoxes = [];

    foreach ($mailBoxes as $box) {
        if(!array_key_exists($box['source'], $formattedBoxes)){
            $formattedBoxes[$box['source']] = [];
        }
        $formattedBoxes[$box['source']][] = $box;
    }

    return array('mailboxes' => $formattedBoxes);
});
$mailModule->post('create', function($req, $res){
    $db = getMailDbConnection();
    $db->insert('virtual_aliases', [
        'domain_id' => 3,
        'source' => $req->query['mailbox'] . '@' . ConfigQuery::getOne('Domain'),
        'destination' => $req->query['destination']
    ]);
    $res->redirect('/admin/mail/?success=1');
});
$mailModule->post('delete', function($req, $res){
    $boxId = $req->query['id'];
    $db = getMailDbConnection();
    $db->delete('virtual_aliases', ['id' => $boxId]);
    $res->redirect('/admin/mail/?success=1');
});
