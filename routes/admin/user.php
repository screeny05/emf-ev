<?php

$userModule = new AdminModule('user', 'User', 'fa-users');

$userModule->on('view', function($req, $res){
    return [
        "user" => UserQuery::create()->find(),
        "pins" => PinQuery::create()->find(),
        "groups" => AccessGroupQuery::create()->find(),
        "roleCount" => AccessRoleQuery::create()->count()
    ];
});
$userModule->on('pin', function($req, $res){
    return [
        'userGroups' => AccessGroupQuery::create()->find()->toKeyValue()
    ];
}, 'PIN generieren');
$userModule->on('update', function($req, $res){
    return [
        'user' => UserQuery::create()->findPk($req->params['id'])->toArray(),
        'userGroups' => AccessGroupQuery::create()->find()->toKeyValue()
    ];
});
$userModule->on('group', function($req, $res){
    $group = false;
    if(array_key_exists('id', $req->params)){
        $group = AccessGroupQuery::create()->findPk($req->params['id']);
    }
    $roles = AccessRoleQuery::create()->find()->toKeyValue();

    $rolesSorted = [];
    foreach($roles as $role => $roleTitle){
        $rolePath = explode('-', $role, 3);
        if(count($rolePath) === 1){
            $rolePath = [$rolePath[0], '', ''];
        }
        if(count($rolePath) === 2){
            $rolePath = [$rolePath[0], $rolePath[1], ''];
        }

        if(!array_key_exists($rolePath[0], $rolesSorted)){
            $rolesSorted[$rolePath[0]] = [];
        }
        if(!array_key_exists($rolePath[1], $rolesSorted[$rolePath[0]])){
            $rolesSorted[$rolePath[0]][$rolePath[1]] = [];
        }

        $rolesSorted[$rolePath[0]][$rolePath[1]][$role] = $roleTitle;
    }

    return [
        'group' => $group ? $group->toArray() : false,
        'roles' => $roles,
        'rolesSorted' => $rolesSorted,
        'groupRoles' => $group ? $group->getAccessRoles()->toKeyValue() : false
    ];
});
$userModule->on('table', function($req, $res){
    return [
        'user' => UserQuery::create()->find()->toArray()
    ];
});
$userModule->on('csv', function($req, $res){
    $res->contentType('text/csv');
    $res->contentDisposition('emf-userdata-' . date('Y-m-d') . '.csv');
    $res->lock();

    $headline = ['Name', 'Email', 'Telefon', 'Adresse', 'Geburtstag', 'Geschlecht', 'Registriert am', 'Benutzergruppe'];
    $data = UserQuery::create()->joinWith('User.Group')->find();

    $stdo = fopen('php://output', 'w');
    fputcsv($stdo, $headline);
    foreach($data as $user){
        fputcsv($stdo, [
            $user->getFirstname() . ' ' . $user->getLastname(),
            $user->getEmail(),
            'tel: ' . $user->getTelephone(),
            $user->getZip() . ' ' . $user->getCity() . ' - ' . $user->getStreet(),
            $user->getBirthday()->format('d.m.Y'),
            $user->getGender() == 'male' ? 'männlich' : 'weiblich',
            $user->getCreatedAt()->format('d.m.Y H:i'),
            $user->getGroup()->getTitle()
        ]);
    }
    return [];
}, 'Userdaten Exportieren (.csv)');
//$userModule->on('newsletter', function($req, $res){}, 'Newsletter versenden');
$userModule->post('pin', function($req, $res){
    $v = Pin::getValidator($req->query);
    $valid = $v->validate();

    if(!$valid){
        $res->set('error', $v->errors())
            ->set('data', [
                'pin' => $req->query,
                'userGroups' => AccessGroupQuery::create()->find()->toKeyValue()
            ]);
    } else {
        $pin = new Pin();
        $pin->fromArray($req->query);
        $pin->save();

        if($req->query['Sendmail']){
            $mail = new EMF\Mail\Mail(ConfigQuery::getAll());
            if(!$mail->send([['address' => $pin->getEmail(), 'name' => $pin->getName()]], 'Dein PIN bei emf-ev.de', 'pin.twig', ['Pin' => $pin->toArray()])){
                die('Mail konnte nicht versendet werden. PIN ist trotzdem gespeichert.');
            }
        }

        $res->redirect('/admin/user?success=1');
    }
});
$userModule->post('pin-delete', function($req, $res){
    if(array_key_exists('id', $req->query)){
        $pin = PinQuery::create()->findPk($req->query['id']);
        if($pin){
            $pin->delete();
        }
    } else {
        PinQuery::create()->deleteAll();
    }
    $res->redirect('/admin/user?success=1');
});
$userModule->post('update', function($req, $res){
    $user = UserQuery::create()->findPk($req->params['id']);
    $user->setActive($req->query['Active'])
        ->setGroupID($req->query['GroupID'])
        ->save();
    $res->redirect('/admin/user/update/' . $req->params['id'] . '/?success=1');
});
$userModule->post('group', function($req, $res){
    $group = false;
    if(array_key_exists('id', $req->params)){
        $group = AccessGroupQuery::create()->findPk($req->params['id']);
    } else {
        $group = new AccessGroup();
        $group->fromArray($req->query);
    }
    $roles = AccessRoleQuery::create()->findPKs($req->query['Roles']);
    $group->setAccessRoles($roles);
    $group->setAdmin($req->query['Admin']);
    $group->save();
    $res->redirect('/admin/user/group/' . $group->getGroupID() . '?success=1');
});
$userModule->post('group-delete', function($req, $res){
    $group = AccessGroupQuery::create()->findPk($req->query['id']);
    if(!$group){
        $res->status(404)
            ->renderError(false, SnippetQuery::getOne('error-404'))
            ->lock();
    } else {
        $user = UserQuery::create()->findByGroupID($req->query['id']);
        foreach($user as $u){
            $u->setGroupID(null)->save();
        }
        $group->delete();
        $res->redirect('/admin/user/?success=1');
    }
});
