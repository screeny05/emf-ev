<?php
$staticRoute = function($req, $res){
    $static = trim($req->path, '/');
    $static = $static ? $static : 'index';
    $res->render($static . '.twig');
};
