<?hh
$errorRoute = function($req, $res){
    $code = $req->params['code'];
    $res->status($code)
        ->renderError(false, SnippetQuery::getOne('error-' . $code))
        ->status(200);
};
