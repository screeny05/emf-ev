<?php

// TODO: Update/Delete/Create Entity not found

class AdminModule {
    public $title;
    public $slug;
    public $faIcon;
    public $actions = [];
    public $quickActions = [];
    public $postActions = [];
    public $apiActions = [];

    public function __construct($slug, $title, $faIcon = "fa-ellipsis-v"){
        $this->slug = $slug;
        $this->title = $title;
        $this->faIcon = $faIcon;
    }

    public function on($slug, $handler, $quickAction = false){
        $this->actions[$slug] = $handler;
        if($quickAction){
            $this->quickActions[] = array(
                "title" => $quickAction,
                "url" => "/admin/" . $this->slug . "/" . $slug
            );
        }
    }

    public function post($action, $handler){
        $this->postActions[$action] = $handler;
    }

    public function action($url, $title, $class = false){
        $this->quickActions[] = array(
            "title" => $title,
            "url" => $url,
            "class" => $class
        );
    }

    public function api($action, $handler){
        $this->apiActions[$action] = $handler;
    }

    public function exec($slug, $req, $res){
        if(!array_key_exists($slug, $this->actions)){
            $res->status(404);
            return $res->renderError(false, SnippetQuery::getOne('error-404'))->lock();
        }
        return $this->actions[$slug]($req, $res);
    }

    public function execPost($action, $req, $res){
        if(!array_key_exists($action, $this->postActions)){
            $res->status(404);
            return $res->renderError(false, SnippetQuery::getOne('error-404'))->lock();
        }
        return $this->postActions[$action]($req, $res);
    }

    public function execApi($action, $req, $res){
        if(!array_key_exists($action, $this->apiActions)){
            $res->status(404);
            return $res->renderError(false, SnippetQuery::getOne('error-404'))->lock();
        }
        return $this->apiActions[$action]($req, $res);
    }

    public function getQuickActions(){
        return $this->quickActions;
    }

}

class AdminModuleCollection {
    private $modules = [];

    public function getModules(){
        return $this->modules;
    }

    public function getModule($slug){
        foreach ($this->modules as $module) {
            if($module->slug == $slug){
                return $module;
            }
        }
        return false;
    }

    public function addModule($module){
        $this->modules[] = $module;
    }
}

$adminColl = new AdminModuleCollection();

require_once('admin/calendar.php');
require_once('admin/config.php');
require_once('admin/files.php');
require_once('admin/news.php');
require_once('admin/snippets.php');
require_once('admin/user.php');
require_once('admin/mail.php');
require_once('admin/sponsors.php');

$adminColl->addModule($snippetsModule);
$adminColl->addModule($newsModule);
$adminColl->addModule($userModule);
$adminColl->addModule($calendarModule);
$adminColl->addModule($filesModule);
$adminColl->addModule($configModule);
$adminColl->addModule($mailModule);
$adminColl->addModule($sponsorsModule);

$adminOverviewRoute = function($req, $res){
    global $adminColl;

    $res->render('admin.twig', array(
        'activeModule' => false,
        'activeAction' => false,
        'quickActions' => false,
        'modules' => $adminColl->getModules()
    ));
};

$adminActionRoute = function($req, $res){
    global $adminColl;

    $activeModuleSlug = $req->params['module'];
    $activeActionSlug = empty($req->params['action']) ? 'view' : $req->params['action'];

    $activeModule = $adminColl->getModule($activeModuleSlug);
    if(!$activeModule){
        return $res->status(404)->renderError(false, '404')->lock();
    }

    $actionData = $activeModule->exec($activeActionSlug, $req, $res);

    $res->set('activeModule', $activeModuleSlug)
        ->set('activeAction', $activeActionSlug)
        ->set('quickActions', $activeModule->getQuickActions())
        ->set('modules', $adminColl->getModules())
        ->set('template', $activeModuleSlug . '/' . ($activeActionSlug == 'update' ? 'create' : $activeActionSlug))
        ->set('data', $actionData)
        ->set('validation', false)
        ->set('success', array_key_exists('success', $req->query) && $req->query['success'] == 1)
        ->set('action', false)
        ->set('snippet', false)
        ->render('admin.twig');
};

$adminPostAction = function($req, $res){
    global $adminColl;

    $activeModuleSlug = $req->params['module'];
    $activeActionSlug = $req->params['action'];

    $module = $adminColl->getModule($activeModuleSlug);
    $module->execPost($activeActionSlug, $req, $res);

    $res->set('activeModule', $activeModuleSlug)
        ->set('activeAction', $activeActionSlug)
        ->set('quickActions', $module->getQuickActions())
        ->set('modules', $adminColl->getModules())
        ->set('template', $activeModuleSlug . '/' . ($activeActionSlug == 'update' ? 'create' : $activeActionSlug))
        ->set('validation', false)
        ->set('action', false)
        ->set('snippet', false)
        ->render('admin.twig');
};

$adminApiRoute = function($req, $res){
    global $adminColl;

    $activeModuleSlug = $req->params['module'];
    $activeActionSlug = $req->params['action'];

    $module = $adminColl->getModule($activeModuleSlug);
    $data = $module->execApi($activeActionSlug, $req, $res);
    $res->renderData($data);
};
