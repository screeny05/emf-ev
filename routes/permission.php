<?hh
const PERM_NOT_LOGGED_IN = 0;
const PERM_LOGGED_IN = 1;
const PERM_HAS_ROLE = 2;

$permissionRoute = function($perm, $options = false){

    return function($req, $res) use ($perm, $options){
        $snippets = SnippetQuery::getAll();

        if($perm === PERM_LOGGED_IN){
            $user = $res->get('user');

            if(!$user){
                $res->status(401);
                $res->renderError(false, $snippets['error-401']);
                $res->lock();
            }
        } elseif($perm === PERM_NOT_LOGGED_IN){
            $user = $res->get('user');

            if($user){
                $res->status(401);
                $res->renderError(false, $snippets['error-loggedin']);
                $res->lock();
            }
        } elseif($perm === PERM_HAS_ROLE){
            $group = $res->get('group');
            $rolePath = '';
            $cannot = [];
            foreach($options as $roleFragment){
                if(strlen($rolePath) > 0){
                    $rolePath .= '-';
                }
                if(substr($roleFragment, 0, 1) === ':'){
                    $roleFragment = $req->params[substr($roleFragment, 1)];
                }
                $rolePath .= $roleFragment;
                if(!$group || !$group->can($rolePath)){
                    $cannot[] = $rolePath;
                }
            }

            if(!empty($cannot)){
                $cannotStr = array_map(function($roleKey){
                    $role = AccessRoleQuery::create()->findPk($roleKey);
                    if(!$role){
                        return '<li>' . $roleKey . '</li>';
                    } else {
                        return '<li>' . $role->getTitle() . ' (' . $roleKey . ')</li>';
                    }
                }, $cannot);
                $res->status(401)
                    ->renderError(false, 'Dir fehlen folgende Berechtigungen: <ul>' . implode($cannotStr) . '</ul>')
                    ->lock();
            }
        }
    };
};
