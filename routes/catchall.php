<?php
$catchallRoute = function($req, $res){
    $snippets = SnippetQuery::getAll();
    $res->status(404);
    $res->renderError(false, $snippets['error-404']);
};
