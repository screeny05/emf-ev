<?php
$apiRoute = function($req, $res){
    $endpoint = 'https://api.cloudflare.com/client/v4/';
    $authKey = '7659bec63015cd0b2359c487bc1ae2f9bb970';
    $authEmail = 'screeny05@gmail.com';

    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $endpoint . 'zones');
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HTTPHEADER, [
        'X-Auth-Key: ' . $authKey,
        'X-Auth-Email: ' . $authEmail
    ]);

    $result = curl_exec($ch);
    $resultCode = curl_getinfo($ch, CURLINFO_HTTP_CODE);

    curl_close($ch);
    $res
        ->status($resultCode)
        ->raw($result);
};
