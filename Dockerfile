FROM ubuntu:16.04

MAINTAINER Sebastian Langer <sl@scn.cx>

ENV MYSQL_MAJOR 5.7
ENV MYSQL_VERSION 5.7.12-1debian8

# Install base packages
RUN apt-get update && apt-get install -y \
    ca-certificates \
    wget \
    nano \
    software-properties-common \
    python-software-properties \
    perl \
    pwgen \
    sendmail \
    apt-transport-https

# Add HHVM-Repo
RUN apt-key adv --recv-keys --keyserver hkp://keyserver.ubuntu.com:80 0xB4112585D386EB94 && add-apt-repository https://dl.hhvm.com/ubuntu

# Install hhvm & nginx
RUN apt-get update && apt-get install -y \
    hhvm \
    nginx

RUN echo "mysql-server-5.7 mysql-server/root_password password root" | debconf-set-selections
RUN echo "mysql-server-5.7 mysql-server/root_password_again password root" | debconf-set-selections
RUN apt-get install -y mysql-server-5.7
RUN service mysql start
RUN mysql_secure_installation
RUN sed -i 's/127\.0\.0\.1/0\.0\.0\.0/g' /etc/mysql/my.cnf
RUN mysql -uroot -p -e 'USE mysql; UPDATE `user` SET `Host`="%" WHERE `User`="root" AND `Host`="localhost"; DELETE FROM `user` WHERE `Host` != "%" AND `User`="root"; FLUSH PRIVILEGES;'

# Configure server
COPY nginx.conf /etc/nginx/sites-available/default
COPY docker_run.sh /docker_run.sh
COPY docker_dump.sql /docker_dump.sql

RUN service mysql start && \
    mysql -u root -e "CREATE DATABASE emf;" && \
    mysql -u root emf < /docker_dump.sql && \
    rm /docker_dump.sql && \
    service mysql stop

RUN echo "daemon off;" >> /etc/nginx/nginx.conf
RUN echo "RUN_AS_USER=\"root\"" >> /etc/default/hhvm && echo "RUN_AS_GROUP=\"root\"" >> /etc/default/hhvm

EXPOSE 80

CMD ["/bin/bash", "/docker_run.sh"]
