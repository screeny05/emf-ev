var getPin = function(blocks, blocklength, seperator){
	var pool = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ1234567890'.split('');
	var result = [];

	var r = function(min, max) {
		return Math.floor(Math.random() * (max - min)) + min;
	};

	while(result.length < blocks * blocklength){
		result.push(pool[r(0, pool.length)]);
	}

	return result.map(function(val, i){
		return ((i + 1) % 4 === 0 && i !== result.length - 1) ? val + seperator : val;
	}).join('');
};

var getSlugified = function(input){
	var from = "ãàáäâẽèéëêìíïîõòóöôùúüûñç·/_,:;";
	var to   = "aaaaaeeeeeiiiiooooouuuunc------";
	for (var i=0, l=from.length ; i<l ; i++) {
		input = input.replace(new RegExp(from.charAt(i), 'g'), to.charAt(i));
	}

	return input
		.toLowerCase()
		.replace(/\s+/g, '-')
		.replace(/[^\w-]+/g, '')
		.replace(/[-]+/g, '-')
		.replace(/^-+/, '')
		.replace(/-+$/, '');
};

var calendarSelectEvent = function(event){
	window.location = '/admin/calendar/update/' + event.id;
	return false;
};

var calendarClickEvent = function(date){
	window.location = '/admin/calendar/create/?date=' + (date / 1000);
};

$(function(){
	window.user = false;
	var $dataUser = $('#js-data-user');

	try {
		window.user = JSON.parse($dataUser.text());
	} catch(e) {}

	$('.btn-pin-generate').click(function(e){
		e.preventDefault();
		$('#' + $(this).data('input')).val(getPin(4, 4, '-'));
	});
	$('.btn-now').click(function(e){
		e.preventDefault();
		var now = new Date();
		var pad = function(i){ return (''+i).length === 1 ? '0' + i : i; };
		$('#' + $(this).data('input')).val(
			now.getFullYear() + '-' + pad(now.getMonth() + 1) + '-' + pad(now.getDate()) +
			' ' + pad(now.getHours()) + ':' + pad(now.getMinutes()) + ':' + pad(now.getSeconds())
		);
	});
	$('.btn-me').click(function(e){
		e.preventDefault();
		var $input = $('#' + $(this).data('input'));
		if(window.user){
			$input.val(window.user.Firstname + ' ' + window.user.Lastname);
		}
	})
	$('.btn-news-slugify').click(function(e){
		e.preventDefault();
		$('#' + $(this).data('input')).val(getSlugified($('#Title').val()));
	});
	$('.btn-select-icon').click(function(e){
		e.preventDefault();
		var target = $('#' + $(this).data('input'));
		$('#btn-select-icon-modal').remove();
		var ul = $('<div class="row" id="btn-select-icon-modal"><div class="colum span_12 offset_2"><ul class="nav nav-icons"></ul></div></div>')
			.insertAfter(target.parents('.row').first())
			.css('margin', '14px 0')
			.find('ul');
		$.each(icons.files, function(key, value){
			var icon = typeof value === 'string' ? value : value.fa;
			ul.append('<li><a class="fa fa-' + icon + '" data-icon="' + icon + '" href="#"></a></li>');
		});
		ul.find('.fa').click(function(e){
			e.preventDefault();
			target.val($(this).data('icon'));
			ul.parents('.row').first().remove();
		});
	});
	$('#File').change(function(e){
		var $this = $(this);
		if(!$this[0].files || !$this[0].files.length){
			return;
		}
		var cdn = $this.parents('form').first().data('filePrefix');
		$('#Path').text(cdn + '/' + $this[0].files[0].name);
	});
	$('.btn-select-all').click(function(e){
		e.preventDefault();
		$('input[type="checkbox"]').attr('checked', 'checked');
	});
	$('.btn-select-media').click(function(e){
		e.preventDefault();

		var $target = $(e.target);
		var $input = $($target.data('target'));

		if($target.data('is-active')){
			return $target
				.data('is-active', false)
				.parent()
				.find('.popup')
				.remove();
		}

		$.get('/api/admin/files/files', function(data){
			data = JSON.parse(data);

			var $popup = $('<div class="popup"></div>');
			var $folderList = $('<ul></ul>');
			$.each(data, function(i, folder){
				if(folder.Files.length === 0){
					return;
				}

				var $folder = $('<li></li>').text(folder.Name);
				var $fileList = $('<ul></ul>');

				$.each(folder.Files, function(i, file){
					var $file = $('<li></li>');
					var $fileLink = $('<a href="#" class="media-selection"></a>').text(file.Name);
					$fileLink.appendTo($file);
					$fileLink.data('file', file);
					$file.appendTo($fileList);
				});

				$fileList.appendTo($folder);
				$folder.appendTo($folderList);
			});
			$folderList.appendTo($popup);

			$popup.appendTo($target.parent());

			$target.data('is-active', true);

			$popup.on('click', '.media-selection', function(e){
				e.preventDefault();
				var $file = $(e.target);
				$target.data('is-active', false);

				$input.val($file.data('file').Path);

				$popup.remove();
			});
		});
	});

	$('input[type="datetime"]').datetimepicker({ lang: 'de', format: 'Y-m-d H:i:00' });

	$('.calendar').each(function(i, el){
		var $el = $(el);

		var eventSources = [{
			googleCalendarId: $el.data('calendarid'),
			className: 'public'
		}];

		if($el.data('showholidays')){
			eventSources.push({
				googleCalendarId: 'de.german#holiday@group.v.calendar.google.com',
				className: 'holidays'
			});
		}

		$el.fullCalendar({
			googleCalendarApiKey: $el.data('calendarapikey'),
			eventSources: eventSources,
			aspectRatio: 1.4,
			lang: 'de',
			eventClick: calendarSelectEvent,
			dayClick: calendarClickEvent
		});
	});
});

var icons = {
	files: [
		'file-o',
		{ fa: 'file-text-o', ext: ['.txt', '.md', '.ini'] },
		{ fa: 'file-pdf-o', ext: ['.pdf'] },
		{ fa: 'file-word-o', ext: ['.doc', '.docx'] },
		{ fa: 'file-excel-o', ext: ['.xls', '.xlsx', '.csv', '.tsv'] },
		{ fa: 'file-powerpoint-o', ext: ['.ppt', '.pptx'] },
		{ fa: 'file-image-o', ext: ['.jpg', '.jpeg', '.gif', '.png', '.webp', '.tiff', '.bmp', '.ico'] },
		{ fa: 'file-archive-o', ext: ['.zip', '.rar', '.7z', '.tar', '.gz', '.bz', '.ar', '.iso', '.bz2', '.lz', '.lzma', '.z', '.cab', '.dmg', '.jar', '.kgb', '.sfx'] },
		{ fa: 'file-audio-o', ext: ['.mp3', '.wav', '.wma', '.ape', '.m4a', '.midi', '.aac', '.aiff', '.flac', '.m4p', '.mpc', '.ogg', '.opus', '.flp'] },
		{ fa: 'file-video-o', ext: ['.mkv', '.mp4', '.webm', '.avi', '.flv', '.vob', '.ogv', '.mov', '.qt', '.wmv', '.asf', '.m4v', '.mpg', '.mpeg', '.3gp'] },
		{ fa: 'file-code-o', ext: ['.py', '.pl', '.php', '.js', '.html', '.css', '.h', '.cpp', '.c', '.cs', '.vb', '.java', '.json', '.xml', '.lua', '.bat', '.sh'] },
	],
	misc: [
		"fa-adjust","fa-adn","fa-align-center","fa-align-justify","fa-align-left","fa-align-right","fa-ambulance","fa-anchor","fa-android","fa-angellist","fa-angle-double-down",
		"fa-angle-double-left","fa-angle-double-right","fa-angle-double-up","fa-angle-down","fa-angle-left","fa-angle-right","fa-angle-up","fa-apple","fa-archive","fa-area-chart",
		"fa-arrow-circle-down","fa-arrow-circle-left","fa-arrow-circle-o-down","fa-arrow-circle-o-left","fa-arrow-circle-o-right","fa-arrow-circle-o-up","fa-arrow-circle-right",
		"fa-arrow-circle-up","fa-arrow-down","fa-arrow-left","fa-arrow-right","fa-arrow-up","fa-arrows","fa-arrows-alt","fa-arrows-h","fa-arrows-v","fa-asterisk","fa-at","fa-backward",
		"fa-ban","fa-bar-chart","fa-barcode","fa-bars","fa-bed","fa-beer","fa-behance","fa-behance-square","fa-bell","fa-bell-o","fa-bell-slash","fa-bell-slash-o","fa-bicycle",
		"fa-binoculars","fa-birthday-cake","fa-bitbucket","fa-bitbucket-square","fa-bold","fa-bolt","fa-bomb","fa-book","fa-bookmark","fa-bookmark-o","fa-briefcase","fa-btc","fa-bug",
		"fa-building","fa-building-o","fa-bullhorn","fa-bullseye","fa-bus","fa-buysellads","fa-calculator","fa-calendar","fa-calendar-o","fa-camera","fa-camera-retro","fa-car",
		"fa-caret-down","fa-caret-left","fa-caret-right","fa-caret-square-o-down","fa-caret-square-o-left","fa-caret-square-o-right","fa-caret-square-o-up","fa-caret-up","fa-cart-arrow-down",
		"fa-cart-plus","fa-cc","fa-cc-amex","fa-cc-discover","fa-cc-mastercard","fa-cc-paypal","fa-cc-stripe","fa-cc-visa","fa-certificate","fa-chain-broken","fa-check","fa-check-circle",
		"fa-check-circle-o","fa-check-square","fa-check-square-o","fa-chevron-circle-down","fa-chevron-circle-left","fa-chevron-circle-right","fa-chevron-circle-up","fa-chevron-down",
		"fa-chevron-left","fa-chevron-right","fa-chevron-up","fa-child","fa-circle","fa-circle-o","fa-circle-o-notch","fa-circle-thin","fa-clipboard","fa-clock-o","fa-cloud",
		"fa-cloud-download","fa-cloud-upload","fa-code","fa-code-fork","fa-codepen","fa-coffee","fa-cog","fa-cogs","fa-columns","fa-comment","fa-comment-o","fa-comments","fa-comments-o",
		"fa-compass","fa-compress","fa-connectdevelop","fa-copyright","fa-credit-card","fa-crop","fa-crosshairs","fa-css3","fa-cube","fa-cubes","fa-cutlery","fa-dashcube","fa-database",
		"fa-delicious","fa-desktop","fa-deviantart","fa-diamond","fa-digg","fa-dot-circle-o","fa-download","fa-dribbble","fa-dropbox","fa-drupal","fa-eject","fa-ellipsis-h","fa-ellipsis-v",
		"fa-empire","fa-envelope","fa-envelope-o","fa-envelope-square","fa-eraser","fa-eur","fa-exchange","fa-exclamation","fa-exclamation-circle","fa-exclamation-triangle","fa-expand",
		"fa-external-link","fa-external-link-square","fa-eye","fa-eye-slash","fa-eyedropper","fa-facebook","fa-facebook-official","fa-facebook-square","fa-fast-backward","fa-fast-forward",
		"fa-fax","fa-female","fa-fighter-jet","fa-file","fa-file-archive-o","fa-file-audio-o","fa-file-code-o","fa-file-excel-o","fa-file-image-o","fa-file-o","fa-file-pdf-o",
		"fa-file-powerpoint-o","fa-file-text","fa-file-text-o","fa-file-video-o","fa-file-word-o","fa-files-o","fa-film","fa-filter","fa-fire","fa-fire-extinguisher","fa-flag",
		"fa-flag-checkered","fa-flag-o","fa-flask","fa-flickr","fa-floppy-o","fa-folder","fa-folder-o","fa-folder-open","fa-folder-open-o","fa-font","fa-forumbee","fa-forward","fa-foursquare",
		"fa-frown-o","fa-futbol-o","fa-gamepad","fa-gavel","fa-gbp","fa-gift","fa-git","fa-git-square","fa-github","fa-github-alt","fa-github-square","fa-glass","fa-globe","fa-google",
		"fa-google-plus","fa-google-plus-square","fa-google-wallet","fa-graduation-cap","fa-gratipay","fa-h-square","fa-hacker-news","fa-hand-o-down","fa-hand-o-left","fa-hand-o-right",
		"fa-hand-o-up","fa-hdd-o","fa-header","fa-headphones","fa-heart","fa-heart-o","fa-heartbeat","fa-history","fa-home","fa-hospital-o","fa-html5","fa-ils","fa-inbox","fa-indent",
		"fa-info","fa-info-circle","fa-inr","fa-instagram","fa-ioxhost","fa-italic","fa-joomla","fa-jpy","fa-jsfiddle","fa-key","fa-keyboard-o","fa-krw","fa-language","fa-laptop",
		"fa-lastfm","fa-lastfm-square","fa-leaf","fa-leanpub","fa-lemon-o","fa-level-down","fa-level-up","fa-life-ring","fa-lightbulb-o","fa-line-chart","fa-link","fa-linkedin",
		"fa-linkedin-square","fa-linux","fa-list","fa-list-alt","fa-list-ol","fa-list-ul","fa-location-arrow","fa-lock","fa-long-arrow-down","fa-long-arrow-left",
		"fa-long-arrow-right","fa-long-arrow-up","fa-magic","fa-magnet","fa-male","fa-map-marker","fa-mars","fa-mars-double","fa-mars-stroke","fa-mars-stroke-h",
		"fa-mars-stroke-v","fa-maxcdn","fa-meanpath","fa-medium","fa-medkit","fa-meh-o","fa-mercury","fa-microphone","fa-microphone-slash","fa-minus","fa-minus-circle",
		"fa-minus-square","fa-minus-square-o","fa-mobile","fa-money","fa-moon-o","fa-motorcycle","fa-music","fa-neuter","fa-newspaper-o","fa-openid","fa-outdent","fa-pagelines",
		"fa-paint-brush","fa-paper-plane","fa-paper-plane-o","fa-paperclip","fa-paragraph","fa-pause","fa-paw","fa-paypal","fa-pencil","fa-pencil-square","fa-pencil-square-o",
		"fa-phone","fa-phone-square","fa-picture-o","fa-pie-chart","fa-pied-piper","fa-pied-piper-alt","fa-pinterest","fa-pinterest-p","fa-pinterest-square","fa-plane","fa-play","fa-play-circle",
		"fa-play-circle-o","fa-plug","fa-plus","fa-plus-circle","fa-plus-square","fa-plus-square-o","fa-power-off","fa-print","fa-puzzle-piece","fa-qq","fa-qrcode","fa-question","fa-question-circle",
		"fa-quote-left","fa-quote-right","fa-random","fa-rebel","fa-recycle","fa-reddit","fa-reddit-square","fa-refresh","fa-renren","fa-repeat","fa-reply","fa-reply-all","fa-retweet",
		"fa-road","fa-rocket","fa-rss","fa-rss-square","fa-rub","fa-scissors","fa-search","fa-search-minus","fa-search-plus","fa-sellsy","fa-server","fa-share","fa-share-alt",
		"fa-share-alt-square","fa-share-square","fa-share-square-o","fa-shield","fa-ship","fa-shirtsinbulk","fa-shopping-cart","fa-sign-in","fa-sign-out","fa-signal","fa-simplybuilt",
		"fa-sitemap","fa-skyatlas","fa-skype","fa-slack","fa-sliders","fa-slideshare","fa-smile-o","fa-sort","fa-sort-alpha-asc","fa-sort-alpha-desc","fa-sort-amount-asc",
		"fa-sort-amount-desc","fa-sort-asc","fa-sort-desc","fa-sort-numeric-asc","fa-sort-numeric-desc","fa-soundcloud","fa-space-shuttle","fa-spinner","fa-spoon","fa-spotify",
		"fa-square","fa-square-o","fa-stack-exchange","fa-stack-overflow","fa-star","fa-star-half","fa-star-half-o","fa-star-o","fa-steam","fa-steam-square","fa-step-backward",
		"fa-step-forward","fa-stethoscope","fa-stop","fa-street-view","fa-strikethrough","fa-stumbleupon","fa-stumbleupon-circle","fa-subscript","fa-subway","fa-suitcase",
		"fa-sun-o","fa-superscript","fa-table","fa-tablet","fa-tachometer","fa-tag","fa-tags","fa-tasks","fa-taxi","fa-tencent-weibo","fa-terminal","fa-text-height",
		"fa-text-width","fa-th","fa-th-large","fa-th-list","fa-thumb-tack","fa-thumbs-down","fa-thumbs-o-down","fa-thumbs-o-up","fa-thumbs-up","fa-ticket","fa-times",
		"fa-times-circle","fa-times-circle-o","fa-tint","fa-toggle-off","fa-toggle-on","fa-train","fa-transgender","fa-transgender-alt","fa-trash","fa-trash-o","fa-tree",
		"fa-trello","fa-trophy","fa-truck","fa-try","fa-tty","fa-tumblr","fa-tumblr-square","fa-twitch","fa-twitter","fa-twitter-square","fa-umbrella","fa-underline",
		"fa-undo","fa-university","fa-unlock","fa-unlock-alt","fa-upload","fa-usd","fa-user","fa-user-md","fa-user-plus","fa-user-secret","fa-user-times","fa-users",
		"fa-venus","fa-venus-double","fa-venus-mars","fa-viacoin","fa-video-camera","fa-vimeo-square","fa-vine","fa-vk","fa-volume-down","fa-volume-off","fa-volume-up",
		"fa-weibo","fa-weixin","fa-whatsapp","fa-wheelchair","fa-wifi","fa-windows","fa-wordpress","fa-wrench","fa-xing","fa-xing-square","fa-yahoo","fa-yelp","fa-youtube","fa-youtube-play","fa-youtube-square"
	]
};

var findIcon = function(file){
	var icon = 'file-o';

	$.each(icons.files, function(k, v){
		$.each(v.ext, function(i, ext){
			if(file.endsWith(ext)){
				icon = this;
			}
		});
	});

	return 'fa-' + icon;
};

if (typeof String.prototype.endsWith !== 'function') {
    String.prototype.endsWith = function(suffix) {
        return this.indexOf(suffix, this.length - suffix.length) !== -1;
    };
}
