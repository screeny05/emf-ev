$(function(){
    var flickr = new flickrApi(emfConfig.flickr.key);

    flickr.req('flickr.photosets.getList', { 'user_id': emfConfig.flickr.user }, function(err, data){
        if(err){
            return showError(err.stat, err.message);
        }

        $.each(data.photosets.photoset, function(i, pset){
            flickr.req('flickr.photosets.getPhotos', { 'photoset_id': pset.id }, function(err, data){
                if(err){
                    return showError(err.stat, err.message);
                }
                console.log(data);

                $.each(data.photoset.photo, function(i, photo){
                    flickr.req('flickr.photos.getSizes', { 'photo_id': photo.id }, function(err, data){
                        if(err){
                            return showError(err.stat, err.message);
                        }
                        $('#gallery').append('<a href="/gallery/' + photo.id + '"><img src="' + data.sizes.size[1].source + '"/></a>');
                    });
                });
            });
        });
    });
});

function flickrApi(key){
    this.key = key;

    var urlBase = 'https://api.flickr.com/services/rest/?format=json&api_key=' + this.key;

    this.req = function(method, data, callback){
        if(typeof data == 'function'){
            callback = data;
            data = {};
        }
        callback = callback || function(){};
        var jsFunc = 'x' + guid('');
        window[jsFunc] = function(data){
            window[jsFunc] = undefined;
            return callback(data.stat == 'fail' ? data : false, data.stat != 'fail' ? data : false);
        };

        var url = urlBase
            + "&jsoncallback=" + jsFunc
            + "&method=" + method;

        url += $.map(data, function(value, key,c){
            return '&' + key + '=' + value;
        }).join('');

        $('<script type="text/javascript" src="' + url + '"/>').appendTo('head');
    };
}

function showError(code, error){
    console.log('[' + code + ']', error);
}
