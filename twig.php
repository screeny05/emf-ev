<?hh
session_start();
require_once('include/headers.php');

$loader = new Twig_Loader_Filesystem('views');
$twig = new Twig_Environment($loader, array(
    'cache' => ConfigQuery::getOne('Environment') === 'dev' ? false : 'cache',
));

$twig->addExtension(new Twig_Extensions_Extension_Text());
$twig->getExtension('core')->setTimezone('Europe/Berlin');

$twig->addGlobal('navbar_items', array(
    SnippetQuery::getOne('TopbarLinkHome') => "/#content",
    SnippetQuery::getOne('TopbarLinkWhere') => "/where/",
    "divider0" => "null",
    SnippetQuery::getOne('TopbarSponsor') => "/sponsors/",
    SnippetQuery::getOne('TopbarLinkNews') => "/news/",
    "divider1" => "null",
    SnippetQuery::getOne('TopbarLinkDownloads') => "/downloads/",
    SnippetQuery::getOne('TopbarLinkMusic') => ConfigQuery::getOne('MusicUrl'),
    SnippetQuery::getOne('TopbarLinkPics') => ConfigQuery::getOne('PicsUrl'),
    SnippetQuery::getOne('TopbarLinkShop') => ConfigQuery::getOne('ShopUrl'),
));
$twig->addFunction(new Twig_SimpleFunction('csrf', function(){
    return CsrfProtector::get();
}));
$twig->addFunction(new Twig_SimpleFunction('s', function($id){
    return SnippetQuery::getOne($id);
}, [ 'is_safe' => [ 'html' ] ]));
$twig->addFunction(new Twig_SimpleFunction('c', function($id){
    return ConfigQuery::getOne($id);
}, [ 'is_safe' => [ 'html' ] ]));
$twig->addFilter(new Twig_SimpleFilter('fileSize', function($bytes, $precision = 1) {
    $sz = ['B', 'KB', 'MB', 'GB', 'TB', 'PB'];
    $factor = floor((strlen($bytes) - 1) / 3);
    return sprintf("%.{$precision}f", $bytes / pow(1024, $factor)) . @$sz[$factor];
}));
$twig->addFilter(new Twig_SimpleFilter('*Path', function($type, $arg){
    $isDev = ConfigQuery::getOne('Environment') === 'dev';
    $basePath = $isDev ? '/assets/' : '/dist/';


    if($type === 'js'){
        $basePath .= 'js/';
    } elseif($type === 'css'){
        $basePath = '/dist/css/';
    } elseif($type === 'img') {
        $basePath .= 'img/';
    } elseif($type === 'vendor'){
        $basePath = '/dist/vendor/';
    } else {
        $basePath = '/files/';
    }

    if(!$isDev){
        $basePath = trim(ConfigQuery::getOne('FilePrefix'), '/') . $basePath;
    }

    return $basePath . $arg;
}));

$app = new Router($twig);

$app->useGlobal(function($req, $res){
    $user = array_key_exists('sid', $_COOKIE) ? UserQuery::create()->filterBySessionId($_COOKIE['sid'])->findOne() : false;
    $group = false;
    if(!$user){
        $user = false;
        $group = AccessGroupQuery::create()->findPk('default');
    } else {
        $group = $user->getGroup();
    }
    $res->set('user', $user)
        ->set('group', $group);
});

$app->useGlobal(function($req, $res){
    $canonical = ConfigQuery::getOne('DefaultRequestScheme') . '://' . ConfigQuery::getOne('Domain') . $req->path;
    $res->set('canonical', $canonical);
});

$app->get(array('/where/', '/impressum/', '/gallery/', '/'), $staticRoute);
$app->get('/sponsors/', Sponsors::routeMain());
$app->get('/news/', News::routeMain());
$app->get('/news/:id-*slug', News::routeArticle());

$app->post('/user/login', CsrfProtector::route(), $permissionRoute(PERM_NOT_LOGGED_IN), User::routeLogin());
$app->post('/user/logout', CsrfProtector::route(), $permissionRoute(PERM_LOGGED_IN), User::routeLogout());
$app->post('/user/register', CsrfProtector::route(), $permissionRoute(PERM_NOT_LOGGED_IN), User::routeRegister());
$app->get('/user/register',  $permissionRoute(PERM_NOT_LOGGED_IN), User::routeRegisterView());

$app->get('/admin/', $permissionRoute(PERM_HAS_ROLE, ['admin']), $adminOverviewRoute);
$app->get('/admin/:module/', $permissionRoute(PERM_HAS_ROLE, ['admin', ':module']), $adminActionRoute);
$app->get('/admin/:module/:action', $permissionRoute(PERM_HAS_ROLE, ['admin', ':module', ':action']), $adminActionRoute);
$app->get('/admin/:module/:action/:id', $permissionRoute(PERM_HAS_ROLE, ['admin', ':module', ':action']), $adminActionRoute);

$app->post('/admin/:module/:action', CsrfProtector::route(), $permissionRoute(PERM_HAS_ROLE, ['admin', ':module', ':action']), $adminPostAction);
$app->post('/admin/:module/:action/:id', CsrfProtector::route(), $permissionRoute(PERM_HAS_ROLE, ['admin', ':module', ':action']), $adminPostAction);

$app->get('/downloads/', File::routeMain());
$app->get('/downloads/:id-*slug', File::routeFile());

$app->get('/error-document/:code', $errorRoute);

$app->get('/api/calendar/notificationCron', $calendarNotificationCron);
$app->get('/api/admin/:module/:action', $permissionRoute(PERM_HAS_ROLE, ['admin', ':module']), $adminApiRoute);


$app->all('/*x', $catchallRoute);

$app->route($_SERVER['REQUEST_METHOD'], $_SERVER['REQUEST_URI']);
