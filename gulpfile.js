var gulp = require('gulp');

var exec = require('child_process').exec;

var sloc = require('gulp-sloc');
var bower = require('gulp-bower');
var rename = require('gulp-rename');
var uglify = require('gulp-uglify');
var concat = require('gulp-concat');
var jshint = require('gulp-jshint');
var jsedit = require('gulp-json-editor');
var compass = require('gulp-compass');
var phplint = require('phplint').lint;
var imagemin = require('gulp-imagemin');
var composer = require('gulp-composer');
var scsslint = require('gulp-scss-lint');
var sourcemaps = require('gulp-sourcemaps');

gulp.task('build:js', function(){
    gulp.src('assets/js/**.js')
        //.pipe(concat('main.js'))
        .pipe(sourcemaps.init())
        .pipe(uglify({
            outSourceMap: true,
            basePath: 'dist/js'
        }))
        .pipe(sourcemaps.write('.'))
        .pipe(gulp.dest('dist/js'));
});

gulp.task('build:css', function(){
    gulp.src('assets/sass/*.scss')
        .pipe(compass({
            http_path: '/',
            sourcemap: true,
            css: 'dist/css',
            image: 'dist/img',
            sass: 'assets/sass',
            require: ['sass-css-importer'],
        }))
        .pipe(gulp.dest('dist/css'));
});

gulp.task('build:img', function(){
    gulp.src('assets/img/**')
        .pipe(imagemin({
            progressive: true
        }))
        .pipe(gulp.dest('dist/img'));
});

gulp.task('build:propel', function(cb){
    exec('vendor/propel/propel/bin/propel build --config-dir include --schema-dir include --output-dir include/models', function(err, stdout, stderr){
        console.log(stdout);
        console.log(stderr);

        if(err)
            return cb(err);

        exec('vendor/propel/propel/bin/propel convert-conf --config-dir include --output-dir include --output-file db.php', function(err, stdout, stderr){
            console.log(stdout);
            console.log(stderr);
            cb(err);
        });
    });
});

gulp.task('build:sql', function(cb){
    exec('vendor/propel/propel/bin/propel sql:build --config-dir include --schema-dir include --overwrite', function(err, stdout, stderr){
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});

gulp.task('build:autoload', function(cb){
    composer('dumpautoload', { optimize: true, cwd: '.', bin: './composer.phar' });
});

gulp.task('prepare:bower', function(){
    return bower()
        .pipe(gulp.dest('dist/vendor'));
});


gulp.task('prepare:composer', function(){
    composer({ cwd: '.', bin: './composer.phar' });
});

gulp.task('prepare:bundler', function(cb){
    exec('bundler install --full-index', function(err, stdout, stderr){
        console.log(stdout);
        console.log(stderr);
        cb(err);
    });
});


gulp.task('config:propel', function(){
    gulp.src('./include/config.json')
        .pipe(jsedit(function(obj){
            return { propel: obj.propel }
        }))
        .pipe(rename('propel.json'))
        .pipe(gulp.dest('include'));
});


gulp.task('lint:scss', function(){
    gulp.src('assets/sass/**/*.scss')
        .pipe(scsslint({
            'bundleExec': true,
            'sync': true,
            'config': 'scsslint.yml'
        }));
});

gulp.task('lint:js', function(){
    gulp.src('assets/js/**/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'))
});

gulp.task('lint:php', function(cb){
    phplint(['*.php', 'include/*.php', 'include/models/*.php'], { limit: 10, stdout: true, stderr: true }, function(err, stdout, stderr){
        if (err) throw new Error(err);

        console.log(stdout ? stdout : '');
        console.log(stderr ? stderr : '');
    });
});


gulp.task('stats', function(){
    gulp.src(['assets/js/**/*.js', 'include/*.php', 'include/models/*.php', '*.php'])
        .pipe(sloc());
});


gulp.task('lint', ['lint:js', 'lint:scss', 'lint:php']);
gulp.task('config', ['config:propel']);
gulp.task('prepare', ['prepare:bundler', 'prepare:composer', 'prepare:bower']);
gulp.task('build', ['build:img', 'build:js', 'build:css', 'build:propel', 'build:autoload']);
gulp.task('default', ['stats', 'config', 'prepare', 'build']);

gulp.task('db', ['config:propel', 'build:propel', 'build:autoload']);
