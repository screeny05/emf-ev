<?php

namespace EMF\Markdown;

use League\CommonMark\Converter;
use League\CommonMark\DocParser;
use League\CommonMark\Environment;
use League\CommonMark\HtmlRenderer;
use League\CommonMark\Inline\Parser\AbstractInlineParser;
use League\CommonMark\ContextInterface;
use League\CommonMark\InlineParserContext;
use League\CommonMark\Inline\Element\Html;

/**
 * Enables embedding of media-elements, e.g. youtube. Use like this:
 * `<< https://youtube.com/watch?v=Jmqa99Ar1Hs`
 */
class EmbedParser extends AbstractInlineParser {

    public function getCharacters() {
        return ['<'];
    }

    public function parse(ContextInterface $context, InlineParserContext $inlineContext)
    {
        $cursor = $inlineContext->getCursor();

        $previousState = $cursor->saveState();
        $cursor->advance();

        $handle = $cursor->match('/^<\s(https?|ftp):\/\/[^\s\/$.?#].[^\s]*$/i');
        if(!$handle){
            $cursor->restoreState($previousState);
            return false;
        }

        $handle = substr($handle, 2);

        $embedRequestUrl = 'http://open.iframe.ly/api/oembed?origin=screeny05&url=' . urlencode($handle);
        $embedData = false;

        try {
            $embedData = json_decode(file_get_contents($embedRequestUrl), true);
        } catch(Exception $e){
            return false;
        }
        if($embedData['status'] && $embedData['status'] != 200){
            return false;
        }

        $inlineContext->getInlines()->add(new Html($embedData['html']));
        return true;
    }
}
