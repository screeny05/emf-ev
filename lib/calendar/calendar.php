<?php

namespace EMF\Calendar;

use ConfigQuery;
use Google_Client;
use Google_Service_Calendar;
use Google_Service_Calendar_Event;
use Google_Service_Calendar_EventDateTime;

class Calendar {
    private $client;
    private $service;
    private $calendarId;

    public function __construct(){
        $this->client = self::getClient();
        $this->authenticate();

        $this->service = new Google_Service_Calendar($this->client);
        $this->calendarId = ConfigQuery::getOne('GoogleCalendarIdPrivate');
    }

    private function authenticate(){
        $this->client->setAccessToken(json_decode(ConfigQuery::getOne('GoogleOauthResponse'), true));

        if($this->client->isAccessTokenExpired()){
            $this->client->fetchAccessTokenWithRefreshToken($this->client->getRefreshToken());
            ConfigQuery::upsert('GoogleOauthResponse', json_encode($this->client->getAccessToken()));
        }
    }

    public function listEvents($beginning, $end){
        return $this->service->events->listEvents($this->calendarId, [
            'maxResults' => 10,
            'orderBy' => 'startTime',
            'singleEvents' => true,
            'timeMin' => $beginning->format('c'),
            'timeMax' => $end->format('c'),
            'showHiddenInvitations' => true,
        ])->getItems();
    }

    public function getEventsNotNotified(){
        $tomorrow = new \DateTime();
        $tomorrow->modify('+1day');
        $events = $this->listEvents(new \DateTime(), $tomorrow);

        return array_filter($events, function($event){
            $props = Calendar::getPrivateProperties($event);
            return !$props['NotifyChange'] && !$props['AlreadyNotified'];
        });
    }

    public function markEventAsNotified($e){
        $props = Calendar::getPrivateProperties($e);
        $props['AlreadyNotified'] = true;

        $properties = new \Google_Service_Calendar_EventExtendedProperties();
        $properties->setPrivate($props);
        $e->setExtendedProperties($properties);

        $this->service->events->patch($this->calendarId, $e->getId(), $e);
    }

    public function createEvent($event){
        $this->service->events->insert($this->calendarId, $event, [
            'sendNotifications' => true
        ]);
    }

    public function updateEvent($id, $event, $sendEvents, $user){
        $eventData = $this->getEventAsArray($id);

        if(!($eventData['UsersCanModify'] || $eventData['Organizer']['email'] === $user->getEmail())){
            throw new \Exception('User cannot modify Event!');
        }

        $this->service->events->patch($this->calendarId, $id, $event, [
            'sendNotifications' => $sendEvents
        ]);
    }

    public function deleteEvent($id, $sendEvents = true){
        $this->service->events->delete($this->calendarId, $id, [
            'sendNotifications' => $sendEvents
        ]);
    }

    public function getEventAsArray($id){
        $event = $this->service->events->get($this->calendarId, $id);
        return self::eventToArray($event);
    }

    public static function eventToArray($e){
        $attendees = $e->getAttendees();
        $attendeesUsers = [];
        $organizerUser = null;
        $properties = self::getPrivateProperties($e);

        foreach ($attendees as $attendee) {
            $user = self::getUserDataByAttendee($attendee);
            if($user['email'] == $properties['creator']){
                $user['isOrganizer'] = true;
                $organizerUser = $user;
            }
            $attendeesUsers[] = $user;
        }

        $data = [
            'EventID' => $e->getId(),
            'Title' => $e->getSummary(),
            'Description' => $e->getDescription(),
            'Place' => $e->getLocation(),
            'UsersCanModify' => $e->getGuestsCanModify(),
            'Start' => self::googleDateTimeToDateTime($e->getStart()),
            'End' => self::googleDateTimeToDateTime($e->getEnd()),
            'Organizer' => $organizerUser,
            'Attendees' => $attendeesUsers,
            'NotifyChange' => $properties['notify'],
            'EventType' => $properties['type']
        ];

        return $data;
    }

    public static function eventFromArray($data, $user){
        $notifyChange = !!$data['NotifyChange'];
        $event = new Google_Service_Calendar_Event();
        $event->setSummary($data['Title']);
        $event->setDescription($data['Description']);
        $event->setLocation($data['Place']);
        $event->setGuestsCanModify($data['UsersCanModify'] == 'true');

        $event->setStart(self::dateStringToGoogleDateTime($data['Start']));
        $event->setEnd(self::dateStringToGoogleDateTime($data['End']));

        if($notifyChange){
            $event->setAttendees(self::getUsersAsAttendees());
        }

        $properties = new \Google_Service_Calendar_EventExtendedProperties();
        $properties->setPrivate([
            'creator' => $user->getEmail(),
            'type' => $data['EventType'],
            'notify' => $notifyChange
        ]);

        $event->setExtendedProperties($properties);

        return $event;
    }

    public static function dateStringToGoogleDateTime($str){
        $googleDate = new Google_Service_Calendar_EventDateTime();
        $date = new \DateTime($str, new \DateTimeZone('Europe/Berlin'));
        $googleDate->setDateTime($date->format(\DateTime::ISO8601));
        return $googleDate;
    }

    public static function googleDateTimeToDateTime($date){
        return new \DateTime($date->getDateTime());
    }

    public static function getUserDataByAttendee($attendee){
        $user = \UserQuery::create()->findOneByEmail($attendee->getEmail());
        if($user){
            $user = $user->toArray();
        }

        return [
            'email' => $attendee->getEmail(),
            'user' => $user,
            'isOrganizer' => $attendee->getOrganizer(),
            'response' => $attendee->getResponseStatus(),
            'comment' => $attendee->getComment(),
            'additionalGuests' => $attendee->getAdditionalGuests()
        ];
    }

    public static function getValidator($data){
        $v = new \Valitron\Validator($data);

        $v->rule('required', ['Title', 'Start', 'End'])->message('{field} muss angegeben werden.');
        $v->rule('date', ['Start', 'End'])->message('{field} muss ein gültiges Datum enthalten.');
        $v->rule('dateAfter', ['End'], date($data['Start']))->message('{field} muss nach Terminbeginn sein.');


        $v->labels([
            'Title' => 'Titel',
            'Start' => 'Terminbeginn',
            'End' => 'Terminende'
        ]);

        return $v;
    }

    public static function getEventTypes(){
        return [
            'Vereinsveranstaltung',
            'Sitzung / Besprechung',
            'Sonstige'
        ];
    }

    public static function getUsersAsAttendees(){
        $attendees = [];
        $users = \UserQuery::create()->find();

        foreach ($users as $user) {
            $attendees[] = ['email' => $user->getEmail()];
        }
        return $attendees;
    }

    public static function getPrivateProperties($event){
        $properties = $event->getExtendedProperties();
        $privateProperties = [];

        if($properties){
            $privateProperties = $properties->getPrivate();
        }
        return $privateProperties;
    }

    public static function getOauthUrl(){
        $client = self::getClient();
        return $client->createAuthUrl();
    }

    public static function getIcsLink(){
        return 'https://calendar.google.com/calendar/ical/' . urlencode(ConfigQuery::getOne('GoogleCalendarIdPrivate')) . '/public/basic.ics';
    }

    public static function getEncodedAccessToken($authToken){
        $client = self::getClient();
        return json_encode($client->fetchAccessTokenWithAuthCode($authToken));
    }

    public static function getClient(){
        $client = new Google_Client();
        $client->setApplicationName('emf-calendar');
        $client->setScopes(Google_Service_Calendar::CALENDAR);
        $client->setAccessType('offline');
        $client->setApprovalPrompt('force');
        $client->setAuthConfig(json_decode(ConfigQuery::getOne('GoogleApiJson'), true));
        return $client;
    }
}
