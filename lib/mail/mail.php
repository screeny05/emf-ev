<?php

namespace EMF\Mail;

class Mail {
    private $sendSmtp;
    private $from;
    private $fromName;
    private $smtpHost;
    private $smtpPort;
    private $smtpUseAuth;
    private $smtpSecure;
    private $smtpUser;
    private $smtpPassword;
    private $config;

    public function __construct($config){
        $this->sendSmtp = $config['MailSendType'] == 'smtp';
        $this->from = $config['MailFrom'];
        $this->fromName = $config['MailFromName'];
        $this->smtpHost = $config['MailSmtpHost'];
        $this->smtpPort = $config['MailSmtpPort'];
        $this->smtpUseAuth = $config['MailSmtpAuth'] == 1;
        $this->smtpSecure = in_array($config['MailSmtpSecure'], ['tls', 'starttls', 'ssl']) ? $config['MailSmtpSecure'] : false;
        $this->smtpUser = $config['MailSmtpUser'];
        $this->smtpPassword = $config['MailSmtpPassword'];
        $this->config = $config;
    }

    public function send($receivers, $subject, $template, $context = []){
        $mail = new \PHPMailer(true);

        if($this->sendSmtp){
            $mail->isSMTP();
        }

        if($this->smtpUseAuth){
            $mail->SMTPAuth = true;
            $mail->Username = $this->smtpUser;
            $mail->Password = $this->smtpPassword;
        }

        $mail->CharSet = 'UTF-8';
        $mail->Host = $this->smtpHost;
        $mail->Port = $this->smtpPort;
        $mail->SMTPSecure = $this->smtpSecure;
        $mail->setFrom($this->from, $this->fromName);

        if(count($receivers) === 1){
            foreach($receivers as $receiver){
                $mail->addAddress($receiver['address'], $receiver['name']);
            }
        } else {
            $mail->addAddress($this->from, $this->fromName);
            foreach($receivers as $receiver){
                $mail->addBCC($receiver['address'], $receiver['name']);
            }
        }

        $mail->isHTML(true);

        $loader = new \Twig_Loader_Filesystem('views');
        $twig = new \Twig_Environment($loader);

        $context['meta'] = [
            'subject' => $subject,
            'fallback' => false,
            'unsubscribe' => false,
            'intro' => false,
            'brandColor' => '#2f8f8f',
            'baseUrl' => $this->config['DefaultRequestScheme'] . '://' . $this->config['Domain']
        ];
        $template = $twig->loadTemplate('mails/' . $template);
        $mail->Subject = $subject;
        $mail->Body = $template->render($context);
        $mail->AltBody = $template->hasBlock('body_alt') ? $template->renderBlock('body_alt', $context) : trim(strip_tags($mail->Body));

        return $mail->send();
    }
}
